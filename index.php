<?php 
	session_start(); //Iniciar sesion.

	include "modelo.php";
	include "vista.php";

	//Comprobamos en primer lugar cómo se introducen los datos, si por URL o mediante formulario.
	if (isset($_GET['accion'])) {
		$accion = $_GET['accion'];
		if(isset($_GET['id']))
			$id = $_GET['id'];
		elseif(isset($_POST['id']))
			$id = $_POST['id'];
	} else {
		if (isset($_POST['accion'])) {
			$accion = $_POST['accion'];
			if(isset($_GET['id']))
				$id = $_GET['id'];
			elseif(isset($_POST['id']))
				$id = $_POST['id'];
			else
				$id = 1;
		} else {
			//Generamos una opción por defecto para la primera visita a la página
			$accion = 'menu';
			$id = 1;
		}
	}

	$result = mcomprobarusuario(); //Comprobar si hay un usuario conectado o no y su estado.
	if($result == -4){ //Si no hay un usuario conectado:
		if ($accion == "registrar"){
			switch ($id) {
				case 1:
					//Para conseguir loguear a una persona, vamos a tener que comprobar que esta se encuentra registrada.
					//Por ello, solicitaremos al modelo que nos proporcione 
					vregistrar();
					break;
				case 2:
					//Comprobar los datos introducidos por el usuario para validar o rechazar el regitro del mismo
					vmostrarregistro(mvalidarregistro());
					break;
				case 3:
					//Obtener las provincias:
					vmostrarprovincias(mobtenerprovincias());
					break;
				case 4:
					//Obtener los municipios de acuerdo a la provincia seleccionada
					vmostrarmunicipios(mobtenermunicipios());
					break;
				default:
					vpaginanoencontrada();
					break;
			}
		}
		elseif ($accion == "login"){
			switch ($id) {
				case 1:
					vlogin();
					break;
				case 2:
					vmostrarlogin(mvalidarlogin());
					break;
				default:
					vpaginanoencontrada();
					break;
			}
		}
		elseif ($accion == "menu") {
			switch ($id) {
				case 1: //Mostrar el menu del usuario anonimo.
					vmostrarmenu();
					break;
				default:
					vpaginanoencontrada();
					break;
			}
		}
		#La accion ver_foros se encarga del tema de los foros
		elseif ($accion == "ver_foros"){
			switch ($id) {
				case 1:
					#Vemos una lista con una informacion breve sobre todos los foros
					vmostrarforos(mlistapost());
					break;
				case 2:
					#Mostramos la informacion completa del foro, con titulos y comentarios
					vmostrarforo(mforoelegido());
					break;
				case 6:
					#Funcion Ajax que gestiona la busqueda de foros por titulo
					vbuscarforo(mbuscarforo(),mnumeropost());
					break;
				case 7:
					#Recuperar los comentarios asociados a un post:
					vmuestracomentarios(mrecuperacomentarios());
					break;
				default:
					vpaginanoencontrada();
					break;
			}
		}
		elseif ($accion == "listado") {
			switch ($id) {
				case 1: //Mostrar pagina de entradas
					vmostrarpaginaentradas();
					break;
				case 2: //Actualizar las entradas y mostrarlas
					vmostrarentradas(mrecuperaentradas(),mnumentradas());
					break;
				case 3: //Ver una entrada
					vmostrarentrada1(mselecentrada());
					break;
				case 6: //Obtener el listado de criticas:
					vmostrarcriticas(mrecuperacriticas(),mnumerocriticas());
					break;
				default:
					vpaginanoencontrada();
					break;
			}
		}
		elseif ($accion == "imagenes"){
			if ($id == 1){
				vactualizarimagenes(mactualizarimagenes());
			}
		}
		elseif ($accion == "escribircritica"){
			switch ($id) {
				case 5:
				#Función que muestra la crítica saleccionada por el usuario.
					vmuestracritica(mrecuperacritica());
					break;
				default:
					vpaginanoencontrada();
					break;
			}
		}
		else{
			vpaginanoencontrada();
		}
	}
	elseif ($result == 0) { //Si hay un usuario conectado	
		//La opción por defecto va a ser mostrar la página principal
		if ($accion == "menu") {
			switch ($id) {
				case 1: //Mostrar el menu del usuario conectado.
					vmostrarmenuusuario();
					break;
				default:
					vpaginanoencontrada();
					break;
			}
		}
		elseif ($accion == "sesion"){
			switch ($id) {
				case 1: //El usuario solicita cerrar su sesión:
					vmostrarcierresesion();
					break;
				case 2:
					mcierrasesion();
					vmostrarsesion(0);
					break;
				default:
					vpaginanoencontrada();
					break;
			}
		}
		elseif ($accion == "verificar"){
			switch ($id) {
				case 1: 
					vmostrarverificar();
					break;
				case 2:
					vmostrarresultadoverificacion(mcomprobarsolicitud());
					break;
				default:
					vpaginanoencontrada();
					break;
			}
		}
		elseif ($accion == "listado") {
			switch ($id) {
				case 1: //Mostrar pagina de entradas
					vmostrarpaginaentradas();
					break;
				case 2: //Actualizar las entradas y mostrarlas
					vmostrarentradas(mrecuperaentradas(),mnumentradas());
					break;
				case 3: //Ver una entrada
					vmostrarentrada1(mselecentrada());
					break;
				case 4:
					if($_SESSION['tipo'] == 2){ //Comprobar que el usuario tiene permisos para modificar
						$ha_modificado = mhamodificado(); //Comprobar si el usuario ha modificado ya esa entrada 
						if($ha_modificado == 0){
							vmodificarentrada(mrecuperaentrada());
						}
						else
							vhamodificado($ha_modificado);
					}
					else
						mostrarmensaje("No puede modificar la entrada","No tiene permiso para modificar la entrada, para ello debe verificarse",0);
					break;
				case 5:
					if($_SESSION['tipo'] == 2){
						if(!empty(mrecuperaentrada())){
							vcheckmodificacion(mcheckmodificacion());
						}
						else
							mostrarmensaje("No se pudo modificar la entrada", "No se pudo obtener la información de la entrada seleccionada.",0);
					}
					else
						mostrarmensaje("No puede modificar la entrada","No tiene permiso para modificar la entrada, para ello debe verificarse",0);
					break;
				case 6: //Obtener el listado de criticas:
					vmostrarcriticas(mrecuperacriticas(),mnumerocriticas());
					break;
				default:
					vpaginanoencontrada();
					break;
			}
		}
		elseif ($accion == "perfil") {
			switch ($id) {
				case 1: //Mostrar pagina de perfil del usuario
					vmostrarperfil(mrecuperaimagen(),mrecuperaprovincia(),mrecuperamunicipio());
					break;
				case 2: //Cambio de contraseña:
					vcambiopassword(mcambiopassword());
					break;
				case 3: //Cambio imagen
					vcambioimagen(mcambioimagen());
					break;
				case 4: //Cambio datos personales:
					vcambiodatosperfil(mcambiodatosperfil());
					break;
				case 5:
					//Obtener las provincias:
					vmostrarprovincias(mobtenerprovincias());
					break;
				case 6:
					//Obtener los municipios de acuerdo a la provincia seleccionada
					vmostrarmunicipios(mobtenermunicipios());
					break;
				case 7: //Ver el perfil de otro usuario
					vmostrarperfilajeno(mobtenerperfilajeno());
					break;
				default:
					vpaginanoencontrada();
					break;
			}
		}	#La accion descuentos gestiona la informacion relativa a los descuentos disponibles para un usuario
		elseif ($accion == "descuentos"){
			switch ($id) {
				case 1:
					#mostramos la lista de descuentos disponibles para el usuario que esta en la sesion
					vmostrarlistadodescuentos();
					break;
				case 2:
					#Mostraremos un pdf con los datos relativos al descuento.
					vmostrardescuentoelegido(mmostrardescuentoelegido());
					break;
				case 3:
					#Recuperar los descuentos disponibles para un usuario
					vmostrardescuentos(mrecuperadescuentos(), mnumerodescuentos());
					break;
				default:
					vpaginanoencontrada();
					break;
			}
		}

		elseif ($accion == "ver_foros"){
			switch ($id) {
				case 1:
					#Vemos una lista con una informacion breve sobre todos los foros
					vmostrarforos(mlistapost());
					break;
				case 2:
					#Mostramos la informacion completa del foro, con titulos y comentarios
					vmostrarforo(mforoelegido());
					break;
				case 3:
					#Funcion que gestiona la escritura de un nuevo comentario
					vescribircomentario(mescribircomentario());
					break;
				case 4:
					#Funcion que gestiona la creacion de un nuevo foro
					vcrearforo();
					break;
				case 5:
					#Mostramos de nuevo la lista con el nuevo foro
					vmostrarforos(mnuevoforo());
					break;
				case 6:
					#Funcion Ajax que gestiona la busqueda de foros por titulo
					vbuscarforo(mbuscarforo(),mnumeropost());
					break;
				#Recuperar los comentarios asociados a un post:
				case 7:
					vmuestracomentarios(mrecuperacomentarios());
					break;
				default:
					vpaginanoencontrada();
					break;
			}
		}
		#La accion escribircritica gestiona las criticas que hay en las entradas de cine
		elseif ($accion == "escribircritica"){
			switch ($id) {
				case 1:
					#Funcion que gestiona la insercion de una critica en la BBDD
					mescribircritica();
					break;
				case 2:
					#Funcion muestra una critica que puede ser modificada
					veditarcritica(meditarcritica());
					break;
				case 3:
					#Funcion que gestiona la modificacion de una critica ya realizada
					vmodificarcritica(mmodificarcritica());
					break;
				case 4:
					#Funcion que se encarga de la eliminacion de las criticas presentes
					veliminarcritica(meliminarcritica());
					break;
				case 5:
				#Función que muestra la crítica saleccionada por el usuario.
					vmuestracritica(mrecuperacritica());
					break;
				default:
					vpaginanoencontrada();
					break;
			}
		}
		elseif ($accion == "notificaciones") {
			switch ($id) {
				case 1: //Mostrar listado notificaciones:
					vmostrarpaginanotificaciones(mnumnotificaciones());
					break;
				case 2: //Buscar notificaciones del usuario:
					vnotificaciones(mnumnotificaciones());
					break;
				case 3: //Obtener las notificaciones del usuario:
					vmostarnotificaciones(mnotificaciones(),mnumnotificaciones());
					break;
				case 4: //Ver una notificación en concreto:
					vnotificacion(mrecuperanotificacion());
					break;
				case 5: //Eliminar notificación:
					velimnarnotificacion(meliminarnotificacion());
					break;
				case 6: //Eliminar todas las notificaciones:
					veliminartodasnotificaciones(meliminartodasnotificaciones());
					break;
				default:
					vpaginanoencontrada();
					break;
			}
		}
		elseif ($accion == "imagenes"){
			if ($id == 1){
				vactualizarimagenes(mactualizarimagenes());
			}
		}
		elseif ($accion == "tiempo") {
			switch ($id) {
				case 1:
					mactualizatiempo();
					break;
				default:
					vpaginanoencontrada();
					break;
			}
		}
		else{
			vpaginanoencontrada();
		}
	}
	else //Si ha caducado la sesión del usuario:
		vmostrarsesion($result);
?>