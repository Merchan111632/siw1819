<?php  
	include_once("constantes.php");
	//Soy la vista
	function vmostrarmenu() {
		echo file_get_contents("menu.html");
	}

	function vmostrarmenuusuario(){
		$cadena = file_get_contents("menu_usuario.html");
		$cadena = str_replace("##usuario##", $_SESSION['nick'], $cadena);
		echo $cadena;
	}

	function vregistrar() {
		echo file_get_contents("registrar.html");
	}

	function vlogin(){
		$cadena = file_get_contents("iniciar_sesion.html");

		$cookie_nombre = 'inicio_sesion';
		if(isset($_COOKIE[$cookie_nombre])){ //Comprobar si la cookie existe ya:
			$trozos = explode(",", $_COOKIE[$cookie_nombre]); //Fragmentar la cadena de valores de la cookie para obtener el usuario y la password
		    $correo = $trozos[0];
		    $password = $trozos[1];

		    $cadena = str_replace("##correo##", $correo, $cadena);
		    $cadena = str_replace("##password##", $password, $cadena);
		    $cadena = str_replace("##estado##", "checked", $cadena);
		}
		else{
			$cadena = str_replace("##correo##", "", $cadena);
			$cadena = str_replace("##password##", "", $cadena);
			$cadena = str_replace("##estado##", "", $cadena);
		}
		echo $cadena;
	}

	function vpaginanoencontrada(){
		echo file_get_contents("paginanoencontrada.html");
	}

	/** Función que muestra el cierre de sesión de un usuario:  **/
	function vmostrarcierresesion(){
		echo file_get_contents("cerrarSesion.html");
	}

	/** Función que muestra la página de las entradas (sobre esta se mostrarán las entradas) **/
	function vmostrarpaginaentradas(){
		if (isset($_SESSION['user'])){
			$cadena = file_get_contents("listadoentradas_registrados.html");
		}else{
			$cadena = file_get_contents("listadoentradas_no_registrados.html");
		}
		echo $cadena;
	}

	/** Función que muestra la página de las notificaciones (sobre esta se mostrarán las notificaciones) **/
	function vmostrarpaginanotificaciones($num_notificaciones){
		$cadena = file_get_contents("listado_notificaciones.html");
		if(!empty($num_notificaciones)){
			$numero_notificaciones = $num_notificaciones -> fetch_assoc();
			if($numero_notificaciones['num_notificaciones'] > 0)
				$cadena = str_replace('##Eliminar##', 'Eliminar todas las notificaciones', $cadena);
			else
				$cadena = mostrarmensaje("No tiene notificaciones", "Actualmente no tiene notificaciones",0);
		}
		echo $cadena;
	}

	/** Función que muestra un listado con las entradas de la página web. **/
	function vmostrarentradas($entradas,$numentradas){
		$cadena = file_get_contents("entradas.html");
		$trozos = explode("##fila##",$cadena);

		$aux = "";
		$cuerpo = "";

		if(isset($_GET["pagina"])){
			$pagina_actual = $_GET["pagina"]; //Obtener la página actual 
			if(!empty($entradas)){ //Comprobar que el parámetro entradas no sea nulo
					while($entrada = $entradas->fetch_assoc()){ //Leer las entradas recuperadas y sustituirlas en el listado iterando
						$aux = $trozos[1];
						$aux = str_replace("##Titulo##", $entrada["titulo"], $aux);
						$aux = str_replace("##Descripcion##", substr($entrada["texto"],0,TAMANO_DESCRIPCION).'...', $aux);
						$aux = str_replace("##enlaceEntrada##", $entrada["id_entrada"], $aux);
						$cuerpo .= $aux;
					}

					$cadena = $trozos[0] . $cuerpo . $trozos[2];

					//Llegado a este punto ya estarían mostradas todas las entradas permitidas.

					//Hay que paginar la página actual
					if(!empty($numentradas)){ //Comprobar que el parámetro numusuarios no sea nulo
						$datos = $numentradas->fetch_assoc();
						$numero_elementos = $datos["num_entradas"];
						paginar($cadena,$numero_elementos,$pagina_actual);
					}
					else //Numero de entradas nulo
						mostrarmensaje("No se detectó en número de entradas", "No se recibió el número de entradas a mostrar",1);
			}
			else //Si el parámetro entradas es nulo:
				mostrarmensaje("No hay entradas para mostrar", "No se encontraron entradas para mostrar, intentelo de nuevo más tarde.",1);
		}
		else
			mostrarmensaje("No se pudo obtener la página actual", "No se encontró el número de la página actual",1);
	}

	/**Funcion que permite realizar la paginación de una página, recibe como parámetros la cadena con toda la información a mostrar de la página,
	el número total de elementos leidos de la base de datos y la pagina actual. Añade a el final de la cadena recibida la paginación **/
	function paginar($cadena,$numero_elementos,$pagina_actual){
		
		$paginas = intdiv($numero_elementos, ELEMENTOS_PAGINA); //Dividir el número de elementos entre las entradas por página para saber cuantas páginas mostrar

		$resto = $numero_elementos % ELEMENTOS_PAGINA; //Determinar el resto de la división (Si se obtiene un resto distinto de cero añadir una página más)
		if ($resto > 0) {
			$paginas++;
		}
		//Comprobar si el usuario está haciendo una busqueda:
		$letra = "";
		if(isset($_GET['letra']))
			$letra = $_GET['letra'];
		//Sustituir el botón siguiente y el botón anterior:
		//Anterior:
		$trozos = explode("##anterior##", $cadena);
		if($pagina_actual > 1){ //Si la página actual no es la primera entonces mostrar el botón a la página anteriror.
			$pagina_anterior = $pagina_actual - 1;
			$aux = $trozos[1];
			$aux = str_replace("##paginaanterior##", $pagina_anterior, $aux);
			$aux = str_replace("##letra##", $letra, $aux);
			$cadena = $trozos[0] . $aux . $trozos[2];				
		}
		else { //Si es la primera página simplemente ocultar el botón anterior
			$cadena = $trozos[0] . $trozos[2];	
		}
		//Siguiente:
		$trozos = explode("##siguiente##", $cadena);
		if($pagina_actual < $paginas){ //Si la página actual no es la última entonces mostrar el botón a la página siguiente.
			$pagina_siguiente = $pagina_actual + 1;
			$aux = $trozos[1];
			$aux = str_replace("##paginasiguiente##", $pagina_siguiente, $aux);
			$aux = str_replace("##letra##", $letra, $aux);
			$cadena = $trozos[0] . $aux . $trozos[2];				
		}
		else { //Si la pagina actual es la última ocultar el botón siguiente
			$cadena = $trozos[0] . $trozos[2];	
		}
		//Por ultimo colocar los números de las páginas:

		$trozos = explode("##paginas##", $cadena);
		$cuerpo = "";

		//Primero determinar cual va a ser el primer número de página que se va a mostrar
		if($pagina_actual <= LIMITE_PAGINA)
			$pagina_inicial = 1;
		else
			$pagina_inicial = $pagina_actual - (LIMITE_PAGINA-1);
		//Segundo determinar hasta que página se va a mostrar
		if($paginas > LIMITE_PAGINA)
			$paginas = $pagina_inicial + (LIMITE_PAGINA-1);

		for($i = $pagina_inicial; $i <= $paginas; $i++){
			$aux = $trozos[1];
			$aux = str_replace("##numeropagina##", $i, $aux);
			$aux = str_replace("##letra##", $letra, $aux);

			//Resaltar la página actual (más adelante en css)
			if ($i == $pagina_actual) {
				$aux = str_replace("##color##", "red", $aux);	
			} else {
				$aux = str_replace("##color##", "blue", $aux);
			}
			$cuerpo .= $aux;
			}
		$cadena = $trozos[0] . $cuerpo . $trozos[2];
		echo $cadena;
	}


	function vmostrarentrada1($resultado){
		if (isset($_SESSION['user'])){
			$cadena = file_get_contents("verentrada_registrado.html");
		}else{
			$cadena = file_get_contents("verentrada_no_registrado.html");
		}
		$trozos = explode("##critica##", $cadena);
		$entrada = $resultado[0];
		$actores = $resultado[1];
		$directores = $resultado[2];
		$critica = $resultado[3];
		if(isset($_SESSION['user']))
			$correo_sesion = $_SESSION['user'];
		else
			$correo_sesion = "anonimo";
		if(isset($_SESSION['tipo']))
			$tipo_usuario = $_SESSION['tipo'];
		else
			$tipo_usuario = -1;
		$valoracion = $resultado[4];
		$imagenes = $resultado[5];

		if(sizeof($resultado) == 7)
			$puede_escribir = $resultado[6];
		
		$aux = "";
		$cuerpo = "";
		$cabecera = "";
		$cola = "";
		$datos = $entrada->fetch_assoc();
		$media = $valoracion->fetch_assoc();
		$aux = $trozos[0];
		$aux = str_replace('##titulo##', $datos['titulo'], $aux);
		$aux = str_replace('##texto##', $datos['texto'], $aux);
		$aux = str_replace('##val_media##', $media['media'], $aux);
		$aux = str_replace('##Autor##', $datos['nick_creador'], $aux);
		if($correo_sesion == $datos["correo_creador"])
			$aux = str_replace("##tipo##", 1, $aux);
		else
			$aux = str_replace("##tipo##", 0, $aux);
		$aux = str_replace('##Fecha##', $datos['fecha_creacion'], $aux);
		$aux = str_replace('##AutorModificacion##', $datos['nick_modificacion'], $aux);
		if($correo_sesion == $datos["correo_modificacion"])
			$aux = str_replace("##tipoModificacion##", 1, $aux);
		else
			$aux = str_replace("##tipoModificacion##", 0, $aux);
		$aux = str_replace('##FechaModificacion##', $datos['fecha_modificacion'], $aux);
		if($tipo_usuario == 2){
			$aux = str_replace('##Modificar_entrada##', 'Modificar datos de la entrada', $aux);
		}
		else
			$aux = str_replace('##Modificar_entrada##', '', $aux);
		$aux = str_replace('##Entrada##', $datos['id_entrada'], $aux);
		if (isset($datos['trailer'])){
			if (!empty($datos['trailer'])){
				$url_video = $datos['trailer'];
				if(strpos($url_video,"youtube")) //Comprobar si la url es de youtube y modificarla:
					$url_video = str_replace("watch?v=","embed/", $url_video);

				$insert_video = "<iframe width='420' height='315' src=".$url_video." allowfullscreen frameborder='0'> </iframe>";
				$aux = str_replace('##trailer##', $insert_video, $aux);
				$aux = str_replace('##TrailerPelicula##', "<hr> Trailer de la película:", $aux);
			}else{
				$aux = str_replace('##trailer##', "", $aux);
				$aux = str_replace('##TrailerPelicula##', "", $aux);
			}
		}else{
			$aux = str_replace('##trailer##', " ", $aux);
		}
		//Obtenemos los actores
		$aux_actores="";
		$it_actores = 1;
		while($datos_actores = $actores->fetch_assoc()){
			if ($it_actores < mysqli_num_rows($actores)){
				if (isset($datos_actores['apellido2'])){
					$aux_actores .= $datos_actores['nombre'] . $datos_actores['apellido1']. $datos_actores['apellido2'] . ', ';
				}else{
					$aux_actores .= $datos_actores['nombre'] . $datos_actores['apellido1'].', ';
				}
			}else{
				if (isset($datos_actores['apellido2'])){
					$aux_actores .= $datos_actores['nombre'] . $datos_actores['apellido1']. $datos_actores['apellido2'];
				}else{
					$aux_actores .= $datos_actores['nombre'] . $datos_actores['apellido1'];
				}
			}
			$it_actores +=1;
		}
		$aux = str_replace('##reparto##', $aux_actores, $aux);

		$aux_directores = "";
		$it_directores = 1;
		while($datos_directores = $directores->fetch_assoc()){
			if ($it_directores < mysqli_num_rows($directores)){
				if (isset($datos_directores['apellido2'])){
					$aux_directores .= $datos_directores['nombre'] . ' ' . $datos_directores['apellido1']. ' '. $datos_directores['apellido2'] . ', ';
				}else{
					$aux_directores .= $datos_directores['nombre'] . ' ' . $datos_directores['apellido1']. ', ';
				}
			}else{
				if (isset($datos_directores['apellido2'])){
					$aux_directores .= $datos_directores['nombre'] . ' ' . $datos_directores['apellido1']. ' '. $datos_directores['apellido2'];
				}else{
					$aux_directores .= $datos_directores['nombre'] . ' ' . $datos_directores['apellido1'];
				}	
			}
			$it_directores += 1;
		}


		$aux = str_replace('##direccion##', $aux_directores, $aux);

		$it = 1;
		$num_imagenes = mysqli_num_rows($imagenes);
		switch ($num_imagenes) {
			case 1:
				$aux = str_replace('##imagen2##', " ", $aux);
				$aux = str_replace('##imagen3##', " ", $aux);
				break;
			case 2:
				$aux = str_replace('##imagen3##', " ", $aux);
				break;
			case 0:
				$aux = str_replace('##imagen1##', " ", $aux);
				$aux = str_replace('##imagen2##', " ", $aux);
				$aux = str_replace('##imagen3##', " ", $aux);
				break;
			default:
				break;
		}
		while ($datos_imagenes = $imagenes->fetch_assoc()){
			$imagen = $datos_imagenes['imagen'];
			if ($it == 1){
				$sus_imagen = '<img id="'.$datos_imagenes['id_imagen'].'" heigh="500" width="500" src="data:image;base64,'.$imagen.' " onclick="setPrincipal(this.id)">';
			}else{
				$sus_imagen = '<img id="'.$datos_imagenes['id_imagen'].'" heigh="200" width="200" src="data:image;base64,'.$imagen.' " onclick="setPrincipal(this.id)">';
			}
			$aux = str_replace('##imagen'.$it.'##', $sus_imagen, $aux);
			$it += 1;
		}


		$cabecera.=$aux;
		
		if(!empty($puede_escribir)){
			$escribe = $puede_escribir->fetch_assoc();
			if ($escribe["numcriticas"] == 0 and ($tipo_usuario == 2)){
				$aux2=$trozos[1];
				$aux2 = str_replace("##identrada##", $datos["id_entrada"], $aux2);
				$cuerpo.=$aux2;
			}else{
				$cuerpo .="";
			}
		}
		else
			$cuerpo .="";
		echo $cabecera . $cuerpo . $trozos[2];
	}


	/** Función que permite mostrar todas las criticas asociadas a una entrada **/
	function vmostrarcriticas($criticas, $numcriticas){
		if(isset($_SESSION['user']))
			$cadena = file_get_contents("criticas_registrado.html");
		else
			$cadena = file_get_contents("criticas_no_registrado.html");
		$trozos = explode("##fila##",$cadena);

		$aux = "";
		$cuerpo = "";

		if(isset($_GET["pagina"])){
			$pagina_actual = $_GET["pagina"]; //Obtener la página actual 
			if(!empty($criticas)){ //Comprobar que el parámetro entradas no sea nulo
				if(isset($_SESSION['user']))
					$correo_sesion = $_SESSION['user'];
				else
					$correo_sesion = "anonimo";
				if(isset($_SESSION['tipo']))
					$tipo_usuario = $_SESSION['tipo'];
				else
					$tipo_usuario = -1;

				while($datos_critica = $criticas->fetch_assoc()){ //Leer las entradas recuperadas y sustituirlas en el listado iterando
					$aux = $trozos[1];
					if ($datos_critica["correo_creador"] != $correo_sesion){
						$aux = str_replace("##modificar##", " ", $aux);
						$aux = str_replace("##eliminarcritica##", "", $aux);
					}else{
						if ($tipo_usuario == 2){
							$aux = str_replace("##modificar##", "Modificar critica", $aux);
							$aux = str_replace("##eliminarcritica##", "Eliminar critica", $aux);
						}else{
							$aux = str_replace("##modificar##", "", $aux);
							$aux = str_replace("##eliminarcritica##", "", $aux);
						}
					}
					$aux = str_replace("##critica##", substr($datos_critica["critica"],0,TAMANO_CRITICA).'...', $aux);
					$aux = str_replace("##identrada##", $_GET["entrada"], $aux);
					$aux = str_replace("##escritor##", $datos_critica["nick"], $aux);
					$aux = str_replace("##Correo##", $datos_critica['correo_creador'], $aux);
					$aux = str_replace("##Fecha_critica##", $datos_critica["fecha"], $aux);
					$aux = str_replace("##valoracion##", $datos_critica["valoracion"], $aux);
					if($correo_sesion == $datos_critica['correo_creador'])
						$aux = str_replace("##tipo##", 1, $aux);
					else
						$aux = str_replace("##tipo##", 0, $aux);
					$cuerpo .= $aux;
				}

				$cadena = $trozos[0] . $cuerpo . $trozos[2];

				//Llegado a este punto ya estarían mostradas todas las críticas permitidas.

				//Hay que paginar la página actual
				if(!empty($numcriticas)){ //Comprobar que el parámetro numusuarios no sea nulo
					$datos = $numcriticas->fetch_assoc();
					$numero_elementos = $datos["num_criticas"];
					paginar($cadena,$numero_elementos,$pagina_actual);
				}
				else //Numero de críticas nulo
					mostrarmensaje("No se detectó en número de críticas", "No se recibió el número de críticas a mostrar",1);
			}
			else //Si el parámetro críticas es nulo:
				mostrarmensaje("No hay críticas para mostrar", "No se encontraron críticas para mostrar, intentelo de nuevo más tarde.",1);
		}
		else
			mostrarmensaje("No se pudo obtener la página actual", "No se encontró el número de la página actual",1);
	}

	/** Funcion que permite que muestra el resultado de comprobar si un usuario ha modificado una entrada previamente **/
	function vhamodificado($valor){
		if($valor == -1)
			mostrarmensaje("No se puede volver a modificar la entrada", "Ya ha modificado esta entrada, espere a que su modificación sea aceptada o rechzada para poder realizar una nueva",0);
		elseif ($valor == -2) {
			mostrarmensaje("No se puede modificar la entrada", "La entrada no puede ser modificada en estos momentos, intentelo más tarde.",0);
		}
		elseif ($valor == -3) {
			mostrarmensaje("No se pudo obtener la información", "No se pudo encontrar la entrada seleccionada.",0);
		}
	} 


	/** Funcion que permite modificar una entrada seleccionada **/
	function vmodificarentrada($datos){
		$cadena = file_get_contents("modificar_entrada.html");

		if(!empty($datos)){ //Comprobar que los datos de la entrada no son nulos:
			$datos_entrada = $datos -> fetch_assoc();
			$cadena = str_replace("##Titulo##", $datos_entrada['titulo'], $cadena);
			$cadena = str_replace("##Contenido##", $datos_entrada['texto'], $cadena);
			$cadena = str_replace("##Entrada##", $_POST['entrada'], $cadena);
			echo $cadena;
		}
		else
			mostrarmensaje("No se pudo modificar la entrada", "No se pudo obtener la información de la entrada seleccionada.",0);
	}

	/** Función que permite comprobar la modificación que ha realizado el usuario de una entrada **/
	function vcheckmodificacion($valor){
		if($valor == 0)
			mostrarmensaje("Se ha enviado la modificación", "Se ha enviado la modificación de la entrada, esta será revisada por el administrador",0);
		elseif ($valor == -1) {
			mostrarmensaje("No se pudo enviar la modificación", "Hubo un error al enviar la modificación intento mas tarde.",1);
		}
		elseif ($valor == -2) {
			mostrarmensaje("El texto no cumple la longitud", "El texto debe tener un minimo de ".MIN_CONTENIDO." y como mucho ".MAX_CONTENIDO." carácteres",1);
		}
		elseif ($valor == -3) {
			mostrarmensaje("El titulo no cumple la longitud", "La longitud del titulo debe estar entre ".MIN_TITULO." y ".MAX_TITULO." carácteres",1);
		}
		elseif ($valor == -4) {
			mostrarmensaje("El contenido es vacio", "El contenido de su modificación esta vacio, reviselo",1);
		}
		elseif ($valor == -5) {
			mostrarmensaje("El titulo es vacio", "El titulo de su modificación esta vacio, reviselo",1);
		}
		elseif ($valor == -6) {
			mostrarmensaje("No se ha encontrado la entrada", "No se ha encontrado la entrada a modificar.",1);
		}
	}

	/** Función que permite mostar el perfil de un usuario **/
	function vmostrarperfil($imagen, $provincia, $municipio){
		$cadena = file_get_contents('perfil.html');

		$cadena = str_replace("##nombre##", $_SESSION['nombre'], $cadena);
		$cadena = str_replace("##apellido1##", $_SESSION['ap1'], $cadena);
		$cadena = str_replace("##apellido2##", $_SESSION['ap2'], $cadena);
		$cadena = str_replace("##correo##", $_SESSION['user'], $cadena);
		$cadena = str_replace("##direccion##", $_SESSION['direccion'], $cadena);
		$cadena = str_replace("##nick##", $_SESSION['nick'], $cadena);

		if($_SESSION['tipo'] == 2)
			$cadena = str_replace("##verificado##", 'Si', $cadena);
		elseif($_SESSION['tipo'] == 1)
			$cadena = str_replace("##verificado##", 'Pendiente de verificación', $cadena);
		elseif($_SESSION['tipo'] == 0)
			$cadena = str_replace("##verificado##", 'No', $cadena);
		if(!empty($imagen)){ //Comprobar que la imagen no sea vacia:
			$imagen_perfil = $imagen -> fetch_assoc();
			//Codificar la imagen en base 64:
			$cadena = str_replace("##imagen##", base64_encode($imagen_perfil['imagen_perfil']), $cadena);
		}
		if(!empty($provincia)){ //Comprobar que la provincia no sea nula:
			$provincia_perfil = $provincia -> fetch_assoc();
			$cadena = str_replace("##provincia##", $provincia_perfil['provincia'], $cadena);
		}
		else
			$cadena = str_replace("##provincia##", "No encontrada", $cadena);

		if(!empty($municipio)){ //Comprobar que la municipio no sea nula:
			$municipio_perfil = $municipio -> fetch_assoc();
			$cadena = str_replace("##municipio##", $municipio_perfil['municipio'], $cadena);
		}
		else
			$cadena = str_replace("##municipio##", "No encontrada", $cadena);

		if(isset($_GET['nick']))
			$cadena = $cadena = str_replace("##goBack##", "window.location.reload()", $cadena);
		else
			$cadena = $cadena = str_replace("##goBack##", "window.history.back()", $cadena);

		echo $cadena;
	}

	/***Funcion encagrada de mostrar mensajes 
		Los mensajes seran de dos tipos:
		-0 Exito.
		-1 Error.
		***/
	function mostrarmensaje($titulo, $texto,$tipo) {
		if($tipo == 1)
			$mensaje = file_get_contents("mensaje.html");
		else
			$mensaje = file_get_contents("mensaje_url.html");
		$mensaje = str_replace("##titulo##", $titulo, $mensaje);
		$mensaje = str_replace("##texto##", $texto, $mensaje);

		echo $mensaje;
	}

	function vmostrarregistro($valor){
		switch ($valor) {
			case 0:
				mostrarmensaje("Registro correcto", "Se ha registrado correctamente en Merchacine",0);
				break;
			case -1:
				mostrarmensaje("Fallo al registrarse", "No se le ha podido insertar en la base de datos",1);
				break;
			case -2:
				mostrarmensaje("Fallo al registrar", "El email especificado ya se encuentra en uso",1);
				break;
			case -3:
				mostrarmensaje("Fallo al registrarse", "No se ha podido acceder a la base de datos, intentelo de nuevo mas tarde",1);
				break;
			case -4:
				mostrarmensaje("Fallo al registrarse", "Las contraseñas no coinciden",1);
				break;
			case -5:
				mostrarmensaje("Fallo al registrarse", "La constraseña de confirmación solo pede contener letras mayúsculas, minúsculas y números",1);
				break;
			case -6:
				mostrarmensaje("Fallo al registrarse", "La constraseña solo pede contener letras mayúsculas, minúsculas y números",1);
				break;
			case -7:
				mostrarmensaje("Fallo al registrarse", "La longitud de la contraseña de confirmación debe ser de al menos ".MIN_PASSWD." y como máximo ".MAX_PASSWD." carácteres",1);
				break;
			case -8:
				mostrarmensaje("Fallo al registrarse", "La longitud de la contraseña debe ser de al menos ".MIN_PASSWD." y como máximo ".MAX_PASSWD." carácteres",1);
				break;
			case -9:
				mostrarmensaje("Fallo al registrarse", "Los correos electrónicos introducidos no coinciden",1);
				break;
			case -10:
				mostrarmensaje("Fallo al registrarse", "El correo electrónico de confirmación introducido no es válido",1);
				break;
			case -11:
				mostrarmensaje("Fallo al registrarse", "El correo electrónico introducido no es válido",1);
				break;
			case -12:
				mostrarmensaje("Fallo al registrarse", "La constraseña de confirmación está vacia",1);
				break;
			case -13:
				mostrarmensaje("Fallo al registrarse", "La contraseña está vacia",1);
				break;
			case -14:
				mostrarmensaje("Fallo al registrarse", "El correo electrónico de confirmación excede el limite máximo de ".MAX_CORREO." caracteres",1);
				break;
			case -15:
				mostrarmensaje("Fallo al registrarse", "El correo electrónico de confirmación está vacio",1);
				break;
			case -16:
				mostrarmensaje("Fallo al registrarse", "El correo electrónico excede el limite máximo de ".MAX_CORREO." caracteres",1);
				break;
			case -17:
				mostrarmensaje("Fallo al registrarse", "El correo electrónico está vacio",1);
				break;
			case -18:
				mostrarmensaje("Fallo al registrarse", "El segundo apellido solo puede contener letras mayúsculas y minúsculas",1);
				break;
			case -19:
				mostrarmensaje("Fallo al registrarse", "El segundo apellido debe tener al menos ".MIN_APELLIDO." y como maximo ".MAX_APELLIDO." caracteres",1);
				break;
			case -20:
				mostrarmensaje("Fallo al registrarse", "El segundo apellido está vacio",1);
				break;
			case -21:
				mostrarmensaje("Fallo al registrarse", "El primer apellido solo puede contener letras mayúsculas y minúsculas",1);
				break;
			case -22:
				mostrarmensaje("Fallo al registrarse", "El primer apellido debe tener al menos ".MIN_APELLIDO." y como maximo ".MAX_APELLIDO." caracteres",1);
				break;
			case -23:
				mostrarmensaje("Fallo al registrarse", "El primer apellido está vacio",1);
				break;
			case -24:
				mostrarmensaje("Fallo al registrarse", "El nombre solo puede contener letras mayúsculas y minúsculas",1);
				break;
			case -25:
				mostrarmensaje("Fallo al registrarse", "El nombre debe tener al menos ".MIN_NOMBRE." y como maximo ".MAX_NOMBRE." caracteres",1);
				break;
			case -26:
				mostrarmensaje("Fallo al registrarse", "El nombre está vacio",1);
				break;
			case -33:
				mostrarmensaje("Fallo al registrarse", "No se ha encontrado la provincia seleccionada, reviselo",1);
				break;
			case -34:
				mostrarmensaje("Fallo al registrarse", "No se pudo comprobar la provincia seleccionada.",1);
				break;
			case -35:
				mostrarmensaje("Fallo al registrarse", "No se ha detectado la provincia",1);
				break;
			case -43:
				mostrarmensaje("Fallo al registrarse", "No se ha encontrado el municipio seleccionada, reviselo",1);
				break;
			case -44:
				mostrarmensaje("Fallo al registrarse", "No se pudo comprobar el municipio seleccionada.",1);
				break;
			case -45:
				mostrarmensaje("Fallo al registrarse", "No se ha detectado el municipio",1);
				break;
			case -52:
				mostrarmensaje("Fallo al registrarse", "La dirección debe tener al menos ".MIN_DIRECCION." y como máximo ".MAX_DIRECCION." carácteres",1);
				break;
			case -53:
				mostrarmensaje("Fallo al registrarse", "La dirección es vacia.",1);
				break;
			case -31:
				mostrarmensaje("Fallo al registrarse", "Debe hacer click en el Captcha",1);
				break;
			case -54:
				mostrarmensaje("Fallo al registrarse", "El nick de usuario no se ha recibido.",1);
				break;
			case -55:
				mostrarmensaje("Fallo al registrarse", "La longitud del nick debe estar entre ".MIN_NICK." y ".MAX_NICK." caracteres",1);
				break;
			case -56:
				mostrarmensaje("Fallo al registrarse", "El nick de usuario solo puede contener letras y números",1);
				break;
			case -57:
				mostrarmensaje("Fallo al registrarse", "Ya existe una persona con ese nick.",1);
				break;
			case 58:
				mostrarmensaje("Fallo al registrarse", "No se pudo comprobar el nick introducido",1);
				break;
		}
	}

	function vmostrarlogin($valor){
		switch ($valor) {
			case 0:
				mostrarmensaje("Inicio de sesión correcto", "Bienvenido/a ".$_SESSION['nombre']." ".$_SESSION['ap1']." ".$_SESSION['ap2']." a Merchacine",0);
				break;
			case -1:
				mostrarmensaje("Fallo al iniciar sesión", "Correo o contraseña no validos",1);
				break;
			case -2:
				mostrarmensaje("Fallo al iniciar sesión", "No se ha podido acceder a la base de datos, intentelo denuevo mas tarde",1);
				break;
			case -6:
				mostrarmensaje("Fallo al iniciar sesión", "La constraseña solo debe contener letras y números",1);
				break;
			case -8:
				mostrarmensaje("Fallo al iniciar sesión", "La longitud de la contraseña debe ser de al menos ".MIN_PASSWD." y como mucho ".MAX_PASSWD." carácteres",1);
				break;
			case -13:
				mostrarmensaje("Fallo al iniciar sesión", "La contraseña está vacia.",1);
				break;
			case -5:
				mostrarmensaje("Fallo al iniciar sesión", "Ha introducido un correo que no es válido",1);
				break;
			case -4:
				mostrarmensaje("Fallo al iniciar sesión", "La longitud del correo excede el límite de ".MAX_CORREO." carácteres",1);
				break;
			case -3:
				mostrarmensaje("Fallo al iniciar sesión", "El correo electrónico está vacio.",1);
				break;
			case -9:
				mostrarmensaje("Fallo al iniciar sesión", "Usted está bloqueado contacte con el administrador para obtener detalles.",1);
				break;
			case -10:
				mostrarmensaje("Fallo al iniciar sesión", "No puede realizar inicio de sesión aquí",1);
				break;
			case -31:
				mostrarmensaje("Fallo al iniciar sesión", "Debe hacer click en el Captcha",1);
				break;
		}
	}

	/** Funcion que muestra el estado de la sesión de un usuario en función del parámetro que recibe. **/
	function vmostrarsesion($valor){
		switch ($valor) {
			case 0:
				mostrarmensaje("Se ha cerrado su sesión", "Se ha cerrado sesión correctamente",0);
				break;
			case -6:
				mostrarmensaje("Se ha cerrado su sesión", "Ha excedido el tiempo límite de inactividad, vuelva a iniciar sesión.",0);
				break;
			case -1:
				mostrarmensaje("Se ha cerrado su sesión", "No se encuentra registrado en esta página.",0);
				break;
			case -2:
				mostrarmensaje("Se ha cerrado su sesión", "No se pudo obtener información de la base de datos.",0);
				break;
			case -3:
				mostrarmensaje("Se ha cerrado su sesión", "Ha sido bloqueado contacte con el administrador para obtener más información",0);
				break;
			case -5:
				mostrarmensaje("Se ha cerrado su sesión", "Inicie sesión en la parte de administrador",0);
				break;
			case -8080:
				mostrarmensaje("Fallo de conexión", "No se ha podido establecer conexión con la base de datos. Su sesión ha sido cerrada.",0);
				break;
		}
	}

	/** Función que muestra si un usuario esta verificado o no y si puede verificarse de sesión de un usuario:  **/
	function vmostrarverificar(){
		$valor = $_SESSION['tipo'];
		if($valor == 0)
			echo file_get_contents("solicitarverificacion.html");
		elseif($valor == 1)
			mostrarmensaje("Ya ha enviado una solicitud de verificación","Usted ya ha enviado una solicitud de verificación, espere el resultado de la misma.",0);
		elseif($valor == 2)
			mostrarmensaje("Es un usuario verificado","Usted ya está verificado en Merchacine, no necesita volverse a verificar",0);
		elseif($valor == 3)
			mostrarmensaje("Es un administrador","Usted es un administrador Merchacine, no necesita verificarse",0);
	}

	/** Función que muestra el resultado de enviar una solicitud de verificación **/
	function vmostrarresultadoverificacion($valor){
		if($valor == 0)
			mostrarmensaje("Solicitud enviada correctamente","Su solicitud de verificación ha sido enviada correctamente, en unos días recibirá una respuesta.",0);
		elseif($valor == -1)
			mostrarmensaje("No se pudo enviar su solictud","Su solicitud no pudo ser enviada, intentelo de nuevo mas tarde",0);
		elseif($valor == -2)
			mostrarmensaje("No se pudo verificar","En estos momentos no se le puede verificar, inténtelo de nuevo más tarde.",0);
		elseif($valor == -3)
			mostrarmensaje("El texto no cumple el tamaño establecido","El texto debe tener una longitud mínima de ".MIN_TEXT." y como máximo de ".MAX_TEXT. "carácteres, su texto contiene ".$aux = strlen(preg_replace('/\s/', '', $_POST["textoexplicacion"]))." carácteres",1);
		elseif($valor == -4)
			mostrarmensaje("El texto está vacio","Debe rellenar el texto para poder enviar una solicitud de verificación.",1);
	}

	/** Función que muestra el resultado de cambiar la contraseña por parte de un usuario **/
	function vcambiopassword($resultado){
		switch($resultado){
			case 0:
				mostrarmensaje("Cambio de contraseña correcto","Se ha cambiado su contraseña correctamente, vuelva a iniciar sesión",0);
				break;
			case -1:
				mostrarmensaje("No se pudo actualizar la contraseña","No se ha podido modificar su contraseña, intentelo de nuevo más tarde",1);
				break;
			//CONTRASEÑA ACTUAL:
			case -2:
				mostrarmensaje("No se pudo actualizar la contraseña", "La contraseña actual no coincide",1);
				break;
			case -3:
				mostrarmensaje("No se pudo actualizar la contraseña", "La contraseña nueva es igual a la actual",1);
				break;
			case -16:
				mostrarmensaje("No se pudo actualizar la contraseña", "La constraseña actual solo pede contener letras mayúsculas, minúsculas y números",1);
				break;
			case -18:
				mostrarmensaje("No se pudo actualizar la contraseña", "La longitud de la contraseña actual debe ser de al menos ".MIN_PASSWD." y como máximo ".MAX_PASSWD." carácteres",1);
				break;
			case -23:
				mostrarmensaje("No se pudo actualizar la contraseña", "La contraseña actual está vacia",1);
				break;
			//NUEVA CONSTRASEÑA:
			case -4:
				mostrarmensaje("No se pudo actualizar la contraseña", "Las contraseñas nuevas no coinciden",1);
				break;
			case -5:
				mostrarmensaje("No se pudo actualizar la contraseña", "La constraseña de confirmación solo pede contener letras mayúsculas, minúsculas y números",1);
				break;
			case -6:
				mostrarmensaje("No se pudo actualizar la contraseña", "La constraseña solo pede contener letras mayúsculas, minúsculas y números",1);
				break;
			case -7:
				mostrarmensaje("No se pudo actualizar la contraseña", "La longitud de la contraseña de confirmación debe ser de al menos ".MIN_PASSWD." y como máximo ".MAX_PASSWD." carácteres",1);
				break;
			case -8:
				mostrarmensaje("No se pudo actualizar la contraseña", "La longitud de la contraseña debe ser de al menos ".MIN_PASSWD." y como máximo ".MAX_PASSWD." carácteres",1);
				break;
			case -12:
				mostrarmensaje("No se pudo actualizar la contraseña", "La constraseña de confirmación está vacia",1);
				break;
			case -13:
				mostrarmensaje("No se pudo actualizar la contraseña", "La contraseña está vacia",1);
				break;
		}
	}

	/** Función que muestra el resultado del proceso de actualización de imagen de perfil de un usario:**/
	function vcambioimagen($resultado){
		switch($resultado){
			case 0:
				mostrarmensaje("Cambio de imagen de perfil correcto","Se ha actualizado su foto de perfil correctamente",0);
				break;
			case -1:
				mostrarmensaje("No se pudo actualizar su imagen de perfil","No se ha podido actualizar su imagen de perfil, intentelo de nuevo más tarde",0);
				break;
			case -2:
				mostrarmensaje("No se pudo actualizar su imagen de perfil", "El formato de la imagen debe ser jpg, jpeg o png",0);
				break;
			case -3:
				mostrarmensaje("No se pudo actualizar su imagen de perfil", "La imagen debe ser menor o igual a ".(MAX_IMAGEN/1000000)." MB",0);
				break;
			case -4:
				mostrarmensaje("No se pudo actualizar su imagen de perfil", "El fichero seleccionado no es una imagen",0);
				break;
			case -5:
				mostrarmensaje("No se pudo actualizar su imagen de perfil", "El fichero está vacio",0);
				break;
		}
	}

	/** Función que permite mostrar las provincias obtenidas de la BBDD **/
	function vmostrarprovincias($provincias){
		if(!empty($provincias)){ //Comprobar que las pronvicias no sean vacias:
			$cadena = file_get_contents('provincia.html');

			$trozos = explode('##fila##', $cadena);

			$aux = "";
			$cuerpo = "";
			while ($provincia = $provincias->fetch_assoc()) {
				$aux = $trozos[1];
				$aux = str_replace('##idprovincia##', $provincia['id_provincia'], $aux);
				$aux = str_replace('##provincia##',  $provincia['provincia'], $aux);

				$cuerpo .= $aux;
			}
			$cadena = $trozos[0] . $cuerpo . $trozos[2];

			echo $cadena;
		}	
		else //Provincias no detectadas.
			mostrarmensaje("No se han podido obtener las provincias","No se ha podido obtener el listado con las provincias",1);
	}

	/** Funcion que permite mostrar los municipios recogidos de la BBDD de acuerdo a un id de provincia seleccionado por el usuario **/
	function vmostrarmunicipios($municipios){
		if(!empty($municipios)){ //Comprobar que las pronvicias no sean vacias:
			$cadena = file_get_contents('municipio.html');

			$trozos = explode('##fila##', $cadena);

			$aux = "";
			$cuerpo = "";
			while ($municipio = $municipios->fetch_assoc()) {
				$aux = $trozos[1];
				$aux = str_replace('##idmunicipio##', $municipio['id_municipio'], $aux);
				$aux = str_replace('##municipio##',  $municipio['municipio'], $aux);

				$cuerpo .= $aux;
			}
			$cadena = $trozos[0] . $cuerpo . $trozos[2];

			echo $cadena;
		}	
		else //Municipios no detectados.
			mostrarmensaje("No se han podido obtener los municipios","No se ha podido obtener el listado con los municipios para la provincia seleccionada",1);

	}

	/**Función que permite mostrar la pantalla de descuentos sobre la que se colocarán los descuentos de un usuario **/
	function vmostrarlistadodescuentos(){
		$cadena = file_get_contents("listadescuentos.html");
		$cadena = str_replace("##nombre##", $_SESSION['nombre'], $cadena);
		$cadena = str_replace("##apellido1##", $_SESSION['ap1'], $cadena);
		$cadena = str_replace("##apellido2##", $_SESSION['ap2'], $cadena);

		echo $cadena;
	}

	/** Función que permite mostrar los descuentos disponibles para un usuario **/
	function vmostrardescuentos($descuentos,$numdescuentos){
		$cadena = file_get_contents("descuentos.html");
		$trozos = explode("##fila##",$cadena);

		$aux = "";
		$cuerpo = "";

		if(isset($_GET["pagina"])){
			$pagina_actual = $_GET["pagina"]; //Obtener la página actual 
			if(!empty($descuentos)){ //Comprobar que el parámetro entradas no sea nulo
					while($descuento = $descuentos->fetch_assoc()){ //Leer las entradas recuperadas y sustituirlas en el listado iterando
						$aux = $trozos[1];
						$aux = str_replace("##cantidad##", $descuento["cantidad"], $aux);
						$aux = str_replace("##cine##", $descuento["cine"], $aux);
						$aux = str_replace("##dia##", $descuento["dia"], $aux);
						$aux = str_replace("##mes##", $descuento["mes"], $aux);
						$aux = str_replace("##ano##", $descuento["ano"], $aux);
						$aux = str_replace("##id_descuento##", $descuento["id_descuento"], $aux);
						$cuerpo .= $aux;
					}

					$cadena = $trozos[0] . $cuerpo . $trozos[2];

					//Llegado a este punto ya estarían mostradas todas las entradas permitidas.

					//Hay que paginar la página actual
					if(!empty($numdescuentos)){ //Comprobar que el parámetro numdescuentos no sea nulo
						$datos = $numdescuentos->fetch_assoc();
						$numero_elementos = $datos["num_descuentos"];
						paginar($cadena,$numero_elementos,$pagina_actual);
					}
					else //Numero de entradas nulo
						mostrarmensaje("No se detectó en número de descuentos", "No se recibió el número de descuentos a mostrar",1);
			}
			else //Si el parámetro entradas es nulo:
				mostrarmensaje("No hay descuentos disponibles", "Actualmente no tiene ningún descuento disponible.",1);
		}
		else
			mostrarmensaje("No se pudo obtener la página actual", "No se encontró el número de la página actual",1);
	}

	function vmostrardescuentoelegido($resultado){
		/*Funciones PDF:
		pdf->SetFont(Establece una fuente para las siguientes celdas hasta que se establezca una nueva fuente)
		pdf->Cell (crear una nueva entrada de texto en el pdf)
		pdf->Cell(0,0,$pdf->Image($qr, $pdf->GetX()+70, $pdf->GetY()+20),0,0) para insertar imágenes en las celdas
		*/
		$datos = $resultado[1]->fetch_assoc();
		$datos_descuento = $resultado[0]->fetch_assoc();
		$nombre = $datos["nombre"];
		$apellido1 = $datos["apellido1"];
		$apellido2 = $datos["apellido2"];
		$cantidad = $datos_descuento["cantidad"];
		$cine = $datos_descuento["cine"];
		$dia = $datos_descuento["dia"];
		$mes = $datos_descuento["mes"];
		$ano = $datos_descuento["ano"];
		$id_descuento = $datos_descuento["id_descuento"];
		$pdf = new FPDF();
		$pdf->AddPage();
		$pdf->SetFont('Arial','B',16);
		$pdf->Cell(0,10,"DESCUENTO DISPONIBLE",1,1,'C');

		$pdf->SetFont('Arial','B',16);
		$pdf->Cell(50,10,'NOMBRE',1,0,'L');
		$pdf->SetFont('Arial','I',16);
		$pdf->Cell(0,10,$nombre,1,1,'C');

		$pdf->SetFont('Arial','B',16);
		$pdf->Cell(50,10,'APELLIDOS',1,0,'L');
		$pdf->SetFont('Arial','I',16);
		$pdf->Cell(0,10,"$apellido1 $apellido2",1,1,'C');

		$pdf->SetFont('Arial','B',16);
		$pdf->Cell(50,10,'CINE',1,0,'L');
		$pdf->SetFont('Arial','I',16);
		$pdf->Cell(0,10,$cine,1,1,'C');

		$pdf->SetFont('Arial','B',16);
		$pdf->Cell(50,10,'CANTIDAD',1,0,'L');
		$pdf->SetFont('Arial','I',16);
		$pdf->Cell(0,10,"$cantidad%",1,1,'C');

		$pdf->SetFont('Arial','B',16);
		$pdf->Cell(80,10,'DISPONIBLE HASTA EL',1,0,'L');
		$pdf->SetFont('Arial','I',16);
		$pdf->Cell(0,10,"$dia/$mes/$ano",1,1,'C');

		$qr = generar_qr($cantidad,$cine,$nombre,$apellido1,$apellido2,$id_descuento);
		$pdf->Cell(0,0,$pdf->Image($qr, $pdf->GetX()+70, $pdf->GetY()+20),0,1,'C');

		$pdf->SetFont('Arial','B',8);
		$pdf->Cell(50,10,"Mostrar este qr en el puesto de venta de entradas del cine $cine para que se aplique el descuento correspondiente.",0,1,'L');
		$pdf->Cell(50,0,"El descuento NO se aplica para compras online.",0,0,'L');

		$pdf->output();


	}

	function generar_qr($cantidad,$cine,$nombre,$apellido1,$apellido2,$id_descuento){
		$dir = 'qrs/';
		$filename = $dir."$apellido1"."$apellido2"."$id_descuento".'.png';
		if (file_exists($filename)){
			return $filename;
		}
		$tam = 3;
		$level = 'M';
		$frameSize = 3;
		$contenido = "La empresa encargada de la gestión de la página web Merchacine se hace cargo de un descuento del $cantidad% en los cines '$cine' para la persona: $nombre,$apellido1,$apellido2";
		QRcode::png($contenido,$filename,$level,$tam,$frameSize);
		return $filename;
	}


	function vmostrarforos($resultado){
			if (isset($_SESSION['user'])){
				$cadena = file_get_contents("listapost_registrados.html");
				$correo_usuario = $_SESSION['user'];
			}else{
				$cadena = file_get_contents("listapost_no_registrados.html");
				$correo_usuario = "anonimo";
			}
			$trozos = explode('##fila##', $cadena);
			$aux = "";
			$cuerpo = "";
			$cola = "";
			if(isset($_SESSION['tipo']))
				$tipo_usuario = $_SESSION['tipo'];
			else
				$tipo_usuario = -1;
			while ($datos = $resultado->fetch_assoc()) {
				$aux = $trozos[1];
				$aux = str_replace("##titulo##", $datos["titulo"], $aux);
				$aux = str_replace("##idpost##", $datos["id_post"], $aux);
				$aux = str_replace("##creador##", $datos["nick"], $aux);
				if($correo_usuario == $datos["correo_creador"])
					$aux = str_replace("##tipo##", 1, $aux);
				else
					$aux = str_replace("##tipo##", 0, $aux);
				$aux = str_replace("##ncomentarios##", $datos["ncomentarios"], $aux);
				$aux = str_replace("##Fecha##", $datos["fecha"], $aux);
				$cuerpo .= $aux;
			}
			$cola .= $trozos[2];
			if ($tipo_usuario == 2 or $tipo_usuario == 0 or $tipo_usuario == 1){
				$cola .= $trozos[3];
			}

			echo $trozos[0] . $cuerpo . $cola .$trozos[4];
	}

	function vmostrarforo($resultados){
		if(!empty($resultados)){
			if (isset($_SESSION['user'])){
				$cadena = file_get_contents("post_registrados.html");
				$correo_usuario = $_SESSION['user'];
			}else{
				$cadena = file_get_contents("post_no_registrados.html");
				$correo_usuario = "anonimo";
			}
			$trozos = explode("##tipousuario##", $cadena);
			$aux1 = "";
			$aux2 = "";

			if(isset($_SESSION['tipo']))
				$tipo_usuario = $_SESSION['tipo'];
			else
				$tipo_usuario = -1;
			
			//Como solo tenemos un resultado, podemos hacer lo siguiente:
			$datos_foro = $resultados -> fetch_assoc();
			$aux1 .= $trozos[0]; 
			$aux1 = str_replace("##titulo##", $datos_foro["titulo"], $aux1);
			$aux1 = str_replace("##creador##", $datos_foro["nick"], $aux1);
			$aux1 = str_replace("##idpost##", $datos_foro["id_post"], $aux1);
			if($correo_usuario == $datos_foro["correo_creador"])
					$aux1 = str_replace("##tipo##", 1, $aux1);
				else
					$aux1 = str_replace("##tipo##", 0, $aux1);
			if ($tipo_usuario == 2 or $tipo_usuario == 0 or $tipo_usuario == 1){
				$aux2.=$trozos[1];
				$aux2 = str_replace("##idpost##", $datos_foro["id_post"], $aux2);
			}

			echo $aux1 . $aux2 . $trozos[2];
		}
		else
			mostrarmensaje("No se encontró la información", "No se ha encontrado la información del post seleccionado",1);
	}

	/** Función que permite mostrar los comentarios asociados a un post determinado **/
	function vmuestracomentarios($comentarios){
		if(!empty($comentarios)){
			if (isset($_SESSION['user'])){
				$cadena = file_get_contents("comentarios_registrado.html");
				$correo_usuario = $_SESSION['user'];
			}else{
				$cadena = file_get_contents("comentarios_no_registrado.html");
				$correo_usuario = "anonimo";
			}

			$trozos = explode("##fila##", $cadena);

			$aux= "";
			$cuerpo ="";
			while ($datos_comentarios = $comentarios->fetch_assoc()){
				$aux = $trozos[1];
				$aux = str_replace("##comentario##", $datos_comentarios["comentario"], $aux);
				$aux = str_replace("##escritor##", $datos_comentarios["nick"], $aux);
				$aux = str_replace("##Fecha##", $datos_comentarios["fecha"], $aux);
				if($correo_usuario == $datos_comentarios["correo_escritor"])
					$aux = str_replace("##tipo##", 1, $aux);
				else
					$aux = str_replace("##tipo##", 0, $aux);
				$cuerpo .= $aux;
			}
			$cadena = $trozos[0] . $cuerpo. $trozos[2];

			echo $cadena;
		}
		else
			mostrarmensaje("No hay comentarios para mostrar", "No existen o no se han encontrado comentarios para mostrar",1);
	}

	function vbuscarforo($resultado,$numeropost) {
		if(isset($_GET['pagina'])){
			if(isset($_SESSION['user'])){
				$cadena = file_get_contents("foro_parcial_registrado.html");
				$correo_usuario = $_SESSION['user'];
			}
			else{
				$cadena = file_get_contents("foro_parcial_no_registrado.html");
				$correo_usuario = "anonimo";
			}

			$trozos = explode('##fila##', $cadena);
			$aux = "";
			$cuerpo = "";
			while ($datos = $resultado->fetch_assoc()) {
				$aux = $trozos[1];
				$aux = str_replace("##titulo##", $datos["titulo"], $aux);
				$aux = str_replace("##idpost##", $datos["id_post"], $aux);
				$aux = str_replace("##creador##", $datos["nick"], $aux);
				if($correo_usuario == $datos["correo_creador"])
					$aux = str_replace("##tipo##", 1, $aux);
				else
					$aux = str_replace("##tipo##", 0, $aux);
				$aux = str_replace("##ncomentarios##", $datos["ncomentarios"], $aux);
				$aux = str_replace("##Fecha##", $datos["fecha"], $aux);
				$cuerpo .= $aux;
			}
			$cadena =  $trozos[0] . $cuerpo . $trozos[2];
			$pagina_actual = $_GET['pagina'];
			//Hay que paginar la página actual
			if(!empty($numeropost)){ //Comprobar que el parámetro numusuarios no sea nulo
				$datos = $numeropost->fetch_assoc();
				$numero_elementos = $datos["num_posts"];
				paginar($cadena,$numero_elementos,$pagina_actual);
			}
			else
				mostrarmensaje("No se detectó en número de posts", "No se recibió el número de posts a mostrar",1);
		}
		else
			mostrarmensaje("No se pudo obtener la página actual", "No se encontró el número de la página actual",1);
	}

	function vcrearforo(){
		echo file_get_contents("nuevopost.html");
	}

	function veliminarcritica($valor){
		if ($valor == 1) {
			mostrarmensaje("Eliminacion exitosa", "Se ha borrado la critica correctamente",0);
		} else {
			mostrarmensaje("Eliminacion fallida", "No se ha borrado la critica correctamente",0);
		}	
	}

	function veditarcritica($resultado){
		if(!empty($resultado)){
			$cadena = file_get_contents("editarcritica.html");
			$datos = $resultado->fetch_assoc();
			$cadena = str_replace("##Entrada##", $datos["id_entrada"], $cadena);
			$cadena = str_replace("##titulo##", $datos["titulo"], $cadena);
			$cadena = str_replace("##critica##", $datos["critica"], $cadena);
			$cadena = str_replace("##identrada##", $datos["id_entrada"], $cadena);
			$cadena = str_replace("##Autor##", $_SESSION['nick'], $cadena);
			$valoracion = $datos["valoracion"];
			$trozos = explode("##Puntuacion##", $cadena);
			$aux = "";
			$cuerpo="";
			for ($i = 1; $i <= PUNTUACION_MAXIMA; $i++){
				$aux = $trozos[1];
				$aux = str_replace("##Valor##", $i, $aux);
				if($i == $valoracion)
					$aux = str_replace("##Seleccionada##", "selected", $aux);
				else
					$aux = str_replace("##Seleccionada##", "", $aux);
				$cuerpo .= $aux;
			}
			
			echo $trozos[0]. $cuerpo . $trozos[2];
		}
		else
			mostrarmensaje("No se pudo obtener la crítica","No se pudo obtener la crítica seleccionada, intentelo más tarde",0);
	}

	function vmodificarcritica($valor){
		switch ($valor) {
			case 0:
				mostrarmensaje("Crítica modificada correctamente", "Se ha modificado su crítica correctamente",1);
				break;
			case -1:
				mostrarmensaje("Error al modificar su crítica", "No se ha recibido el texto a modificar",1);
				break;
			case -2:
				mostrarmensaje("Error al modificar su crítica", "No se ha detectado la entrada a la que pertenece la crítica",1);
				break;
			case -3:
				mostrarmensaje("Error al modificar su crítica", "No se ha recibido la valoración de la crítica",1);
				break;
			case -4:
				mostrarmensaje("Error al modificar su crítica", "No se ha podido almacenar la modificación de su crítica",1);
				break;
		}
	}

	/** Función que muestra el número de notificaciones que tiene un usuario en un momento determinado **/
	function vnotificaciones($notificaciones){
		if(!empty($notificaciones)){
			$num_notificaciones = $notificaciones -> fetch_assoc();
			
			if($num_notificaciones['num_notificaciones'] > 1)
				echo "Tiene usted ".$num_notificaciones['num_notificaciones']." notificaciones ¿Desea verlas?";
			elseif($num_notificaciones['num_notificaciones'] == 1)
				echo "Tiene usted ".$num_notificaciones['num_notificaciones']." notificación ¿Desea verla?";
		}
	}

		/** Funcion que permite mostrar las notificaciones de un usuario **/
	function vmostarnotificaciones($notificaciones, $numero_notificaciones){
		if(!empty($notificaciones)){ //Notificaciones recibidas:
			if(!empty($numero_notificaciones)){
				$num_notificaciones = $numero_notificaciones -> fetch_assoc();
				if($num_notificaciones['num_notificaciones'] == 0){
					mostrarmensaje('No tiene notificaciones', 'Actualmente no tiene notificaciones',1);
				}
				else{
					$cadena = file_get_contents('notificaciones.html');
					$trozos = explode("##fila##",$cadena);

					$aux = "";
					$cuerpo = "";

					if(isset($_GET["pagina"])){
						$pagina_actual = $_GET["pagina"]; //Obtener la página actual 
						while($notificacion = $notificaciones->fetch_assoc()){ //Leer las notificaciones recuperadas y sustituirlas en el listado iterando
							$aux = $trozos[1];
							$aux = str_replace("##Titulo##", $notificacion['titulo'], $aux);
							$aux = str_replace("##Descripcion##", substr($notificacion['texto'],0,MAX_DESC_NOTIFICACION).'...', $aux);
							$aux = str_replace("##Fecha##", $notificacion['fecha'], $aux);
							$aux = str_replace("##Notificacion##", $notificacion['id_notificacion'], $aux);
							$cuerpo .= $aux;
						}

						$cadena = $trozos[0] . $cuerpo . $trozos[2];

						//Llegado a este punto ya estarían mostradas todas las notificaciones permitidas.

						//Hay que paginar la página actual
						$numero_elementos = $num_notificaciones['num_notificaciones'];
						paginar($cadena,$numero_elementos,$pagina_actual);
					}
					else
						mostrarmensaje("No se pudo obtener la página actual", "No se encontró el número de la página actual",1);
				}
			}
			else
				mostrarmensaje("No se han encontrado el número de notificaciones","No se han encontrado el número de notificaciones en la base de datos.",0);
		}
		else
			mostrarmensaje("No se han encontrado notificaciones","No se han encontrado notificaciones en la base de datos.",0);
	}

	/** Funcion que permite mostrar una notificación del usuario **/
	function vnotificacion($notificacion){
		if(!empty($notificacion)){
			$cadena = file_get_contents('notificacion.html');

			$info_notificacion = $notificacion -> fetch_assoc();

			$cadena = str_replace("##Titulo##", $info_notificacion['titulo'], $cadena);
			$cadena = str_replace("##Texto##", $info_notificacion['texto'], $cadena);
			$cadena = str_replace("##Fecha##", $info_notificacion['fecha'], $cadena);

			echo $cadena;
		}
		else
			mostrarmensaje("Notificación no encontrada", "No se ha encontrado la notificación seleccionada",0);
	}

	/** Función que muestra el resultado de eliminar una notificación de la BBDD **/
	function velimnarnotificacion($valor){
		switch($valor){
			case 0:
				mostrarmensaje("Notificación eliminada correctamente","Se ha eliminado la notificación correctamente.",1);
				break;
			case -1:
				mostrarmensaje("No se ha podido eliminar la notificación","No se ha podido eliminar la notificación, intentelo de nuevo más tarde.",1);
				break;
			case -2:
				mostrarmensaje("No se ha encontrado la notificación", "No se ha podido encontrar la notificación seleccionada.",1);
				break;
		}
	}

	/**Función que muestra una crítica seleccionada por el usuario **/
	function vmuestracritica($consultas){
		if(!empty($consultas)){
			if(isset($_POST['entrada'])){
				$consulta = $consultas[0];
				$consulta2 = $consultas[1];
				$critica = $consulta ->fetch_assoc();
				$entrada = $consulta2->fetch_assoc();
				if (isset($_SESSION['user'])){
					$cadena = file_get_contents('vercritica_registrado.html');
					$correo_sesion = $_SESSION['user'];
				}else{
					$cadena = file_get_contents('vercritica_no_registrado.html');
					$correo_sesion = "anonimo";
				}
				$cadena = str_replace('##titulo##', $entrada['titulo'], $cadena);
				$cadena = str_replace('##Critica##', $critica['critica'], $cadena);
				$cadena = str_replace('##Autor##',$critica['nick'], $cadena);
				$cadena = str_replace('##Fecha##', $critica['fecha'], $cadena);
				$cadena = str_replace('##valoracion##', $critica['valoracion'], $cadena);
				$cadena = str_replace('##Entrada##', $_POST['entrada'], $cadena);
				if($correo_sesion == $critica['correo_creador'])
					$cadena = str_replace("##tipo##", 1, $cadena);
				else
					$cadena = str_replace("##tipo##", 0, $cadena);

				echo $cadena;
			}
			else
				mostrarmensaje("No se ha encontrado la entrada", "No se pudo obtener la entrada que contiene la crítica seleccionada",0);
		}
		else
			mostrarmensaje("No se ha encontrado la crítica", "No se pudo obtener la información de la crítica seleccionada",0);
	}

	function vcambiodatosperfil($resultado){
		switch($resultado){
			case 0:
				echo("Datos personales modificados correctamente");
				break;
			case 1:
				echo("No ha modificado ningún dato personal");
				break;
			case -1:
				mostrarmensaje("Error al modificar sus datos","El nombre solo puede tener letras y números",1);
				break;
			case -2:
				mostrarmensaje("Error al modificar sus datos", "La longitud del nombre debe ser de al menos ".MIN_NOMBRE." y como mucho ".MAX_NOMBRE." carácteres",1);
				break;
			case -3:
				mostrarmensaje("Fallo al modificar sus datos", "El primer apellido solo puede contener letras mayúsculas y minúsculas",1);
				break;
			case -4:
				mostrarmensaje("Fallo al modificar sus datos", "El primer apellido debe tener al menos ".MIN_APELLIDO." y como maximo ".MAX_APELLIDO." caracteres",1);
				break;
			case -5:
				mostrarmensaje("Fallo al modificar sus datos", "El segundo apellido solo puede contener letras mayúsculas y minúsculas",1);
				break;
			case -6:
				mostrarmensaje("Fallo al modificar sus datos", "El segundo apellido debe tener al menos ".MIN_APELLIDO." y como maximo ".MAX_APELLIDO." caracteres",1);
				break;
			case -7:
				mostrarmensaje("Fallo al modificar sus datos", "El nick seleccionado ya está en uso",1);
				break;
			case -8:
				mostrarmensaje("Fallo al modificar sus datos", "No se pudo comprobar el nick seleccionado, intentelo más tarde.",1);
				break;
			case -9:
				mostrarmensaje("Fallo al modificar sus datos", "El nick solo puede contener letras y números",1);
				break;
			case -10:
				mostrarmensaje("Fallo al modificar sus datos", "La longitud del nick debe estar entre ".MIN_NICK." y ".MAX_NICK." caracteres",1);
				break;
			case -11:
				mostrarmensaje("Fallo al modificar sus datos", "La dirección debe tener al menos ".MIN_DIRECCION." y como máximo ".MAX_DIRECCION." carácteres",1);
				break;
			case -15:
				mostrarmensaje("Fallo al modificar sus datos", "No se han podido guardar los cambios, intentelo de nuevo más tarde.",1);
				break;
			case -13:
				mostrarmensaje("Fallo al modificar sus datos", "Debe seleccionar un municipio si desea cambiar de provincia.",1);
				break;
			case -33:
				mostrarmensaje("Fallo al modificar sus datos", "No se ha encontrado la provincia seleccionada.",1);
				break;
			case -34:
				mostrarmensaje("Fallo al modificar sus datos", "No se ha podido comprobar la provincia seleccionada.",1);
				break;
			case -43:
				mostrarmensaje("Fallo al modificar sus datos", "No se ha encontrado el municipio seleccionado.",1);
				break;
			case -44:
				mostrarmensaje("Fallo al modificar sus datos", "No se ha podido comprobar el municipio seleccionado.",1);
				break;
		}
	}

	function vactualizarimagenes($resultado){
		$principal = $resultado[0];
		$secundarias = $resultado[1];
		$datos_principal = $principal->fetch_assoc();
		$imagen_principal = '<img id="'.$datos_principal['id_imagen'].'" heigh="500" width="500" src="data:image;base64,'.$datos_principal['imagen'].' " onclick="setPrincipal(this.id)"><br>';


		$imagenes_secundarias = "";
		while ($datos_secundarias = $secundarias->fetch_assoc()){
			$sus_imagen = '<img id="'.$datos_secundarias['id_imagen'].'" heigh="200" width="200" src="data:image;base64,'.$datos_secundarias['imagen'].' " onclick="setPrincipal(this.id)"> ';
			$imagenes_secundarias .= $sus_imagen;
		}

		echo $imagen_principal . $imagenes_secundarias . '<br>';
	}

	/** Función que muestra el resultado de escribir un comentario en un post del foro **/
	function vescribircomentario($resultado){
		switch($resultado){
			case 0:
				echo("Comentario añadido correctamente");
				break;
			case -1:
				echo("No se ha podido añadir el comentario, intentlo más tarde");
				break;
			case -2:
				echo("El comentario no se ha podido añadir no se ha recibido el id del post, no cumple la longitud establecida (mínimo ".MIN_COMENTARIO." y máximo ".MAX_COMENTARIO." carácteres)");
				break;
			case -3:
				echo("El comentario no se ha podido añadir no se ha recibido el id del post.");
				break;
			case -4:
				echo("El comentario no se ha podido añadir no se ha recibido el id del post, el comentario está vacio");
				break;
		}
	}

	/** Funcion que permite mostrar el resultado de borrar todas las notificaciones de un usuario **/
	function veliminartodasnotificaciones($resultado){
		switch($valor){
			case 0:
				mostrarmensaje("Notificaciones eliminadas correctamente","Se han eliminado la notificaciones correctamente.",1);
				break;
			case -1:
				mostrarmensaje("No se ha podido eliminar las notificaciones","No se ha podido eliminar las notificaciones, intentelo de nuevo más tarde.",1);
				break;
		}
	}

	/** Funcion que permite mostrar el perfil de un usuario ajeno **/
	function vmostrarperfilajeno($resultado){
		if(!empty($resultado)){
			$usuario = $resultado[0];
			$entradas = $resultado[1];
			$criticas = $resultado[2];
			$valoracion = $resultado[3];

			$cadena1 = file_get_contents("perfil_ajeno.html");
			$datos_usuario = $usuario -> fetch_assoc();

			$cadena1 = str_replace("##Nick##", $datos_usuario['nick'], $cadena1);
			$cadena1 = str_replace("##nombre##", $datos_usuario['nombre'], $cadena1);
			$cadena1 = str_replace("##apellido1##", $datos_usuario['apellido1'], $cadena1);
			$cadena1 = str_replace("##apellido2##", $datos_usuario['apellido2'], $cadena1);

			$trozos = explode('##im_perfil##', $cadena1);
			if ($datos_usuario['imagen_perfil'] == null){
				$texto = "El usuario no tiene imagen de perfil";
				$cadena2 = $trozos[0] . $texto . $trozos[2];
			}else{
				$cadena2 = $trozos[0] . $trozos[1] . $trozos[2];
				$cadena2 = str_replace("##imagen##", base64_encode($datos_usuario['imagen_perfil']), $cadena2);
			}
			if($datos_usuario['tipo'] == 2 || $datos_usuario['tipo'] == 3){
				$num_entradas = $entradas -> fetch_assoc();
				$num_criticas = $criticas -> fetch_assoc();
				$val = $valoracion -> fetch_assoc();

				$trozos = explode('##tipo##', $cadena2);
				$trozos[1] = str_replace("##frasetipo##", 'El usuario se encuentra verificado en Merchacine', $trozos[1]);
				$trozos[1] = str_replace("##entradas##", $num_entradas['num_entradas'], $trozos[1]);
				$trozos[1] = str_replace("##criticas##", $num_criticas['num_criticas'], $trozos[1]);
				$trozos[1] = str_replace("##valoracion##", $val['val_media'], $trozos[1]);
				$cadena = $trozos[0] . $trozos[1] . $trozos[2];
			}else{
				$trozos = explode('##tipo##', $cadena2);
				$cadena = $trozos[0] . $trozos[2];
			}
			echo $cadena;
		}
		else
			mostrarmensaje("No se ha encontrado al usuario", "No se ha encontrado al usuario seleccionado",0);
	}
?>