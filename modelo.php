<?php 
	include_once("constantes.php");
	require("fpdf181/fpdf.php");
	require("phpqrcode/qrlib.php");
	//Añadir la libreria para los Captcha
	require_once "recaptchalib.php";

	/** Funcion que realiza la conexión con la base de datos **/
	function conexion() {
		mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT); //Permitir que mysli lance excepciones.
		try{
			//$con = mysqli_connect("localhost", "root", "","final_merchacine");
			$con = mysqli_connect("dbserver", "grupo27", "EY9iapae0e","db_grupo27");
		}catch(mysqli_sql_exception $e){
			return null;
		}
		return $con;
	}


	/***Funcion que contiene la logica del proceso de registro de la pagina web:
		1.Comprobar que ningun campo es vacio.
		2.Comprobar que las direcciones de email son validas.
		3.Comprobar que los emails coinciden
		4.Comprobar la longitud de las contraseñas
		5.Comprobar que las contraseñas solo tienen numeros y letras.
		6.Comprobar que las constraseñas coinciden.
		7.Buscar en la BBDD paa evitar coincidencias de email.
		8.Insertar el email en la BBDD
		***/
	function mvalidarregistro(){
		$con = conexion();

		if($con == null){ //No hay conexión
			mcierrasesion();
			return -8080;
		}

		$captcha = mcheckcaptcha();
		if($captcha == 0){ //El usuario ha hecho click en el Captcha
			if(isset($_POST["nombreUsuario"])){ //Comprobar que el nombre no sea nulo
				$nombre = $_POST["nombreUsuario"];
				$aux = strlen(preg_replace('/\s/', '', $nombre)); //Eliminar todos los espacios en blanco
				if($aux >= MIN_NOMBRE && $aux <= MAX_NOMBRE){
					if(!preg_match('/[^A-Za-z]/', $nombre)){
						if(isset($_POST["apellido1"])){//Comprobar que le primer apellido no sea nulo
							$ap1 = $_POST["apellido1"];
							$aux = strlen(preg_replace('/\s/', '', $ap1)); //Eliminar todos los espacios en blanco
							if($aux >= MIN_APELLIDO && $aux <= MAX_APELLIDO){
								if(!preg_match('/[^A-Za-z]/', $ap1)){
									if(isset($_POST["apellido2"])){//Comprobar que el segundo apellido no sea nulo
										$ap2 = $_POST["apellido2"];
										$aux = strlen(preg_replace('/\s/', '', $ap2)); //Eliminar todos los espacios en blanco
										if($aux >= MIN_APELLIDO && $aux <= MAX_APELLIDO){
											if(!preg_match('/[^A-Za-z]/', $ap2)){
												//Comprobar los emails:
												$check_emails = mcheckemails();
												if($check_emails == 0){ 
													$correo = $_POST["correo"];
													$correo_conf =$_POST["correo_conf"];
													if(isset($_POST['nick'])){ //Comprobar el nick de usuario:
														$nick = $_POST['nick'];
														$aux = strlen(preg_replace('/\s/', '', $nick)); //Eliminar todos los espacios en blanco
														if($aux >= MIN_NICK && $aux <= MAX_NICK){ //Comprobar la longitud del nick
															if(!preg_match('/[^A-Za-z0-9]/', $nick)){ //Comprobar que solo tiene letras y números
																//Comprobar que el nick no está repetido en la BBDD
																$consulta = "select count(nick) as num_nick from final_usuarios where nick = '$nick'";
																if($result = $con->query($consulta)){
																	$num_nick = $result -> fetch_assoc();
																	if($num_nick['num_nick'] != 0) 
																		return -57; //Nick en la BBDD
																}
																else //Consulta fallida
																	return -58;
																$check_provincia = mcheckprovincia(); //Comprobar la provincia seleccionada por el usuario
																if($check_provincia == 0){ //Provincia correcta:	
																	$provincia = $_POST['provincia'];
																	$check_municipio= mcheckmunicipio(); //Comprobar el municipio seleccionado por el usuario
																	if($check_municipio == 0){ //Municipio correcto:
																		$municipio = $_POST['municipio'];
																		if(isset($_POST['direccion'])){ //Comprobar que la direccíón no sea vacia:
																			$direccion = $_POST['direccion'];
																			$aux = strlen(preg_replace('/\s/', '', $direccion)); //Eliminar todos los espacios en blanco
																			if($aux >= MIN_DIRECCION && $aux <= MAX_DIRECCION){ //Comprobar longitud de la dirección:
																				$direccion = $_POST['direccion'];
																				$check_passwords = mcheckpasswords("password","password_conf");//Comprobar las passwords
																				if($check_passwords == 0){ 
																					$passwd = $_POST["password"];
																					$passwd_conf = $_POST["password_conf"];
																					//Buscar en la base de datos por si hay coincidencia de email.
																					$consulta = "select correo_electronico from final_usuarios where correo_electronico = '$correo'";
																					if ($resultado = $con->query($consulta)){
																						if (mysqli_num_rows($resultado) != 1){ //No hay ningun usuario en la BBDD con ese correo electronico:
																							$consulta_insert = "insert into final_usuarios (correo_electronico, password, nick, nombre, apellido1, apellido2, provincia, municipio, direccion) values ('$correo', '". md5($passwd) ."', '$nick', '$nombre', '$ap1', '$ap2', '$provincia', '$municipio', '$direccion')"; //Encriptar la constraseña con md5
																							if ($con->query($consulta_insert)) //Insert correcto
																								return 0;
																							else //Error al insertar
																								return -1;
																						}
																						else //Coincidencia de usuario en la BDD
																							return -2;
																					}
																					else //Error al realizar la query
																						return -3;
																				}
																				else //Comprobacion passwords:
																					return $check_passwords;
																			} 
																			else//Dirección no cumple la longitud
																				return -52;
																		}	
																		else //Dirección no vacia
																			return -53;	
																	}
																	else //Municipio incorrecta
																		return $check_municipio;
																} 
																else //Provincia incorrecta:	
																	return $check_provincia;
															}
															else //Nick contiene algo que no son letras y números
																return -56;
														}
														else //Longitud de nick no correcta:
															return -55;	
													}
													else //Nick de usuario vacio:
														return -54;	
												}
												else //Comprobacion emails:
													return $check_emails;
											}//Apellido 2 no solo letras
											else
												return -18;
										} //Apellido 2 no cumple longitud
										else 
											return -19;
									}//Apellido 2 nulo
									else
										return -20;
								}//Apellido 1 solo contiene letras
								else
									return -21;
							} //Apellido 1 no cumple longitud
							else return -22;

						}//Apellido 1 nulo
						else
							return -23;
					} //Nombre solo cotiene letras
					else
						return -24;
				} //Nombre no cumple longitud:
				else 
					return -25;
			} //Nombre nulo
			else
				return -26;
		}
		else //Error en el Captcha.
			return $captcha;
	}

	/** Función que comprueba una única contraseña **/
	function mcheckpassword($nombre){
		if(isset($_POST[$nombre])){ //Comprobar que la contraseña de confirmacion no sea nula
			$passwd = $_POST[$nombre];
			$aux = strlen(preg_replace('/\s/', '', $passwd)); //Eliminar todos los espacios en blanco
			//Comprobar que la contraseña tienen la longitud adecuada:
			if($aux >= MIN_PASSWD && $aux <= MAX_PASSWD){
				//Comprobar que la contraseña solo tiene letras (mayusculas o minusculas) y numeros
				if(!preg_match('/[^A-Za-z0-9]/', $passwd)){
					return 0;
				}
				else //Contraseña tiene caracteres incorrectos
					return -6;
			}
			else //Password no cumple con las longitudes
				return -8;
		}
		else //Password nula
			return -13;
	}

	/** Funcion que permite comprobar las contraseña y la contraseña de confirmación**/
	function mcheckpasswords($password,$password_conf){
		//Comprobar la primera contraseña:
		$check_passwd = mcheckpassword($password);	
		if($check_passwd == 0){
			$passwd = $_POST[$password];
			//Comprobar la password de confirmación:
			$check_passwd_conf = mcheckpassword($password_conf);
			if($check_passwd_conf == 0){
				$passwd_conf = $_POST[$password_conf];
				//Comprobar que la contraseñas coinciden:
				if(strcmp($passwd,$passwd_conf) == 0){
					return 0; //Passwords correctas
				}
				else //Paswords no coinciden
					return -4;
			} //Password de confirmación incorrecta.
			else
				return $check_passwd_conf + 1;

		} //Password incorrecta:
		else
			return $check_passwd;
	}

	/** Función que permite chequear los correos electronicos **/
	function mcheckemails(){
		if(isset($_POST["correo"])){ //Comprobar que el correo no sea nulo
			$correo = $_POST["correo"];
			$aux = strlen(preg_replace('/\s/', '', $correo)); //Eliminar todos los espacios en blanco
			if($aux <= MAX_CORREO){ //Comprobar que la longitud del correo es correcta
				if(isset($_POST["correo_conf"])){ //Comprobar que el correo de confirmacion no sea nulo
					$correo_conf =$_POST["correo_conf"];
					$aux = strlen(preg_replace('/\s/', '', $correo_conf)); //Eliminar todos los espacios en blanco
					if($aux <= MAX_CORREO){ //Comprobar que el correo de confirmación es correcto
						//Verificar que los correos que se han enviado es una direccion de correo valida:
						if(mvalidaremail($correo)){
							if(mvalidaremail($correo_conf)){
								//Comprobar que los correos coinciden:
								if(strcmp($correo, $correo_conf) == 0){
									return 0;
								}
								else //Correos no coinciden
									return -9;
							}
							else //Correo de confirmación no valido
								return -10;
						}
						else //Correo no valido
							return -11;
					}
					else //Longitud correo confirmación incorrecta 
						return -14;
				}
				else //Correo de confirmación nulo
					return -15;
			}
			else //Longitud correo incorrecta 
				return -16;
		} //Correo nulo
		else
			return -17;
	}


	/*** Funcion que permite validar un email ***/
	function mvalidaremail($email){
		
		// Borramos los caracteres no válidos del email
		$email = filter_var($email, FILTER_SANITIZE_EMAIL);
		
		// Realizamos la validación del email
		return filter_var($email, FILTER_VALIDATE_EMAIL);
	}

	/** Función que permite comprobar la provincia seleccionada por el usuario **/
	function mcheckprovincia(){
		$con = conexion();

		if($con == null){ //No hay conexión
			mcierrasesion();
			return -8080;
		}

		if(isset($_POST['provincia'])){ //Comprobar que la provincia no sea vacia:
			$provincia = $_POST['provincia']; //Obtener la provincia seleccionada por el usuario:

			$consulta = "SELECT COUNT(id_provincia) as provincia from final_provincias where id_provincia = '$provincia'";

			if($resultados = $con->query($consulta)){ //Obtener el resultado de la query
				$resultado = $resultados->fetch_assoc();
				if($resultado['provincia'] == 1){ //Se ha encontrado la provincia en la base de datos
					return 0;
				}
				else //No se ha encontrado la provincia en la base de datos:
					return -33;

			}
			else //Error en la query:
				return -34;
		}
		else //Provincia vacia:
			return -35;
	}

	/** Función que permite comprobar el municipio seleccionado por el usuario **/
	function mcheckmunicipio(){
		$con = conexion();

		if($con == null){ //No hay conexión
			mcierrasesion();
			return -8080;
		}

		if(isset($_POST['municipio'])){ //Comprobar que el municipio no sea vacio:
			$municipio = $_POST['municipio']; //Obtener el municipio seleccionada por el usuario:
			$provincia = $_POST['provincia']; 

			$consulta = "SELECT COUNT(id_municipio) as municipio from final_municipios where id_municipio = '$municipio' and provincia_id = '$provincia'";

			if($resultados = $con->query($consulta)){ //Obtener el resultado de la query
				$resultado = $resultados->fetch_assoc();
				if($resultado['municipio']  == 1){ //Se ha encontrado el municipio en la base de datos
					return 0;
				}
				else //No se ha encontrado el municipio en la base de datos:
					return -43;
			}
			else //Error en la query:
				return -44;
		}
		else //Municipio vacio:
			return -45;
	}

	/*** Funcion que pemite validar el login de un usuario si el login se lleva a cabo con exito se inicia una sesion 
	para dicho usuario ***/
	function mvalidarlogin(){
		$con = conexion();

		if($con == null){ //No hay conexión
			mcierrasesion();
			return -8080;
		}

		$encriptar = 0; //Variable que indicará si es necesario encriptar o no la password.
		//Leer el correo y la contraseña que envía el usuario a través del formulario
		
		if(isset($_POST["correo"])){ //Comprobar que el correo no es nulo:
			$correo =  $_POST["correo"];
			$aux = strlen(preg_replace('/\s/', '', $correo)); //Eliminar todos los espacios en blanco
			if($aux <= MAX_CORREO){ //Comprobar la longitud del correo:
				if(mvalidaremail($correo)){ //Comprobar que es un correo valido:
					$check_passwd = mcheckpassword("password");
					if($check_passwd == 0){ //Si contraseña correcta:
						$captcha = mcheckcaptcha();
						if($captcha == 0){ //El usuario ha hecho click en el Captcha
							$passwd = $_POST["password"];
							$passwd_encrypt = md5($passwd); //Encriptar la constraseña para buscarla en la BBDD
							$consulta = "select * from final_usuarios where correo_electronico = '$correo' and password = '$passwd_encrypt'";
							$consulta2 = "select * from final_usuarios where correo_electronico = '$correo' and password = '$passwd'";
							if (($resultado = $con->query($consulta)) && ($resultado2 = $con->query($consulta2))){ //Comprobar con password encriptada y sin encriptar
								//Contamos el numero de lineas devueltas por la query, si es igual a 1 el inicio de sesion saerá correcto.
								if (mysqli_num_rows($resultado) == 1 || mysqli_num_rows($resultado2) == 1){//Usuario encontrado:
									if(mysqli_num_rows($resultado) == 1){
										$datos_usuario = $resultado->fetch_assoc();
										$passwd = $passwd_encrypt; 
										$encriptar = 1;
									}
									else
										$datos_usuario = $resultado2->fetch_assoc();
									if($datos_usuario['bloqueado'] == 0){ //Si no es un usuario bloqueado
										if($datos_usuario['Tipo'] != 3){
											//Iniciar sesion para el usuario:
											$_SESSION['user'] = $correo;
											$_SESSION['password'] = $passwd;
											$_SESSION['tipo'] = $datos_usuario['Tipo'];
											$_SESSION['nombre'] = $datos_usuario['nombre'];
											$_SESSION['ap1'] = $datos_usuario['apellido1'];
											$_SESSION['ap2'] = $datos_usuario['apellido2'];
											$_SESSION['provincia'] = $datos_usuario['provincia'];
											$_SESSION['municipio'] = $datos_usuario['municipio'];
											$_SESSION['direccion'] = $datos_usuario['direccion'];
											$_SESSION['nick'] = $datos_usuario['nick'];

											$_SESSION['tiempo'] = time();

											//Comprobar el boton recuerdame:
											mcheckrecuerdame($encriptar);
											return 0;
										}
										else //Admin intentando hacer inicio de sesión:
											return -10;
									}
									else //Si es un usuario bloqueado:
										return -9;
								}
								else //Inicio de sesión incorrecto.
									return -1;
							}
							else //Error en la query
								return -2;
						}
						else //Error Captcha
							return $captcha;
					}
					else //Contraseña incorrecta
						return $check_passwd;
				}
				else //Correo no valido
					return -5;
			}
			else //Longitud de correo incorrecta
				return -4;
		}
		else //Correo vacio
			return -3;
	}

	/** Función que permite comprobar si un usuario ha aceptado o no el captcha **/
	function mcheckcaptcha(){
		//Clave secreta del Captcha
		$secret = "6LcJhqIUAAAAANhHZOjtygHU_uj100cy6gDV-85z";
 
		//Respuesta del Captcha:
		$response = null;
 
		//Comprobar la clave secreta del Captcha
		$reCaptcha = new ReCaptcha($secret);
		

		//Comprobhar que se ha hecho click en el Captcha y verificar la respuesta:
		if ($_POST["g-recaptcha-response"]) {
		    $response = $reCaptcha->verifyResponse(
		        $_SERVER["REMOTE_ADDR"],
		        $_POST["g-recaptcha-response"]
		    );
		}

		//Si la respuesta no es nula u el valor es correcto:
		if ($response != null && $response->success) {
			return 0;
		}
		else
			return -31;
	}

	/** Función que permite comprobar el botón recuerdame del formulario de inicio de sesión, en caso de estar seleccionado guardar cookie **/
	//Recibe como parámetro un valor que indica si es necesario encriptar la password a la hora de crear la cookie o no.
	function mcheckrecuerdame($encriptar){
		$cookie_nombre = 'inicio_sesion';
		if(isset($_POST['Recuerdame'])){
			
			//Generar la cookie con los datos: 
			if($encriptar == 1)
				$cookie_valores = $_POST['correo'] .','. md5($_POST['password']); //Almacenar los datos de inicio de sesión del usuario:
			else
				$cookie_valores = $_POST['correo'] .','.$_POST['password']; //Almacenar los datos de inicio de sesión del usuario:
			setcookie($cookie_nombre, $cookie_valores, time() + (86400 * 30), "/"); //Recordar durante 1 mes.
		}
		else{
			if(isset($_COOKIE[$cookie_nombre])){ //Comprobar si la cookie existe ya:
				echo "Borramos cookie";	
				unset($_COOKIE[$cookie_nombre]);
				setcookie($cookie_nombre, "", time() - 3600, "/"); //Actualizar el tiempo de expiración de la cookie a hace una hora.
			}
		}
	}
	
	/*** Funcion que permite comprobar el tiempo de sesion de un usuario, si este excede el limite se le cerrará sesión automaticamente.***/
	function mcomprobarusuario() {
		$con = conexion();

		if($con == null){ //No hay conexión
			mcierrasesion();
			return -8080;
		}

		if (!isset($_SESSION['user'])) { //Comprobar que la sesion no el vacia
			return -4;
		}

		$resta = time() - $_SESSION['tiempo']; //Calcular el tiempo que lleva conectado el usuario

		if ($resta > MAX_TIEMPO_SESION) {
			session_destroy(); //Destruir la sesion (Borra todas las variables de sesión).
			return -6; //Tiempo de sesión execedido
		}

		//Actualizar el tiempo de sesion, si aun no se ha excedido:
		$_SESSION['tiempo'] = time();

		//Comprobar que ese usuario aun es valido y existe en la BBDD
		$consulta = "select * from final_usuarios where correo_electronico = '" . $_SESSION['user'] . "'";

		if ($resultado = $con->query($consulta)) {
			if (mysqli_num_rows($resultado) == 1){ //Usuario encontrado
				$datos_usuario = $resultado->fetch_assoc();
				if($datos_usuario['bloqueado'] == 0){ //Comprobar que el usuario no este bloqueado:
					if($datos_usuario['Tipo'] != 3){ 
						//Guardar info del usuario
						$_SESSION['tipo'] = $datos_usuario['Tipo'];
						$_SESSION['password'] = $datos_usuario['password'];
						$_SESSION['nombre'] = $datos_usuario['nombre'];
						$_SESSION['ap1'] = $datos_usuario['apellido1'];
						$_SESSION['ap2'] = $datos_usuario['apellido2'];
						$_SESSION['provincia'] = $datos_usuario['provincia'];
						$_SESSION['municipio'] = $datos_usuario['municipio'];
						$_SESSION['direccion'] = $datos_usuario['direccion'];
						$_SESSION['nick'] = $datos_usuario['nick'];
						
						return 0; 
					}
					else{
						mcierrasesion();
						return -5;
					}
				}
				else{ //Usuario bloqueado
					mcierrasesion();
					return -3;
				}
			} 
			else {//Error usuario no encontrado:
				return -1;
				session_destroy(); //Destruir la sesion (Borra todas las variables de sesión).
			}
		} 
		else //Error al realizar la query:
			return -2;	
	}

	/** Funcion que se encarga de cerrar la sesión del usuario. **/
	function mcierrasesion(){
		session_destroy();
	}

	/** Funcion que comprueba una solicitud de verificación enviada por un usuario, si es correcta
		la envia para su aprobación **/
	function mcomprobarsolicitud(){
		$con = conexion();

		if($con == null){ //No hay conexión
			mcierrasesion();
			return -8080;
		}

		echo empty($_POST["textoexplicacion"]);
		if(isset($_POST["textoexplicacion"]) ){
			$solicitud = $_POST["textoexplicacion"];
			$aux = strlen(preg_replace('/\s/', '', $solicitud)); //Eliminar todos los espacios en blanco
			if($aux >= MIN_TEXT && $aux <= MAX_TEXT){
				$correo = $_SESSION['user'];
				$consulta = "insert into final_solicitudes (correo_electronico, descripcion) values ('$correo', '$solicitud')";

				if ($con->query($consulta)) { //Insert correcto
					$consulta2 = "UPDATE final_usuarios SET Tipo = '1' WHERE final_usuarios.correo_electronico = '$correo'";
					if($con->query($consulta2)){ //Se ha cambiado el campo tipo del usuario.
						return 0;
					}
					else //Error al actualizar el campo tipo del usuario
						return -1;
				}
				else //Error al insertar la solicitud
					return -2;
			}
			else //Texto no cumple la longitud establecida:
				return -3;
		}
		else //Solicitud vacia:
			return -4;
	}

	/** Función que permite obtener el número de entradas que hay en la BBDD **/
	function mnumentradas(){
		$con = conexion();

		if($con == null){ //No hay conexión
			mcierrasesion();
			return -8080;
		}

		//Comprobar si el usuario está buscando una entrada:
		if(isset($_GET["letra"])){
			$letra = $_GET["letra"];

			$consulta = "select count(id_entrada) as num_entradas from final_entradas where titulo like '$letra%'"; //Contar el número de entradas en la BBDD que empiecen por una letra y devolver el número como una variable llamada num_entradas
		}
		else//Si no es así obtener todas las entradas:
			$consulta = "select count(id_entrada) as num_entradas from final_entradas"; //Contar el número de entradas en la BBDD y devolver el número como una variable llamada num_entradas

		return $con->query($consulta);
	}

	/** Función que permite obtener las entradas de la BBDD en función de la página en la que se encuentra el usuario  y si está buscando,
	también en función de la letra**/
	function mrecuperaentradas(){
		$con = conexion();

		if($con == null){ //No hay conexión
			mcierrasesion();
			return -8080;
		}

		if(isset($_GET["pagina"])){ //Comprobar que el parámetro página no sea nulo.
			$pagina_actual = $_GET["pagina"]; //Obtener el número de página actual en la que se encuentra el usuario.
			$pagina_actual--; //Como el número de página comienza por 1 en vez de por 0 reducir en 1 el número de la página.
			$desplazamiento = $pagina_actual * ELEMENTOS_PAGINA; //Calular el desplazamiento en función de la página actual * el número de entradas a mostrar por página

			//Comprobar si el usuario está buscando una entrada:
			if(isset($_GET["letra"])){
				$letra = $_GET["letra"];

				//Limit  permitirá determinar con el primer parámetro desde que entrada coger de la BBDD y con el segundo parámetro cuantas entradas coger.
				$consulta = "select * from final_entradas where titulo like '$letra%' order by fecha_creacion limit $desplazamiento, ".ELEMENTOS_PAGINA;
			}
			else
				//Limit  permitirá determinar con el primer parámetro desde que entrada coger de la BBDD y con el segundo parámetro cuantas entradas coger.
				$consulta = "select * from final_entradas order by fecha_creacion limit $desplazamiento, ".ELEMENTOS_PAGINA;

			return $con->query($consulta); //Devolver las entradas seleccionadas.
		}
		return null;
	}

	/** Función que permite recuperar una entrada concreta que ha sido seleccionada por un usuario **/
	function mrecuperaentrada(){
		$con = conexion();

		if($con == null){ //No hay conexión
			mcierrasesion();
			return -8080;
		}

		if(isset($_POST["entrada"])){ //Comprobar qur el parámetro de la entrada no sea nulo.
			$entrada = $_POST["entrada"];
			$consulta = "select * from final_entradas where id_entrada = '$entrada'";

			if($resultado = $con->query($consulta)){ //Devolver la entrada seleccionada.

				if(mysqli_num_rows($resultado) == 1)
					return $resultado;
			}
		}
		return null;
	}

	/** Función que permite comprobar si un usuario ha modificado una entrada concreta alguna vez **/
	function mhamodificado(){
		$con = conexion();

		if($con == null){ //No hay conexión
			mcierrasesion();
			return -8080;
		}

		if(isset($_POST['entrada'])){ //Comprobar qur el parámetro de la entrada no sea nulo.
			$id_entrada = $_POST['entrada'];
			$correo = $_SESSION['user'];
			$consulta = "select id_entrada, correo_creador from final_entradas_modificadas where id_entrada = '$id_entrada' and correo_creador = '$correo'";

			if($resultado = $con->query($consulta)){
				if (mysqli_num_rows($resultado) == 0) //Usuario no encontrado 
					return 0;
				else //Usuario encontrado en la BBDD
					return -1;
			}
			else //Error en la query 1
				return -2;
		}
		else //Parametro entrada no detectado:
			return -3;
	}

	/** Función que permite comprobar una modificación enviada por un usuario verificado, en caso de ser correcta la almacena en la 
	base de datos **/
	function mcheckmodificacion(){
		$con = conexion();

		if($con == null){ //No hay conexión
			mcierrasesion();
			return -8080;
		}

		if(isset($_POST['entrada'])){ //Comprobar el número de entrada
			if(isset($_POST['tituloModificar'])){ //Comprobar que el titulo de la modificación no es vacio:
				if(isset($_POST['contenidoModificar'])){ //Comprobar que el contenido de la modificación no es vacio:
					$titulo = $_POST['tituloModificar'];
					$contenido = $_POST['contenidoModificar'];
					$aux = strlen(preg_replace('/\s/', '', $titulo)); //Eliminar todos los espacios en blanco
					if($aux >= MIN_TITULO &&  $aux <= MAX_TITULO){ //Comprobar la longitud del titulo de la modificación:
						$aux = strlen(preg_replace('/\s/', '', $contenido)); //Eliminar todos los espacios en blanco
						if($aux >= MIN_CONTENIDO && $aux <= MAX_CONTENIDO){ //Comprobar la longitud del contenido de la modificación:
							$id_entrada = $_POST['entrada'];
							$correo = $_SESSION['user'];
							$consulta = "insert into final_entradas_modificadas (id_entrada, correo_creador, titulo, texto) values ('$id_entrada', '$correo', '$titulo', '$contenido')";

							if($resultado = $con->query($consulta)){//Insert correcto
								return 0;
							}
							else //Error en el insert
								return -1;
						}
						else //Contenido de la modificación no cumple la longitud:
							return -2;
					}
					else //Titulo de la modificación no cumple la longitud:
						return -3;
						mostrarmensaje("El contenido es vacio", "El contenido de su modificación esta vacio, reviselo",1);
				}
				else //Contenido de la modificación es vacio:
					return -4;
					mostrarmensaje("El contenido es vacio", "El contenido de su modificación esta vacio, reviselo",1);
			}
			else //Titulo de la modificación es vacio:
				return -5;
		}
		else //Numero de entrada nulo
			return -6;
	}

	/** Funcion que permite controlar el cambio de contraseña de un usuario registrado:**/
	function mcambiopassword(){
		$con = conexion();

		if($con == null){ //No hay conexión
			mcierrasesion();
			return -8080;
		}
		
		//Comprobar la password actual:
		$check_actual = mcheckpassword("password_actual");

		if($check_actual == 0){ 
			$passwd_actual = $_POST['password_actual'];
			if(strcmp(md5($passwd_actual),$_SESSION['password']) == 0){ //Comparar la password actual
			
				//Chequear las passwords nuevas:
				$check_passwords = mcheckpasswords('password','password_conf');

				//Si las passwords son correctas:
				if($check_passwords == 0){
					$password = $_POST['password'];
					if(strcmp($password,$passwd_actual) != 0){ //Comprobar que la password nueva no sea igual a la anterior:
						$correo = $_SESSION['user'];
						$password = md5($password);
						$consulta = "UPDATE final_usuarios SET password = '$password' WHERE final_usuarios.correo_electronico = '$correo'";

						if($con->query($consulta)){
							mcierrasesion(); //Cerrar la sesión del usuario.
							return 0; //Modificación correcta
						}
						else{
							return -1; //Error en el update
						}
					}
					else //Password nueva igual a la anterior:
						return -3;
				}
				else //Constraseñas no correctas:
					return $check_passwords;
			}
			else //Password actual no coincide
				return -2;
		}
		else //Contraseña actual incorrecta
			return $check_actual-10;
	}

	/** Función que permite controlar el cambio de imagen de perfil de un usuario **/ 
	function mcambioimagen(){
		$con = conexion();

		if($con == null){ //No hay conexión
			mcierrasesion();
			return -8080;
		}
		
		if(isset($_FILES['fichero']) && $_FILES['fichero']['size'] != 0){ //Comprobar que el fichero no es vacio
			//Comprobar si el fichero que ha subido el usuario es una imagen o no:
			$check = getimagesize($_FILES['fichero']['tmp_name']);
   			if($check == true) { //El fichero es una imagen:
   				//Comprobar que el tamaño no excede el máximo:
        		if ($_FILES['fichero']['size'] <= MAX_IMAGEN) {
				    //Comprobar el formato del fichero:
					$trozos = explode("/", $_FILES["fichero"]["type"]);
				    $imageFileType = strtolower($trozos[1]);
				    if($imageFileType == "jpg" || $imageFileType == "png" || $imageFileType == "jpeg"){
				    	$imagen= addslashes (file_get_contents($_FILES['fichero']['tmp_name']));
				    	$correo = $_SESSION['user'];
				    	$consulta = "UPDATE final_usuarios SET imagen_perfil = '$imagen' WHERE final_usuarios.correo_electronico = '$correo'";

				    	if($con->query($consulta)){ //Foto actualizada correctamente:
				    		return 0;
				    	} 
				    	else //Error en el update:
				    		return -1;
				    }
				    else //Formato no valido
				    	return -2;
				}
				else //Imagen supera el límite de tamaño:
					return -3;
   			}
   			else //El fichero no es una imagen
		    	return -4;
		}
		else //Fichero subido es vacio:
			return -5;
	}

	/** Función que permite recuperar la imagen de perfil de un usuario de la base de datos : **/
	function mrecuperaimagen(){
		$con = conexion();

		if($con == null){ //No hay conexión
			mcierrasesion();
			return -8080;
		}

		$correo = $_SESSION['user'];
		$consulta = "Select imagen_perfil from final_usuarios where correo_electronico = '$correo'";

		return $con->query($consulta);
	}

	/** Función que permite obtener el conjunto de provincias almacenadas en la base de datos **/
	function mobtenerprovincias(){
		$con = conexion();

		if($con == null){ //No hay conexión
			mcierrasesion();
			return -8080;
		}

		$consulta = 'select * from final_provincias';

		if($resultado = $con->query($consulta)) //Si recupera correctamente las provincias:
			return $resultado;
		else //Si no se han podido recuperar las provincias:
			return null;
	}

	/** Función que permite obtener el conjunto de municipios almacenados en la base de datos de acuerdo a el id de provincia recibido **/
	function mobtenermunicipios(){
		$con = conexion();

		if($con == null){ //No hay conexión
			mcierrasesion();
			return -8080;
		}

		if($_GET['idprovincia']){ //Comprobar que el id de la provincia no sea nulo:
			$provincia_id = $_GET['idprovincia'];
			$consulta = "select * from final_municipios where provincia_id = '$provincia_id'";

			if($result = $con->query($consulta)){ //Datos obtenidos de manera correcta:
				return $result;
			}
		}
		return null;
	}

	/** Función que permite recuperar el nombre de la provincia del perfil del usuario **/
	function mrecuperaprovincia(){
		$con = conexion();

		if($con == null){ //No hay conexión
			mcierrasesion();
			return -8080;
		}

		$id_provincia = $_SESSION['provincia'];
		$consulta = "select provincia from final_provincias where id_provincia = '$id_provincia'";

		if($result = $con->query($consulta)){ //Si la query ha ido bien:
			return $result;
		}
		return null;
	}


	/** Función que permite recuperar el nombre del municipio del perfil del usuario **/
	function mrecuperamunicipio(){
		$con = conexion();

		if($con == null){ //No hay conexión
			mcierrasesion();
			return -8080;
		}

		$id_municipio = $_SESSION['municipio'];
		$consulta = "select municipio from final_municipios where id_municipio = '$id_municipio'";

		if($result = $con->query($consulta)){ //Si la query ha ido bien:
			return $result;
		}
		return null;
	}

	function mrecuperadescuentos(){
		/*
			Funcion que devuelve los descuentos asignados a el usuario con sesion activa
		*/
		$bd = conexion();

		if($bd == null){ //No hay conexión
			mcierrasesion();
			return -8080;
		}
		if(isset($_GET['pagina'])){
			$correo_usuario = $_SESSION['user'];
			$pagina_actual = $_GET["pagina"]; //Obtener el número de página actual en la que se encuentra el usuario.
			$pagina_actual--; //Como el número de página comienza por 1 en vez de por 0 reducir en 1 el número de la página.
			$desplazamiento = $pagina_actual * ELEMENTOS_PAGINA; //Calular el desplazamiento en función de la página actual * el número de entradas a mostrar por página

			$consulta = "select final_descuentos.* from final_descuentos,final_descuentos_usuarios, final_usuarios where final_usuarios.correo_electronico='$correo_usuario' AND final_usuarios.correo_electronico = final_descuentos_usuarios.correo_electronico AND final_descuentos_usuarios.id_descuento = final_descuentos.id_descuento limit $desplazamiento,".ELEMENTOS_PAGINA;
			if($result = $bd->query($consulta))
				return $result;
		}
		return null;
	}

	/** Función que permite recuperar el numero de descuentos disponibles para un usuario **/
	function mnumerodescuentos(){
		$bd = conexion();

		if($bd == null){ //No hay conexión
			mcierrasesion();
			return -8080;
		}
		$correo = $_SESSION['user'];
		$consulta= "select count(id_descuento) as num_descuentos from final_descuentos_usuarios where correo_electronico = '$correo'";
		if($result = $bd -> query($consulta))
			return $result;
		else
			return null;
	}

	function mmostrardescuentoelegido(){
		/*
			Funcion que muestra la informacion relacionada con un descuento
		*/
		$bd = conexion();

		if($bd == null){ //No hay conexión
			mcierrasesion();
			return -8080;
		}

		$id_descuento = $_POST["iddescuento"];
		$correo_usuario = $_SESSION['user'];
		$consulta1 = "select final_descuentos.* from final_descuentos where id_descuento=$id_descuento";
		$consulta2 = "select nombre,apellido1,apellido2 from final_usuarios where correo_electronico='$correo_usuario'";
		return array($bd->query($consulta1),$bd->query($consulta2));
	}


	function mlistapost(){
		/*
			Devuelve: informacion sobre todos los posts existentes
		*/
		$bd = conexion();

		if($bd == null){ //No hay conexión
			mcierrasesion();
			return -8080;
		}

		$consulta = "select final_posts.*, final_usuarios.nick, count(final_comentarios.id_post) as 'ncomentarios' from final_posts,final_comentarios,final_usuarios where final_posts.id_post = final_comentarios.id_post and final_usuarios.correo_electronico = final_posts.correo_creador GROUP BY final_posts.id_post,final_comentarios.id_post order by final_posts.fecha desc";
		return $bd->query($consulta);
	}

	function mforoelegido(){
		/*
			Devuelve: 
			- Informacion sobre el foro elegido
		*/
		$bd = conexion();

		if($bd == null){ //No hay conexión
			mcierrasesion();
			return -8080;
		}

		if(isset($_POST['idpost'])){
			$idpost = $_POST["idpost"];
			$consulta1 = "select final_posts.*, final_usuarios.nick from final_posts inner join final_usuarios on (final_posts.correo_creador = final_usuarios.correo_electronico) where id_post = $idpost";
			if($result = $bd->query($consulta1))
				return $result;
		}
		return null;
	}

	function mbuscarforo(){
		/*
			Devuelve: Informacion de los foros cuyo titulo comienza con las letras introducidas en un buscador.
		*/
		$bd = conexion();

		if($bd == null){ //No hay conexión
			mcierrasesion();
			return -8080;
		}
		if(isset($_GET["pagina"])){ //Comprobar que el parámetro página no sea nulo.
			$pagina_actual = $_GET["pagina"]; //Obtener el número de página actual en la que se encuentra el usuario.
			$pagina_actual--; //Como el número de página comienza por 1 en vez de por 0 reducir en 1 el número de la página.
			$desplazamiento = $pagina_actual * ELEMENTOS_PAGINA; //Calular el desplazamiento en función de la página actual * el número de entradas a mostrar por página

			if(isset($_GET["letras"]) && strcmp($_GET["letras"]," ") != 0){
				$letras = $_GET["letras"];
				$consulta = "select final_posts.*, final_usuarios.nick, count(final_comentarios.id_post) as 'ncomentarios' from final_posts,final_comentarios,final_usuarios where final_posts.titulo like '$letras%' and final_posts.id_post = final_comentarios.id_post and final_usuarios.correo_electronico = final_posts.correo_creador GROUP BY final_posts.id_post,final_comentarios.id_post order by final_posts.fecha desc limit $desplazamiento,".ELEMENTOS_PAGINA;
			}
			else
				$consulta = "select final_posts.*,final_usuarios.nick, count(final_comentarios.id_post) as 'ncomentarios' from final_posts,final_comentarios,final_usuarios where final_posts.id_post = final_comentarios.id_post and final_usuarios.correo_electronico = final_posts.correo_creador GROUP BY final_posts.id_post,final_comentarios.id_post order by final_posts.fecha desc limit $desplazamiento,".ELEMENTOS_PAGINA;
			return $bd->query($consulta);
		}
		return null;
	}

	function mnuevoforo(){
		/*
			Generamos un nuevo post con un primer comentario y mostramos la lista de los foros.
		*/
		$bd = conexion();

		if($bd == null){ //No hay conexión
			mcierrasesion();
			return -8080;
		}

		$comentario = $_POST["comentario"];
		$titulo_post = $_POST["titulo_post"];
		$correo_creador = $_SESSION['user'];
		$correo_escritor = $correo_creador;
		$max_id_c = "select max(id_post) 'max' from final_posts";
		$res = $bd->query($max_id_c);
		$datos = $res->fetch_assoc();
		$max_id = $datos["max"];
		$max_id = $max_id + 1;
		$consulta1 = "insert into final_posts (id_post, correo_creador, titulo) values ($max_id,'$correo_creador','$titulo_post')";
		$bd->query($consulta1);
		$consulta2 = "insert into final_comentarios (id_post,comentario,correo_escritor) values ($max_id,'$comentario','$correo_escritor')";
		$bd->query($consulta2);
		return mlistapost();
	}

	function mescribircomentario(){
		/*
			Funcion que inserta un nuevo comentario en la BBDD
		*/
		$bd = conexion();

		if($bd == null){ //No hay conexión
			mcierrasesion();
			return -8080;
		}

		if(isset($_POST["comentario"]) && !empty($_POST["comentario"])){
			$comentario = $_POST["comentario"];
			$aux = strlen(preg_replace('/\s/', '', $comentario)); //Eliminar todos los espacios en blanco
			if(isset($_POST["idpost"])){
				if($aux >= MIN_COMENTARIO && $aux <= MAX_COMENTARIO){
					$correo_usuario = $_SESSION['user'];
					$idpost = $_POST["idpost"];
					$consulta = "insert into final_comentarios (id_post,comentario,correo_escritor) values ($idpost,'$comentario','$correo_usuario')";
					if($bd->query($consulta))
						return 0;
					else
						return -1;
				}
				else
					return -2;
			}
			else
				return -3;
		}
		else
			return -4;
	}

	function mselecentrada(){
		/*
			Devuelve: 
			- Informacion relacionada con la entrada seleccionada
			- Criticas sobre la entrada correspondiente
			- Escrito: comprobacion de si el usuario que intenta escribir ya ha escrito una critica, en cuyo caso no podremos escribir una nueva critica
		*/
		$bd = conexion();

		if($bd == null){ //No hay conexión
			mcierrasesion();
			return -8080;
		}

		if(isset($_POST['entrada'])){
			$identrada = $_POST['entrada'];
			$consulta = "select final_entradas.*,final_usuarios.nick as nick_creador, (SELECT nick from final_usuarios WHERE final_usuarios.correo_electronico = final_entradas.correo_modificacion) as nick_modificacion from final_entradas,final_usuarios where final_entradas.id_entrada='$identrada' and final_usuarios.correo_electronico = final_entradas.correo_creador";
			$consulta_actores = "select final_actores.* from final_actores NATURAL JOIN final_entradas_actores where final_entradas_actores.id_entrada=$identrada";
			$consulta_directores = "select final_directores.* from final_directores NATURAL JOIN final_entradas_directores where final_entradas_directores.id_entrada = $identrada";
			$criticas = "select * from final_criticas where id_entrada=$identrada";
			$punt_media = "select CAST(AVG(valoracion) AS DECIMAL(10,1)) as media from final_criticas where id_entrada = $identrada";
			$imagenes = "select * from final_imagenes where id_entrada = $identrada";
			if(isset($_SESSION['user'])){
				$correosesion = $_SESSION['user'];
				$escrito = "select count(*) as numcriticas from final_criticas where id_entrada = $identrada AND final_criticas.correo_creador='$correosesion'";
				return array($bd->query($consulta),$bd->query($consulta_actores),$bd->query($consulta_directores),$bd->query($criticas),$bd->query($punt_media),$bd->query($imagenes),$bd->query($escrito));
			}
			else
				return array($bd->query($consulta),$bd->query($consulta_actores),$bd->query($consulta_directores),$bd->query($criticas),$bd->query($punt_media),$bd->query($imagenes));
		}
		return null;
	}

	/** Función que permite recuperar para una entrada todas sus críticas asociadas **/
	function mrecuperacriticas(){
		$bd = conexion();

		if($bd == null){ //No hay conexión
			mcierrasesion();
			return -8080;
		}

		if(isset($_GET['entrada'])){
			if(isset($_GET["pagina"])){ //Comprobar que el parámetro página no sea nulo.
				$identrada = $_GET['entrada'];
				$pagina_actual = $_GET["pagina"]; //Obtener el número de página actual en la que se encuentra el usuario.
				$pagina_actual--; //Como el número de página comienza por 1 en vez de por 0 reducir en 1 el número de la página.
				$desplazamiento = $pagina_actual * ELEMENTOS_PAGINA; //Calular el desplazamiento en función de la página actual * el número de criticas a mostrar por página

				if(isset($_GET["letras"]) && strcmp($_GET["letras"]," ") != 0){
					$letras = $_GET["letras"];
					$criticas = "select final_criticas.*,final_usuarios.nick from final_criticas inner join final_usuarios on (final_usuarios.correo_electronico = final_criticas.correo_creador) where final_criticas.id_entrada='$identrada' and final_usuarios.nick like '$letras%' order by final_criticas.fecha limit $desplazamiento,".ELEMENTOS_PAGINA;
				}
				else
					$criticas = "select final_criticas.*,final_usuarios.nick from final_criticas inner join final_usuarios on (final_usuarios.correo_electronico = final_criticas.correo_creador) where final_criticas.id_entrada='$identrada' order by final_criticas.fecha limit $desplazamiento,".ELEMENTOS_PAGINA;
				return $bd->query($criticas);
			}
		}
		return null;
	}

	/** Función que permite recuperar el número de críticas asociadas a una entrada **/
	function mnumerocriticas(){
		$bd = conexion();

		if($bd == null){ //No hay conexión
			mcierrasesion();
			return -8080;
		}

		if(isset($_GET['entrada'])){
			if(isset($_GET['letras']) && strcmp($_GET['letras'], " ") != 0){
				$letras = $_GET['letras'];
				$identrada = $_GET['entrada'];
				$consulta = "select final_usuarios.nombre, count(id_entrada) as num_criticas from final_criticas inner join final_usuarios on (final_usuarios.correo_electronico = final_criticas.correo_creador) where final_usuarios.nombre like '$letras%' and id_entrada = '$identrada' group by final_usuarios.nombre";
			}
			else
				$consulta = "select count(id_entrada) as num_criticas from final_criticas where id_entrada = '$identrada'";
			
			if($result = $bd->query($consulta))
				return $result;
		}
		return null;
	}

	function mescribircritica(){
		/*
			Funcion que inserta una nueva critica en la BBDD
		*/
		$bd = conexion();

		if($bd == null){ //No hay conexión
			mcierrasesion();
			return -8080;
		}

		$correo_creador = $_SESSION['user'];
		$identrada = $_POST["identrada"];
		$critica = $_POST["texto_critica"];
		$valoracion = $_POST['valoracion'];
		$consulta = "insert into final_criticas (id_entrada,critica,correo_creador,valoracion) values ($identrada,'$critica','$correo_creador',$valoracion)";
		$bd->query($consulta);
	}

	function meditarcritica(){
		/*
			Devuelve: consulta con una critica seleccionada para poder ser editada
		*/
		$bd = conexion();

		if($bd == null){ //No hay conexión
			mcierrasesion();
			return -8080;
		}
		if(isset($_POST["entrada"])){
			$correo_sesion = $_SESSION['user'];
			$identrada = $_POST["entrada"];
			$critica = "select final_entradas.id_entrada, final_entradas.titulo,final_criticas.critica,final_entradas.id_entrada,final_criticas.valoracion,final_criticas.correo_creador from final_criticas,final_entradas where final_criticas.id_entrada=$identrada AND final_criticas.correo_creador='$correo_sesion' AND final_entradas.id_entrada = final_criticas.id_entrada";
			return $bd->query($critica);
		}
		return null;
	}

	function mmodificarcritica(){
		/*
			Devuelve: Instruccion sql para actualizar una critica presente
		*/
		$bd = conexion();

		if($bd == null){ //No hay conexión
			mcierrasesion();
			return -8080;
		}

		if(isset($_POST['critica'])){
			if(isset($_POST['identrada'])){
				if(isset($_POST['valoracion'])){
					$critica = $_POST["critica"];
					$correo_creador = $_SESSION['user'];
					$identrada = $_POST["identrada"];
					$valoracion = $_POST["valoracion"];

					$consulta = "update final_criticas set critica='$critica',valoracion = '$valoracion' where id_entrada = id_entrada AND correo_creador = '$correo_creador'";
					if($bd -> query($consulta))
						return 0;
					else
						return -4;
				}
				else
					return -3;
			}
			else
				return -2;
		}
		else
			return -1;
	}

	function meliminarcritica(){
		/*
			Devuelve: resultado del intento de eliminacion de una critica
			1 - Se ha eliminado correctamente
			0 - No se ha eliminado correctamente
		*/
		$bd = conexion();

		if($bd == null){ //No hay conexión
			mcierrasesion();
			return -8080;
		}

		$correo_creador = $_SESSION['user'];
		$identrada = $_POST["identrada"];
		$consulta = "delete from final_criticas where id_entrada=$identrada AND correo_creador='$correo_creador'";
		if ($bd->query($consulta) == TRUE) {
			return 1;
		} else {
			return 0;
		}	
	}

	/** Función que permite obtener el número de notificaciones de un usuario **/
	function mnumnotificaciones(){
		$con = conexion();

		if($con == null){ //No hay conexión
			mcierrasesion();
			return -8080;
		}

		$correo = $_SESSION['user'];
		$consulta = "select count(id_notificacion) as num_notificaciones from final_notificaciones where correo_electronico = '$correo'";

		return $con->query($consulta);
	}

	/** Función que permite recuperar las entradas de un usuario **/
	function mnotificaciones(){
		$con = conexion();

		if($con == null){ //No hay conexión
			mcierrasesion();
			return -8080;
		}

		if(isset($_GET["pagina"])){ //Comprobar que el parámetro página no sea nulo.
			$pagina_actual = $_GET["pagina"]; //Obtener el número de página actual en la que se encuentra el usuario.
			$pagina_actual--; //Como el número de página comienza por 1 en vez de por 0 reducir en 1 el número de la página.
			$desplazamiento = $pagina_actual * ELEMENTOS_PAGINA; //Calular el desplazamiento en función de la página actual * el número de notificaciones a mostrar por página

			$correo = $_SESSION['user'];
			$consulta = "select * from final_notificaciones where correo_electronico = '$correo' order by fecha limit $desplazamiento, ".ELEMENTOS_PAGINA;

			return $con->query($consulta);
		}
		return null;
	}

	/** Funcion que permite recuperar una notificación seleccionada por el usuario **/
	function mrecuperanotificacion(){
		$con = conexion();

		if($con == null){ //No hay conexión
			mcierrasesion();
			return -8080;
		}

		if(isset($_POST['notificacion'])){
			$id_notificacion = $_POST['notificacion'];
			$correo = $_SESSION['user'];

			$consulta = "select * from final_notificaciones where id_notificacion = '$id_notificacion' and correo_electronico = '$correo'";

			if($result = $con->query($consulta)){
				$consulta2 = "delete from final_notificaciones where id_notificacion = '$id_notificacion' and correo_electronico = '$correo'";
				$con->query($consulta2);
				return $result;
			}

		}
		return null;
	}

	/** Función que permite eliminar una notificación de la BBDD **/
	function meliminarnotificacion(){
		$con = conexion();

		if($con == null){ //No hay conexión
			mcierrasesion();
			return -8080;
		}

		if(isset($_GET['notificacion'])){
			$id_notificacion = $_GET['notificacion'];
			$correo = $_SESSION['user'];

			$consulta = "delete from final_notificaciones where id_notificacion = '$id_notificacion' and correo_electronico = '$correo'";
			if($con->query($consulta))
				return 0;
			else //Error al eliminar la notificación de la BBDD
				return -1;
		}
		else //Notificacion no encontrada
			return -2;
	}

	function mactualizartiempo(){
		$_SESSION['tiempo'] = time();
	}

	/** Función que permite recuperar una crítica seleccionada por un usuario **/
	function mrecuperacritica(){
		$con = conexion();

		if($con == null){ //No hay conexión
			mcierrasesion();
			return -8080;
		}

		if(isset($_POST['entrada'])){
			if(isset($_POST['creador'])){
				$id_entrada = $_POST['entrada'];
				$correo = $_POST['creador'];

				$consulta = "select correo_creador,critica, valoracion, fecha, nick from final_usuarios inner join final_criticas on (final_usuarios.correo_electronico = final_criticas.correo_creador) where id_entrada = '$id_entrada' and correo_creador = '$correo'";
				$consulta2 = "select titulo from final_entradas inner join final_criticas on (final_entradas.id_entrada = final_criticas.id_entrada) where final_entradas.id_entrada = '$id_entrada'";

				if($result = $con->query($consulta) and ($result2 = $con->query($consulta2)))
					return array($result,$result2);
			}
		}
		return null;

	}

	/** Función que permite recuperar el número de posts del foro **/
	function mnumeropost(){
		$con = conexion();

		if($con == null){ //No hay conexión
			mcierrasesion();
			return -8080;
		}

		if(isset($_GET['letras']) && strcmp($_GET['letras'], " ") != 0){
			$letras = $_GET['letras'];
			$consulta = "select count(id_post) as num_posts from final_posts where titulo like '$letras%'";
		}
		else
			$consulta = "select count(id_post) as num_posts from final_posts";

		if($result = $con -> query($consulta))
			return $result;
		else
			return null;
	}

	/** Función que permite recuperar los comentarios asociados a un post determinado **/
	function mrecuperacomentarios(){
		$con = conexion();

		if($con == null){ //No hay conexión
			mcierrasesion();
			return -8080;
		}

		if(isset($_POST['pagina_actual'])){ //Comprobar que el parámetro página no sea nulo.
			if (isset($_POST['idpost'])) { //Comprobar el id del post
				$id_post = $_POST['idpost'];
				$pagina_actual = $_POST['pagina_actual']; //Obtener el número de página actual en la que se encuentra el usuario.
				$pagina_actual--; //Como el número de página comienza por 1 en vez de por 0 reducir en 1 el número de la página.
				$desplazamiento = $pagina_actual * COMENTARIOS_PAGINA; //Calular el desplazamiento en función de la página actual * el número de comentarios a mostrar por página

				$consulta = "select final_comentarios.*, final_usuarios.nick from final_comentarios inner join final_usuarios on (final_usuarios.correo_electronico = final_comentarios.correo_escritor) where id_post = '$id_post' order by fecha desc limit $desplazamiento, ".COMENTARIOS_PAGINA;
				if($result = $con->query($consulta))
					return $result;
			}
		}
		return null;
	}

	/** Función que permite comprobar el cambio de datos de perfil del usuario **/
	function mcambiodatosperfil(){
		$con = conexion();

		if($con == null){ //No hay conexión
			mcierrasesion();
			return -8080;
		}

		$datos_modificados = 0; //Variable que indica si los datos han sido modificados o no uçpor el usuario.
		//Inicializar las variables por si algún campo no es modificado:
		$correo = $_SESSION['user'];
		$nombre = $_SESSION['nombre'];
		$ap1 = $_SESSION['ap1'];
		$ap2 = $_SESSION['ap2'];
		$nick = $_SESSION['nick'];
		$direccion = $_SESSION['direccion'];
		$provincia = $_SESSION['provincia'];
		$municipio = $_SESSION['municipio'];

		if(isset($_POST['nombre'])){
			if(!empty($_POST['nombre']) && strlen($_POST['nombre']) > 0){
				$nombre = $_POST['nombre'];
				$datos_modificados = 1;
				$aux = strlen(preg_replace('/\s/', '', $nombre)); //Eliminar todos los espacios en blanco
				if($aux >= MIN_NOMBRE && $aux <= MAX_NOMBRE){ //Comprobar longitud
					if(preg_match('/[^A-Za-z]/', $nombre)) //Comprobar los caracteres
						return -1;
				}
				else
					return -2;
			}
		}
		if(isset($_POST['ap1'])){
			if(!empty($_POST['ap1']) && strlen($_POST['ap1']) > 0){
				$ap1 = $_POST['ap1'];
				$datos_modificados = 1;
				$aux = strlen(preg_replace('/\s/', '', $ap1)); //Eliminar todos los espacios en blanco
				if($aux >= MIN_APELLIDO && $aux <= MAX_APELLIDO){ //Comprobar longitud
					if(preg_match('/[^A-Za-z]/', $ap1)) //Comprobar los caracteres
						return -3;
				}
				else
					return -4;
			}
		}
		if(isset($_POST['ap2'])){
			if(!empty($_POST['ap2']) && strlen($_POST['ap2']) > 0){
				$ap2 = $_POST['ap2'];
				$datos_modificados = 1;
				$aux = strlen(preg_replace('/\s/', '', $ap2)); //Eliminar todos los espacios en blanco
				if($aux >= MIN_APELLIDO && $aux <= MAX_APELLIDO){ //Comprobar longitud
					if(preg_match('/[^A-Za-z]/', $ap2)) //Comprobar los caracteres
						return -5;
				}
				else
					return -6;
			}
		}
		if(isset($_POST['nick'])){
			if(!empty($_POST['nick']) && strlen($_POST['nick']) > 0){
				$nick = $_POST['nick'];
				$datos_modificados = 1;
				$aux = strlen(preg_replace('/\s/', '', $nick)); //Eliminar todos los espacios en blanco
				if($aux >= MIN_NICK && $aux <= MAX_NICK){ //Comprobar longitud
					if(!preg_match('/[^A-Za-z0-9]/', $nick)){ //Comprobar los caracteres
						$consulta = "select count(nick) as num_nick from final_usuarios where nick = '$nick'";
						if($result = $con->query($consulta)){
							$num_nick = $result -> fetch_assoc();
							if($num_nick['num_nick'] != 0) 
								return -7; //Nick en la BBDD
						}
						else
							return -8;
					}
					else	
						return -9;
				}
				else
					return -10;
			}
		}

		if(isset($_POST['direccion'])){
			if(!empty($_POST['direccion']) && strlen($_POST['direccion']) > 0){
				$datos_modificados = 1;
				$direccion = $_POST['direccion'];
				$aux = strlen(preg_replace('/\s/', '', $direccion)); //Eliminar todos los espacios en blanco
				if(!($aux >= MIN_DIRECCION && $aux <= MAX_DIRECCION)) //Comprobar longitud
					return -11;
			}
		}

		if(isset($_POST['provincia'])){
			if(!empty($_POST['provincia'])){
				$datos_modificados = 1;
				$provincia = $_POST['provincia'];
				$check_provincia = mcheckprovincia();
				if($check_provincia != 0)
					return $check_provincia;
				else{
					if(!empty($_POST['municipio'])){
						$municipio = $_POST['municipio'];
						$datos_modificados = 1;
						$check_municipio = mcheckmunicipio();
						if($check_municipio != 0)
							return $check_municipio;
					}
					else
						return -13;
				}
			}
		}

		if($datos_modificados == 1){
			$consulta = "update final_usuarios set nombre = '$nombre', apellido1 = '$ap1', apellido2= '$ap2', nick = '$nick', direccion = '$direccion', provincia = '$provincia', municipio = '$municipio' where correo_electronico = '$correo'";
			if($con -> query($consulta))
				return 0;
			else
				return -15;
		}
		else
			return 1;
	}
	
	function mactualizarimagenes(){
		$bd = conexion();
		$identrada = $_POST['identrada'];
		$im_principal = $_POST['imagen_principal'];
		$principal = "select * from final_imagenes where id_entrada = $identrada and id_imagen = $im_principal";
		$secundarias = "select * from final_imagenes where id_entrada = $identrada and id_imagen <> $im_principal";
		return array($bd->query($principal),$bd->query($secundarias));
	}

	/** Función que permite borrar todas las notificaciones de un usuario **/
	function meliminartodasnotificaciones(){
		$con = conexion();

		if($con == null){ //No hay conexión
			mcierrasesion();
			return -8080;
		}

		$correo = $_SESSION['user'];
		$consulta = "delete from final_notificaciones where correo_electronico = '$correo'";

		if($con->query($consulta))
			return 0;
		else
			return -1;
	}

	/** Función que permite obtener el perfil de un usuario ajeno **/
	function mobtenerperfilajeno(){

		if(isset($_GET['nick'])){
			$con = conexion();

			if($con == null){ //No hay conexión
				mcierrasesion();
				return -8080;
			}
			$nick = $_GET['nick'];
			$cons_correo = "select correo_electronico from final_usuarios where nick = '$nick'";
			$res = $con->query($cons_correo);
			$datos_correo = $res->fetch_assoc();
			$correo = $datos_correo['correo_electronico'];
			$nick = $_GET['nick'];
			$consulta = "select nombre, apellido1, apellido2, nick, tipo, imagen_perfil from final_usuarios where nick = '$nick'";
			$entradas = "select count(id_entrada) as num_entradas from final_entradas where correo_creador = '$correo'";
			$criticas = "select count(*) as num_criticas from final_criticas where correo_creador = '$correo'";
			$val_media = "select avg(valoracion) as val_media from final_criticas where correo_creador = '$correo'";
			if($result = $con -> query($consulta) and ($result2 = $con -> query($entradas)) and ($result3 = $con -> query($criticas)) and ($result4 = $con -> query($val_media))){
				return array($result,$result2,$result3,$result4);
			}
		}
		return null;
	}
?>