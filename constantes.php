<?php
	//Constantes:
	
	define ("MAX_TIEMPO_SESION", "600"); //Segundos

	//Constantes para el registro:
	define("MIN_NOMBRE","3");
	define("MIN_APELLIDO","4");
	define("MAX_APELLIDO","30");
	define("MAX_NOMBRE","30");
	define("MAX_PASSWD","100");
	define("MIN_PASSWD","6");
	define("MAX_CORREO","60");
	define("MAX_DIRECCION","50");
	define("MIN_DIRECCION","5");
	define("MAX_NICK", "30");
	define("MIN_NICK", "5");

	//Constantes para la solicitud de verificación:
	define("MAX_TEXT", "500");
	define("MIN_TEXT", "50");

	//Constantes para el listado de entradas y criticas:
	define("ELEMENTOS_PAGINA","5");
	define("LIMITE_PAGINA","10");
	define("TAMANO_DESCRIPCION","80");
	define("TAMANO_CRITICA", "150");
	define("PUNTUACION_MAXIMA", "10");

	//Constantes para modificar una entrada
	define("MAX_TITULO","100");
	define("MIN_TITULO","10");
	define("MIN_CONTENIDO","10");
	define("MAX_CONTENIDO", "1000");

	//Constantes para el perfil:
	define("MAX_IMAGEN","2000000"); //2MB

	//Constantes para notificaciones:
	define("MAX_DESC_NOTIFICACION","20");

	//Constantes para el foro:
	define("COMENTARIOS_PAGINA","4");
	define("MIN_COMENTARIO", "20");
	define("MAX_COMENTARIO", "500");


?>
