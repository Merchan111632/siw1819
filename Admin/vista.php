<?php  
	//Incluir el fichero con constantes:
	include_once("constantes.php");

	/** Función que muestra el resultado de no encontrar una página web **/
	function vpaginanoencontrada($almacenamiento){
		
		if(session_status() != PHP_SESSION_NONE && isset($_SESSION['user'])){
			$cadena = file_get_contents("error404.html");
			if(!empty($almacenamiento)){
				$info = $almacenamiento -> fetch_assoc();
				$cadena = str_replace("##Espacio##", $info['tamano'], $cadena);
			}
			else
				$cadena = str_replace("##Espacio##", '?', $cadena);
		}
		else{
			$cadena = file_get_contents("error404_no_user.html");
		}
		echo $cadena;
	}

	/** Función que muestra el menú de opciones del administrador **/
	function vmostraropcionesadmin($estadisticas, $almacenamiento){
		$cadena =  file_get_contents("menu.html");

		if(!empty($almacenamiento)){
			$info = $almacenamiento -> fetch_assoc();
			$cadena = str_replace("##Espacio##", $info['tamano'], $cadena);
		}
		else
			$cadena = str_replace("##Espacio##", '?', $cadena);

		if(!empty($estadisticas)){
			$usuarios = $estadisticas[0];
			$bloqueados = $estadisticas[1];
			$verificados = $estadisticas[2];
			$posts = $estadisticas[3];
			$entradas = $estadisticas[4];
			$descuentos = $estadisticas[5];

			if(!empty($usuarios)){
				$info = $usuarios -> fetch_assoc();
				$cadena = str_replace("##NumUsuarios##", $info['num_usuarios'], $cadena);
			}
			else
				$cadena = str_replace("##NumUsuarios##", '-', $cadena);

			if(!empty($bloqueados)){
				$info = $bloqueados -> fetch_assoc();
				$cadena = str_replace("##Bloqueados##", $info['num_bloqueados'], $cadena);
			}
			else
				$cadena = str_replace("##Bloqueados##", '-', $cadena);

			if(!empty($verificados)){
				$info = $verificados -> fetch_assoc();
				$cadena = str_replace("##Verificados##", $info['num_verificados'], $cadena);
			}
			else
				$cadena = str_replace("##Verificados##", '-', $cadena);

			if(!empty($posts)){
				$info = $posts -> fetch_assoc();
				$cadena = str_replace("##NumeroPost##", $info['num_posts'], $cadena);
			}
			else
				$cadena = str_replace("##NumeroPost##", '-', $cadena);

			if(!empty($entradas)){
				$info = $entradas -> fetch_assoc();
				$cadena = str_replace("##NumeroEntradas##", $info['num_entradas'], $cadena);
			}
			else
				$cadena = str_replace("##NumeroEntradas##", '-', $cadena);

			if(!empty($descuentos)){
				$info = $descuentos -> fetch_assoc();
				$cadena = str_replace("##NumeroDescuentos##", $info['num_descuentos'], $cadena);
			}
			else
				$cadena = str_replace("##NumeroDescuentos##", '-', $cadena);
		}
		else{
			$cadena = str_replace("##NumUsuarios##", '-', $cadena);
			$cadena = str_replace("##Bloqueados##", '-', $cadena);
			$cadena = str_replace("##Verificados##", '-', $cadena);
			$cadena = str_replace("##NumeroPost##", '-', $cadena);
			$cadena = str_replace("##NumeroEntradas##", '-', $cadena);
			$cadena = str_replace("##NumeroDescuentos##", '-', $cadena);
		}

		echo $cadena;
	}

	/** Función que muestra el login del administrador **/
	function vloginadmin(){
		echo file_get_contents("login.html");
	}
	
	/** Función que muestra el cierre de sesión de un usuario:  **/
	function vmostrarcierresesion(){
		echo file_get_contents("cerrarSesion.html");
	}

	/*** Funcion encagrada de mostrar mensajes 
		Los mensajes seran de dos tipos:
		-0 mensaje con url.
		-1 mensaje sin url.
	***/
	function mostrarmensaje($titulo, $texto,$tipo) {
		if($tipo == 1)
			$mensaje = file_get_contents("mensaje.html");
		else{
			if(session_status() != PHP_SESSION_NONE && isset($_SESSION['user'])){
				$mensaje = file_get_contents("mensaje_url_user.html");
			}
			else
				$mensaje = file_get_contents("mensaje_url.html");
		}
		$mensaje = str_replace("##titulo##", $titulo, $mensaje);
		$mensaje = str_replace("##texto##", $texto, $mensaje);

		echo $mensaje;
	}

	/** Función que muestra el resultado del login de un administrador **/
	function vmostrarloginadmin($valor){
		switch ($valor) {
			case 0:
				mostrarmensaje("Inicio de sesión correcto", "Bienvenido administrador/a ".$_SESSION['nombre']." ".$_SESSION['ap1']." ".$_SESSION['ap2']." a Merchacine",0);
				break;
			case -1:
				mostrarmensaje("Fallo al iniciar sesión", "Correo o contraseña no validos",1);
				break;
			case -2:
				mostrarmensaje("Fallo al iniciar sesión", "No se ha podido acceder a la base de datos, intentelo denuevo mas tarde",1);
				break;
			case -6:
				mostrarmensaje("Fallo al iniciar sesión", "La constraseña solo debe contener letras y números",1);
				break;
			case -8:
				mostrarmensaje("Fallo al iniciar sesión", "La longitud de la contraseña debe ser de al menos ".MIN_PASSWD." y como mucho ".MAX_PASSWD." carácteres",1);
				break;
			case -13:
				mostrarmensaje("Fallo al iniciar sesión", "La contraseña está vacia.",1);
				break;
			case -5:
				mostrarmensaje("Fallo al iniciar sesión", "Ha introducido un correo que no es válido",1);
				break;
			case -4:
				mostrarmensaje("Fallo al iniciar sesión", "La longitud del correo excede el límite de ".MAX_CORREO." carácteres",1);
				break;
			case -3:
				mostrarmensaje("Fallo al iniciar sesión", "El correo electrónico está vacio.",1);
				break;
			case -9:
				mostrarmensaje("Fallo al iniciar sesión", "Usted está bloqueado contacte con el administrador para obtener detalles.",1);
				break;
			case -7:
				mostrarmensaje("Fallo al iniciar sesión", "No tiene permiso para acceder aquí",1);
				break;
			case -31:
				mostrarmensaje("Fallo al iniciar sesión", "Debe hacer click en el Captcha",1);
				break;
		}
	}

	/** Funcion que muestra el estado de la sesión de un usuario en función del parámetro que recibe. **/
	function vmostrarsesion($valor){
		switch ($valor) {
			case 0:
				mostrarmensaje("Se ha cerrado su sesión", "Se ha cerrado sesión correctamente",0);
				break;
			case -6:
				mostrarmensaje("Se ha cerrado su sesión", "Ha excedido el tiempo límite de inactividad, vuelva a iniciar sesión.",0);
				break;
			case -1:
				mostrarmensaje("Se ha cerrado su sesión", "No se encuentra registrado en esta página.",0);
				break;
			case -2:
				mostrarmensaje("Se ha cerrado su sesión", "No se pudo obtener información de la base de datos.",0);
				break;
			case -3:
				mostrarmensaje("Se ha cerrado su sesión", "Ha sido bloqueado contacte con el administrador para obtener más información",0);
				break;
			case -3:
				mostrarmensaje("Se ha cerrado su sesión", "No tiene permisos para acceder a este sitio",0);
				break;
			case -8080:
				mostrarmensaje("Fallo de conexión", "No se ha podido establecer conexión con la base de datos. Su sesión ha sido cerrada.",0);
				break;
		}
	}

	/** Función que muestra la página del listado de usuarios (sobre esta se mostrarán los usuarios de merchacine) **/
	function vlistadousuarios($almacenamiento){
		$cadena = file_get_contents("listadousuarios.html");
		if(!empty($almacenamiento)){
			$info = $almacenamiento -> fetch_assoc();
			$cadena = str_replace("##Espacio##", $info['tamano'], $cadena);
		}
		else
			$cadena = str_replace("##Espacio##", '?', $cadena);
		echo $cadena;
	}

	/** Función que muestra un listado con los usuarios registrados de la página web **/
	function vmostrarusuarios($usuarios,$numusuarios){
		$cadena = file_get_contents("usuarios.html");
		$trozos = explode("##fila##",$cadena);

		$aux = "";
		$cuerpo = "";

		if(isset($_GET["pagina"])){
			$pagina_actual = $_GET["pagina"]; //Obtener la página actual 
			if(!empty($usuarios)){ //Comprobar que el parámetro usuarios no sea nulo
					while($usuario = $usuarios->fetch_assoc()){ //Leer las entradas recuperadas y sustituirlas en el listado iterando
						$aux = $trozos[1];
						$aux = str_replace("##Usuario##",$usuario['apellido1']." ".$usuario['apellido2'].",".$usuario['nombre'], $aux);
						$aux = str_replace("##Nick##",$usuario['nick'], $aux);
						$aux = str_replace("##Correo##", $usuario["correo_electronico"], $aux);
						if($usuario['bloqueado'] == 0){
							$aux = str_replace("##Estado##",'No bloqueado', $aux);
							$aux = str_replace("##Accion##",'Bloquear Usuario', $aux);
							$aux = str_replace("##id##",'3', $aux);
							$aux = str_replace("##coloraccion##",'red', $aux);
						}
						else{
							$aux = str_replace("##Estado##",'Bloqueado', $aux);
							$aux = str_replace("##Accion##",'Desbloquear Usuario', $aux);
							$aux = str_replace("##id##",'4', $aux);
							$aux = str_replace("##coloraccion##",'green', $aux);
						}
						$cuerpo .= $aux;
					}

					$cadena = $trozos[0] . $cuerpo . $trozos[2];

					//Llegado a este punto ya estarían mostrados todos los usuarios permitidos.

					//Hay que paginar la página actual
					if(!empty($numusuarios)){ //Comprobar que el parámetro numusuarios no sea nulo
						$datos = $numusuarios->fetch_assoc();
						$numero_elementos = $datos["num_usuarios"];
						paginar($cadena,$numero_elementos,$pagina_actual);
					}
					else //Numero de usuarios nulo
						mostrarmensaje("No se detectó en número de usuarios", "No se recibió el número de usuarios a mostrar",1);
			}
			else //Si el parámetro usuarios es nulo:
				mostrarmensaje("No hay usuarios para mostrar", "No se encontraron usuarios para mostrar, intentelo de nuevo más tarde.",1);
		}
		else //Página actual nula:
			mostrarmensaje("No se pudo obtener la página actual", "No se encontró el número de la página actual",1);
	}

	/**Funcion que permite realizar la paginación de una página, recibe como parámetros la cadena con toda la información a mostrar de la página,
	el número total de elementos leidos de la base de datos y la pagina actual. Añade a el final de la cadena recibida la paginación **/
	function paginar($cadena,$numero_elementos,$pagina_actual){
		$paginas = intdiv($numero_elementos, ELEMENTOS_PAGINA); //Dividir el número de elementos entre las entradas por página para saber cuantas páginas mostrar
		$resto = $numero_elementos % ELEMENTOS_PAGINA; //Determinar el resto de la división (Si se obtiene un resto distinto de cero añadir una página más)
		if ($resto > 0) {
			$paginas++;
		}
		//Comprobar si el usuario está haciendo una busqueda:
		$letra = "";
		if(isset($_GET['letra']))
			$letra = $_GET['letra'];
		//Sustituir el botón siguiente y el botón anterior:
		//Anterior:
		$trozos = explode("##anterior##", $cadena);
		if($pagina_actual > 1){ //Si la página actual no es la primera entonces mostrar el botón a la página anteriror.
			$pagina_anterior = $pagina_actual - 1;
			$aux = $trozos[1];
			$aux = str_replace("##paginaanterior##", $pagina_anterior, $aux);
			$aux = str_replace("##letra##", $letra, $aux);
			$cadena = $trozos[0] . $aux . $trozos[2];				
		}
		else { //Si es la primera página simplemente ocultar el botón anterior
			$cadena = $trozos[0] . $trozos[2];	
		}
		//Siguiente:
		$trozos = explode("##siguiente##", $cadena);
		if($pagina_actual < $paginas){ //Si la página actual no es la última entonces mostrar el botón a la página siguiente.
			$pagina_siguiente = $pagina_actual + 1;
			$aux = $trozos[1];
			$aux = str_replace("##paginasiguiente##", $pagina_siguiente, $aux);
			$aux = str_replace("##letra##", $letra, $aux);
			$cadena = $trozos[0] . $aux . $trozos[2];				
		}
		else { //Si la pagina actual es la última ocultar el botón siguiente
			$cadena = $trozos[0] . $trozos[2];	
		}
		//Por ultimo colocar los números de las páginas:

		$trozos = explode("##paginas##", $cadena);
		$cuerpo = "";

		//Primero determinar cual va a ser el primer número de página que se va a mostrar
		if($pagina_actual <= LIMITE_PAGINA)
			$pagina_inicial = 1;
		else
			$pagina_inicial = $pagina_actual - (LIMITE_PAGINA-1);
		//Segundo determinar hasta que página se va a mostrar
		if($paginas > LIMITE_PAGINA)
			$paginas = $pagina_inicial + (LIMITE_PAGINA-1);

		for($i = $pagina_inicial; $i <= $paginas; $i++){
			$aux = $trozos[1];
			$aux = str_replace("##numeropagina##", $i, $aux);
			$aux = str_replace("##letra##", $letra, $aux);

			//Resaltar la página actual (más adelante en css)
			if ($i == $pagina_actual) {
				$aux = str_replace("##color##", "#ffb848", $aux);	
			} else {
				$aux = str_replace("##color##", "white", $aux);
			}
			$cuerpo .= $aux;
			}
		$cadena = $trozos[0] . $cuerpo . $trozos[2];
		echo $cadena;
	}

	/** Función que muestra el resultado de intentar bloquear a un usuario **/
	function vresultadobloqueo($valor){
		switch ($valor) {
			case 0:
				echo "Se ha bloqueado correctamente. El usuario ha sido bloqueado correctamente.";
				break;
			case -1:
				echo"Error al bloquear. No se ha podido bloquear al usuario seleccionado.";
				break;
		}
	}

	/** Función que muestra el resultado de intentar desbloquear a un usuario **/
	function vresultadodesbloqueo($valor){
		switch ($valor) {
			case 0:
				echo "Se ha desbloqueado correctamente. El usuario ha sido desbloqueado correctamente.";
				break;
			case -1:
				echo "Error al desbloquear. No se ha podido desbloquear al usuario seleccionado.";
				break;
		}
	}

	/** Función que muestra la página del listado de solicitudes de verificación (sobre esta se mostrarán las solicitudes de los usuarios de Merchacine) **/
	function vlistaverificaciones($almacenamiento){
		$cadena = file_get_contents("listadoverificaciones.html");
		if(!empty($almacenamiento)){
			$info = $almacenamiento -> fetch_assoc();
			$cadena = str_replace("##Espacio##", $info['tamano'], $cadena);
		}
		else
			$cadena = str_replace("##Espacio##", '?', $cadena);
		echo $cadena;
	}

	/** Función que muestra las solicitudes de verificación enviadas por los usuarios de Merchacine **/
	function vmostrarverificaciones($solicitudes,$numsolicitudes){
		$cadena = file_get_contents("solicitudes.html");
		$trozos = explode("##fila##",$cadena);

		$aux = "";
		$cuerpo = "";

		if(isset($_GET["pagina"])){
			$pagina_actual = $_GET["pagina"]; //Obtener la página actual 
			if(!empty($solicitudes)){ //Comprobar que el parámetro usuarios no sea nulo
					while($solicitud = $solicitudes->fetch_assoc()){ //Leer las entradas recuperadas y sustituirlas en el listado iterando
						$aux = $trozos[1];
						$aux = str_replace("##Nick##", $solicitud['nick'], $aux);
						$aux = str_replace("##Correo##", $solicitud['correo_electronico'], $aux);
						$aux = str_replace("##Solicitud##", $solicitud['correo_electronico'], $aux);
						$aux = str_replace("##Fecha##", $solicitud['fecha'], $aux);
						$cuerpo .= $aux;
					}

					$cadena = $trozos[0] . $cuerpo . $trozos[2];

					//Llegado a este punto ya estarían mostrados todos los usuarios permitidos.

					//Hay que paginar la página actual
					if(!empty($numsolicitudes)){ //Comprobar que el parámetro numsolicitudes no sea nulo
						$datos = $numsolicitudes->fetch_assoc();
						$numero_elementos = $datos["num_solicitudes"];
						paginar($cadena,$numero_elementos,$pagina_actual);
					}
					else //Numero de solicitudes nulo
						mostrarmensaje("No se detectó en número de solicitudes", "No se recibió el número de solicitudes a mostrar",1);
			}
			else //Si el parámetro solicitudes es nulo:
				mostrarmensaje("No hay solicitudes para mostrar", "No se encontraron solicitudes para mostrar, intentelo de nuevo más tarde.",1);
		}
		else //Página actual nula:
			mostrarmensaje("No se pudo obtener la página actual", "No se encontró el número de la página actual",1);
	}

	/** Función que permite mostrar una solicitud que ha seleccionado el administrador **/
	function vmostrarsolicitud($solicitud, $almacenamiento){
		$cadena = file_get_contents('solicitud.html');
		if(!empty($almacenamiento)){
			$info = $almacenamiento -> fetch_assoc();
			$cadena = str_replace("##Espacio##", $info['tamano'], $cadena);
		}
		else
			$cadena = str_replace("##Espacio##", '?', $cadena);

		if (isset($_POST['solicitud'])){ //Comprobar que el parametro solicitud no sea nulo.
			if(!empty($solicitud)){ //Comprobar que el contenido de la entrada no es vacio:
				$info_solicitud= $solicitud->fetch_assoc();
				$cadena = str_replace("##Autor##", $info_solicitud['correo_electronico'], $cadena);
				$cadena = str_replace("##Nick##", $info_solicitud['nick'], $cadena);
				$cadena = str_replace("##Solicitud##", $info_solicitud['correo_electronico'], $cadena);
				$cadena = str_replace("##Fecha##", $info_solicitud['fecha'], $cadena);
				$aux_descripcion = wordwrap($info_solicitud['descripcion'], TAMANO_SOLICITUD, "\n", false);
				$cadena = str_replace("##Descripcion##", $aux_descripcion, $cadena);
				$cadena = str_replace("##Correo##", $info_solicitud['correo_electronico'], $cadena);

				echo $cadena;
			}
			else
				mostrarmensaje("No se pudo obtener la información", "No se pudo obtener la información de la solicitud selccionada.",0);
		}
		else
			mostrarmensaje("No se pudo obtener la información", "No se pudo encontrar la solicitud seleccionada.",0);
	}

	/** Función que muestra el resultado de verificar un usuario **/
	function vresultadoverificar($valor){
		switch ($valor) {
			case 0:
				mostrarmensaje("Se ha verificado correctamente", "El usuario ha sido verificado correctamente.",0);
				break;
			case -1:
				mostrarmensaje("Error al eliminar", "No se ha podido eliminar la solicitud de la base de datos.",0);
				break;
			case -2:
				mostrarmensaje("Error al verificar", "No se ha podido verificar a el usuario seleccionado",0);
				break;
			case -3:
				mostrarmensaje("Error al verificar", "No se encontró la solicitud de verificación seleccionada",0);
				break;
		}
	}

	/** Función que muestra el resultado de verificar un usuario **/
	function vresultadorechazar($valor){
		switch ($valor) {
			case 0:
				mostrarmensaje("Se ha rechazado correctamente", "La solicitud del usuario ha sido rechazada correctamente",0);
				break;
			case -1:
				mostrarmensaje("Error al eliminar", "No se ha podido eliminar la solicitud de la base de datos.",0);
				break;
			case -1:
				mostrarmensaje("Error al eliminar", "No se ha podido actualizar el campo tipo del usuario seleccionado.",0);
				break;
			case -3:
				mostrarmensaje("Error al verificar", "No se encontró la solicitud de verificación seleccionada",0);
				break;
		}
	}

	/** Función que muestra la página del listado de solicitudes de modificación (sobre esta se mostrarán las solicitudes de los usuarios de Merchacine) **/
	function vlistadomodificaciones($almacenamiento){
		$cadena = file_get_contents("listadomodificacion.html");
		if(!empty($almacenamiento)){
			$info = $almacenamiento -> fetch_assoc();
			$cadena = str_replace("##Espacio##", $info['tamano'], $cadena);
		}
		else
			$cadena = str_replace("##Espacio##", '?', $cadena);
		echo $cadena;
	}

	/** Función que muestra las solicitudes de modificación enviadas por los usuarios de Merchacine **/
	function vmostrarmodificaciones($solicitudes,$numsolicitudes){
		$cadena = file_get_contents("modificaciones.html");
		$trozos = explode("##fila##",$cadena);

		$aux = "";
		$cuerpo = "";

		if(isset($_GET["pagina"])){
			$pagina_actual = $_GET["pagina"]; //Obtener la página actual 
			if(!empty($solicitudes)){ //Comprobar que el parámetro usuarios no sea nulo
					while($solicitud = $solicitudes->fetch_assoc()){ //Leer las entradas recuperadas y sustituirlas en el listado iterando
						$aux = $trozos[1];
						$aux = str_replace("##Nick##", $solicitud['nick'], $aux);
						$aux = str_replace("##Correo##", $solicitud['correo_creador'], $aux);
						$aux = str_replace("##User##", $solicitud['correo_creador'], $aux);
						$aux = str_replace("##Solicitud##", $solicitud['id_entrada'], $aux);
						$aux = str_replace("##Fecha##", $solicitud['fecha'], $aux);
						$aux = str_replace("##Fecha##", $solicitud['fecha'], $aux);
						$cuerpo .= $aux;
					}

					$cadena = $trozos[0] . $cuerpo . $trozos[2];

					//Llegado a este punto ya estarían mostrados todos los usuarios permitidos.

					//Hay que paginar la página actual
					if(!empty($numsolicitudes)){ //Comprobar que el parámetro numsolicitudes no sea nulo
						$datos = $numsolicitudes->fetch_assoc();
						$numero_elementos = $datos["num_solicitudes"];
						paginar($cadena,$numero_elementos,$pagina_actual);
					}
					else //Numero de solicitudes nulo
						mostrarmensaje("No se detectó en número de solicitudes", "No se recibió el número de solicitudes a mostrar",1);
			}
			else //Si el parámetro solicitudes es nulo:
				mostrarmensaje("No hay solicitudes para mostrar", "No se encontraron solicitudes para mostrar, intentelo de nuevo más tarde.",1);
		}
		else //Página actual nula:
			mostrarmensaje("No se pudo obtener la página actual", "No se encontró el número de la página actual",1);
	}

	/** Función que permite mostrar una solicitud de modificación que ha seleccionado el administrador, así como la entrada original antes de
	la modificación de la misma **/
	function vmostrarmodificacion($modificacion,$original,$almacenamiento){
		$cadena = file_get_contents('modificacion.html');
		if(!empty($almacenamiento)){
			$info = $almacenamiento -> fetch_assoc();
			$cadena = str_replace("##Espacio##", $info['tamano'], $cadena);
		}
		else
			$cadena = str_replace("##Espacio##", '?', $cadena);
		if (isset($_POST['solicitud'])){ //Comprobar que el parametro solicitud no sea nulo.
			if(!empty($modificacion)){ //Comprobar que el contenido de la modificación no es vacio:
				if(!empty($original)){ //Comprobar que el contenido original de la entrada no sea vacio
					$info_modificacion = $modificacion->fetch_assoc();
					$cadena = str_replace("##AutorModificacion##", $info_modificacion['correo_creador'], $cadena);
					$cadena = str_replace("##Nick##", $info_modificacion['nick'], $cadena);
					$cadena = str_replace("##FechaModificacion##", $info_modificacion['fecha'], $cadena);
					$cadena = str_replace("##Contenido##", $info_modificacion['texto'], $cadena);
					$cadena = str_replace("##Titulo##", $info_modificacion['titulo'], $cadena);
					$cadena = str_replace("##Solicitud##", $_POST['solicitud'], $cadena);
					$cadena = str_replace("##User##", $info_modificacion['correo_creador'], $cadena);
					$cadena = str_replace("##Entrada##", $info_modificacion['id_entrada'], $cadena);

					$info_original = $original->fetch_assoc();
					$cadena = str_replace("##AutorOriginal##", $info_original['correo_creador'], $cadena);
					$cadena = str_replace("##NickAutor##", $info_original['nick'], $cadena);
					if($_SESSION['user'] == $info_original['correo_creador'])
						$cadena = str_replace("##tipo##", 1, $cadena);
					else
						$cadena = str_replace("##tipo##", 0, $cadena);
					$cadena = str_replace("##FechaOriginal##", $info_original['fecha_creacion'], $cadena);
					$aux_texto = wordwrap($info_original['texto'], LONGITUD_ENTRADA, "\n", false);
					$cadena = str_replace("##ContenidoOriginal##", $aux_texto, $cadena);
					$aux_titulo = wordwrap($info_original['titulo'], LONGITUD_TITULO, "\n", false);
					$cadena = str_replace("##TituloOriginal##", $aux_titulo, $cadena);
					
					echo $cadena;
				}
				else
					mostrarmensaje("No se pudo obtener la información", "No se pudo obtener la información de la entrada original.",0);
			}
			else
				mostrarmensaje("No se pudo obtener la información", "No se pudo obtener la información de la solicitud de modificación selccionada.",0);
		}
		else
			mostrarmensaje("No se pudo obtener la información", "No se pudo encontrar la solicitud de modificación seleccionada.",0);
	}

	/** Función que muestra el resultado de aceptar una modificacíón **/
	function vaceptarmodificacion($valor){
		switch ($valor) {
			case 0:
				mostrarmensaje("Modificación guardada correctamente", "La modificación ha sido almacenada correctamente",0);
				break;
			case -1:
				mostrarmensaje("Error al eliminar la modificación", "No se ha podido eliminar la modificación de su tabla",1);
				break;
			case -2:
				mostrarmensaje("Error al guardar la modificación", "No se ha podido almacenar la modificación",1);
				break;
			case -3:
				mostrarmensaje("El texto no cumple la longitud", "El texto no cumple el mínimo establecido de ".MIN_CONTENIDO." carácteres",1);
				break;
			case -4:
				mostrarmensaje("El titulo no cumple la longitud", "La longitud del titulo debe estar entre ".MIN_TITULO." y ".MAX_TITULO." carácteres",1);
				break;
			case -5:
				mostrarmensaje("Usuario no detectado", "No se ha encontrado el correo del usuario que ha modificado la entrada, reviselo",1);
				break;
			case -6:
				mostrarmensaje("El contenido es vacio", "El contenido de su modificación esta vacio, reviselo",1);
				break;
			case -7:
				mostrarmensaje("El titulo es vacio", "El titulo de su modificación esta vacio, reviselo",1);
				break;
			case -8:
				mostrarmensaje("Número de solicitud no encontrado", "El número de solicitud no se ha encontrado,reviselo",1);
				break;
		}
	}

	/** Función que muestra el resultado de rechzar una solicitud de modificación enviada por un usuario **/
	function vrechazarmodificacion($valor){
		switch($valor){
			case 0:
				mostrarmensaje("Solicitud de modificación eliminada correctamente", "La solicitud de modificación ha sido eliminada correctamente",0);
				break;
			case -1:
				mostrarmensaje("No se ha podido eliminar la solicitud de modificación", "La solicitud de modificación no senha podido eliminar.",0);
				break;
		}
	}

	/** Función que permite mostar el perfil de un usuario **/
	function vmostrarperfil($imagen, $provincia, $municipio, $almacenamiento){
		$cadena = file_get_contents('perfil.html');

		$cadena = str_replace("##nombre##", $_SESSION['nombre'], $cadena);
		$cadena = str_replace("##apellido1##", $_SESSION['ap1'], $cadena);
		$cadena = str_replace("##apellido2##", $_SESSION['ap2'], $cadena);
		$cadena = str_replace("##correo##", $_SESSION['user'], $cadena);
		$cadena = str_replace("##direccion##", $_SESSION['direccion'], $cadena);
		$cadena = str_replace("##nick##", $_SESSION['nick'], $cadena);


		if(!empty($imagen)){ //Comprobar que la imagen no sea vacia:
			$imagen_perfil = $imagen -> fetch_assoc();
			//Codificar la imagen en base 64:
			$cadena = str_replace("##imagen##", base64_encode($imagen_perfil['imagen_perfil']), $cadena);
		}
		if(!empty($provincia)){ //Comprobar que la provincia no sea nula:
			$provincia_perfil = $provincia -> fetch_assoc();
			$cadena = str_replace("##provincia##", $provincia_perfil['provincia'], $cadena);
		}
		else
			$cadena = str_replace("##provincia##", "No encontrada", $cadena);

		if(!empty($municipio)){ //Comprobar que el municipio no sea nulo:
			$comunidad_perfil = $municipio -> fetch_assoc();
			$cadena = str_replace("##municipio##", $comunidad_perfil['municipio'], $cadena);
		}
		else
			$cadena = str_replace("##municipio##", "No encontrada", $cadena);

		if(isset($_GET['nick']))
			$cadena = $cadena = str_replace("##goBack##", "window.location.reload()", $cadena);
		else
			$cadena = $cadena = str_replace("##goBack##", "window.history.back()", $cadena);

		if(!empty($almacenamiento)){
			$info = $almacenamiento -> fetch_assoc();
			$cadena = str_replace("##Espacio##", $info['tamano'], $cadena);
		}
		else
			$cadena = str_replace("##Espacio##", '?', $cadena);

		echo $cadena;
	}

	/** Función que muestra el resultado de cambiar la contraseña por parte de un usuario **/
	function vcambiopassword($resultado){
		switch($resultado){
			case 0:
				mostrarmensaje("Cambio de contraseña correcto","Se ha cambiado su contraseña correctamente, vuelva a iniciar sesión",0);
				break;
			case -1:
				mostrarmensaje("No se pudo actualizar la contraseña","No se ha podido modificar su contraseña, intentelo de nuevo más tarde",1);
				break;
			//CONTRASEÑA ACTUAL:
			case -2:
				mostrarmensaje("No se pudo actualizar la contraseña", "La contraseña actual no coincide",1);
				break;
			case -3:
				mostrarmensaje("No se pudo actualizar la contraseña", "La contraseña nueva es igual a la actual",1);
				break;
			case -16:
				mostrarmensaje("No se pudo actualizar la contraseña", "La constraseña actual solo pede contener letras mayúsculas, minúsculas y números",1);
				break;
			case -18:
				mostrarmensaje("No se pudo actualizar la contraseña", "La longitud de la contraseña actual debe ser de al menos ".MIN_PASSWD." y como máximo ".MAX_PASSWD." carácteres",1);
				break;
			case -23:
				mostrarmensaje("No se pudo actualizar la contraseña", "La contraseña actual está vacia",1);
				break;
			//NUEVA CONSTRASEÑA:
			case -4:
				mostrarmensaje("No se pudo actualizar la contraseña", "Las contraseñas nuevas no coinciden",1);
				break;
			case -5:
				mostrarmensaje("No se pudo actualizar la contraseña", "La constraseña de confirmación solo pede contener letras mayúsculas, minúsculas y números",1);
				break;
			case -6:
				mostrarmensaje("No se pudo actualizar la contraseña", "La constraseña solo pede contener letras mayúsculas, minúsculas y números",1);
				break;
			case -7:
				mostrarmensaje("No se pudo actualizar la contraseña", "La longitud de la contraseña de confirmación debe ser de al menos ".MIN_PASSWD." y como máximo ".MAX_PASSWD." carácteres",1);
				break;
			case -8:
				mostrarmensaje("No se pudo actualizar la contraseña", "La longitud de la contraseña debe ser de al menos ".MIN_PASSWD." y como máximo ".MAX_PASSWD." carácteres",1);
				break;
			case -12:
				mostrarmensaje("No se pudo actualizar la contraseña", "La constraseña de confirmación está vacia",1);
				break;
			case -13:
				mostrarmensaje("No se pudo actualizar la contraseña", "La contraseña está vacia",1);
				break;
		}
	}

	/** Función que muestra el resultado del proceso de actualización de imagen de perfil de un usario:**/
	function vcambioimagen($resultado){
		switch($resultado){
			case 0:
				mostrarmensaje("Cambio de imagen de perfil correcto","Se ha actualizado su foto de perfil correctamente",0);
				break;
			case -1:
				mostrarmensaje("No se pudo actualizar su imagen de perfil","No se ha podido actualizar su imagen de perfil, intentelo de nuevo más tarde",0);
				break;
			case -2:
				mostrarmensaje("No se pudo actualizar su imagen de perfil", "El formato de la imagen debe ser jpg, jpeg o png",0);
				break;
			case -3:
				mostrarmensaje("No se pudo actualizar su imagen de perfil", "La imagen debe ser menor o igual a ".(MAX_IMAGEN/1000000)." MB",0);
				break;
			case -4:
				mostrarmensaje("No se pudo actualizar su imagen de perfil", "El fichero seleccionado no es una imagen",0);
				break;
			case -5:
				mostrarmensaje("No se pudo actualizar su imagen de perfil", "El fichero está vacio",0);
				break;
		}
	}

	function vmostrarforos($resultado, $almacenamiento){
		$cadena = file_get_contents("listapost.html");
		if(!empty($almacenamiento)){
			$info = $almacenamiento -> fetch_assoc();
			$cadena = str_replace("##Espacio##", $info['tamano'], $cadena);
		}
		else
			$cadena = str_replace("##Espacio##", '?', $cadena);
		$trozos = explode('##fila##', $cadena);
		$aux = "";
		$cuerpo = "";
		$tipo_usuario = $_SESSION['tipo'];
		$correo_usuario = $_SESSION['user'];
		while ($datos = $resultado->fetch_assoc()) {
			$aux = $trozos[1];
			$aux = str_replace("##titulo##", substr($datos["titulo"],0,TAMANO_TITULO).'...', $aux);
			$aux = str_replace("##idpost##", $datos["id_post"], $aux);
			$aux = str_replace("##creador##", $datos["nick"], $aux);
			if($correo_usuario == $datos["correo_creador"])
					$aux = str_replace("##tipo##", 1, $aux);
				else
					$aux = str_replace("##tipo##", 0, $aux);
			$aux = str_replace("##ncomentarios##", $datos["ncomentarios"], $aux);
			$aux = str_replace("##Fecha##", $datos["fecha"], $aux);
			$cuerpo .= $aux;
		}

		if ($tipo_usuario == 3){
			$cola = $trozos[2];
		}else{
			$cola = "";
		}
		echo $trozos[0] . $cuerpo . $cola;
	}

	function vmostrarforo($resultados, $almacenamiento){
		if(!empty($resultados)){
			$cadena = file_get_contents("post.html");
			if(!empty($almacenamiento)){
				$info = $almacenamiento -> fetch_assoc();
				$cadena = str_replace("##Espacio##", $info['tamano'], $cadena);
			}
			else
				$cadena = str_replace("##Espacio##", '?', $cadena);
			$trozos = explode("##tipousuario##", $cadena);
			$aux1 = "";
			$aux2 = "";

			if(isset($_SESSION['tipo']))
				$tipo_usuario = $_SESSION['tipo'];
			else
				$tipo_usuario = -1;
			
			//Como solo tenemos un resultado, podemos hacer lo siguiente:
			$datos_foro = $resultados -> fetch_assoc();
			$aux1 .= $trozos[0]; 
			$aux_titulo = wordwrap($datos_foro['titulo'], LONGITUD_TITULO_POST, "\n", false);
			$aux1 = str_replace("##titulo##", $datos_foro["titulo"], $aux1);
			$aux1 = str_replace("##creador##", $datos_foro["nick"], $aux1);
			$aux1 = str_replace("##idpost##", $datos_foro["id_post"], $aux1);

			$correo_usuario = $_SESSION['user'];
			if($correo_usuario == $datos_foro["correo_creador"])
					$aux1 = str_replace("##tipo##", 1, $aux1);
				else
					$aux1 = str_replace("##tipo##", 0, $aux1);

			if ($tipo_usuario == 3){
				$aux2.=$trozos[1];
				$aux2 = str_replace("##idpost##", $datos_foro["id_post"], $aux2);
			}
			$trozos[2] = str_replace("##NumComent##", $datos_foro['num_comentarios'], $trozos[2]);
			echo $aux1 . $aux2 . $trozos[2];
		}
		else
			mostrarmensaje("No se encontró la información", "No se ha encontrado la información del post seleccionado",1);
	}

	/** Función que permite mostrar los comentarios asociados a un post determinado **/
	function vmuestracomentarios($comentarios){
		if(!empty($comentarios)){
			$cadena = file_get_contents("comentarios.html");
			$trozos = explode("##fila##", $cadena);

			$aux= "";
			$cuerpo ="";
			$correo_usuario = $_SESSION['user'];
			while ($datos_comentarios = $comentarios->fetch_assoc()){
				$aux = $trozos[1];
				$aux_comentario = wordwrap($datos_comentarios['comentario'], LONGITUD_COMENTARIO_POST, "\n", false);
				$aux = str_replace("##comentario##", $aux_comentario, $aux);
				$aux = str_replace("##escritor##", $datos_comentarios["nick"], $aux);
				if($correo_usuario == $datos_comentarios["correo_escritor"])
					$aux = str_replace("##tipo##", 1, $aux);
				else
					$aux = str_replace("##tipo##", 0, $aux);
				$aux = str_replace("##Fecha##", $datos_comentarios["fecha"], $aux);
				$aux = str_replace("##idcomentario1##", $datos_comentarios["id_post"], $aux);
				$aux = str_replace("##idcomentario2##", $datos_comentarios["fecha"], $aux);
				$aux = str_replace("##idcomentario3##", $datos_comentarios["correo_escritor"], $aux);

				$cuerpo .= $aux;
			}
			$cadena = $trozos[0] . $cuerpo. $trozos[2];

			echo $cadena;
		}
		else
			mostrarmensaje("No hay comentarios para mostrar", "No existen o no se han encontrado comentarios para mostrar",1);
	}

	function vcrearforo($almacenamiento){
		$cadena = file_get_contents("nuevopost.html");
		if(!empty($almacenamiento)){
			$info = $almacenamiento -> fetch_assoc();
			$cadena = str_replace("##Espacio##", $info['tamano'], $cadena);
		}
		else
			$cadena = str_replace("##Espacio##", '?', $cadena);
		echo $cadena;
	}

	function vbuscarforo($resultado, $numeropost) {
		if(isset($_GET['pagina'])){
			$cadena = file_get_contents("foro_parcial.html");
			$trozos = explode('##fila##', $cadena);
			$aux = "";
			$cuerpo = "";
			$correo_usuario = $_SESSION['user'];
			while ($datos = $resultado->fetch_assoc()) {
				$aux = $trozos[1];
				$aux = str_replace("##titulo##", substr($datos["titulo"],0,TAMANO_TITULO).'...', $aux);
				$aux = str_replace("##idpost##", $datos["id_post"], $aux);
				$aux = str_replace("##creador##", $datos["nick"], $aux);
				if($correo_usuario == $datos["correo_creador"])
					$aux = str_replace("##tipo##", 1, $aux);
				else
					$aux = str_replace("##tipo##", 0, $aux);
				$aux = str_replace("##ncomentarios##", $datos["ncomentarios"], $aux);
				$aux = str_replace("##Fecha##", $datos["fecha"], $aux);
				$cuerpo .= $aux;
			}
			$cadena =  $trozos[0] . $cuerpo . $trozos[2];
			$pagina_actual = $_GET['pagina'];
			//Hay que paginar la página actual
			if(!empty($numeropost)){ //Comprobar que el parámetro numusuarios no sea nulo
				$datos = $numeropost->fetch_assoc();
				$numero_elementos = $datos["num_posts"];
				paginar($cadena,$numero_elementos,$pagina_actual);
			}
			else
				mostrarmensaje("No se detectó en número de posts", "No se recibió el número de posts a mostrar",1);
		}
		else
			mostrarmensaje("No se pudo obtener la página actual", "No se encontró el número de la página actual",1);
	}

	/** Función que muestra el resultado de eliminar un post **/
	function veliminarpost($resultado){
		switch ($resultado) {
			case 0:
				mostrarmensaje("Post eliminado correctamente","El post ha sido eliminado de manera correcta",0);
				break;
			case -1:
				mostrarmensaje("Error al borrar el post","El post no se ha podido eliminar",0);
				break;
			case -3:
				mostrarmensaje("Post no encotrado","No se ha podido encontrar el post.",0);
				break;
		}
	}

	/** Función que muestra el resultado de eliminar un comentario del foro **/
	function veliminarcomentario($resultado){
		switch ($resultado) {
			case 0:
				echo "Comentario eliminado correctamente, el comentario ha sido eliminado de manera correcta";
				break;
			case -1:
				mostrarmensaje("Error al borrar el comentario","El comentario no se ha podido eliminar",1);
				break;
			case -3:
				mostrarmensaje("Creador del comentario no encotrado","No se ha podido encontrar a el creador del comentario.",1);
				break;
			case -4:
				mostrarmensaje("Fecha del comentario no encotrada","No se ha podido encontrar la fecha del comentario.",1);
				break;
			case -5:
				mostrarmensaje("Fecha del comentario no encotrada","No se ha podido encontrar el comentario.",1);
				break;
		}
	}

	function vbuscaractores($resultado) {
		$cadena = file_get_contents("add_actores_parcial.html");
		$trozos = explode('##fila##', $cadena);
		$aux = "";
		$cuerpo = "";
		while ($datos = $resultado->fetch_assoc()) {
			$aux = $trozos[1];
			$aux = str_replace("##nombre##", $datos["nombre"], $aux);
			$aux = str_replace("##apellido1##", $datos["apellido1"], $aux);
			$aux = str_replace("##apellido2##", $datos["apellido2"], $aux);
			$aux = str_replace("##id_actor##", $datos["id_actor"], $aux);
			$cuerpo .= $aux;
		}
		echo $trozos[0] . $cuerpo . $trozos[2];
	}

	function vbuscardirectores($resultado) {
		$cadena = file_get_contents("add_directores_parcial.html");
		$trozos = explode('##fila##', $cadena);
		$aux = "";
		$cuerpo = "";
		while ($datos = $resultado->fetch_assoc()) {
			$aux = $trozos[1];
			$aux = str_replace("##nombre##", $datos["nombre"], $aux);
			$aux = str_replace("##apellido1##", $datos["apellido1"], $aux);
			$aux = str_replace("##apellido2##", $datos["apellido2"], $aux);
			$aux = str_replace("##id_director##", $datos["id_director"], $aux);
			$cuerpo .= $aux;
		}
		echo $trozos[0] . $cuerpo . $trozos[2];
	}

	function vnuevoactor(){
		echo file_get_contents("nuevo_actor.html");
	}

	function vnuevodir(){
		echo file_get_contents("nuevo_director.html");
	}

	function vmostrarcrearentrada($almacenamiento){
		$cadena = file_get_contents('crear_entrada.html');
		if(!empty($almacenamiento)){
			$info = $almacenamiento -> fetch_assoc();
			$cadena = str_replace("##Espacio##", $info['tamano'], $cadena);
		}
		else
			$cadena = str_replace("##Espacio##", '?', $cadena);
		echo $cadena;
	}

	function vresultadoañadiractor($valor){
		if ($valor == 1) {
			echo "Actor añadido: Se ha añadido el actor de forma correcta";
		} else {
			echo "Fallo al añadir: No se ha añadido el actor de forma correcta";
		}		
	}

	function vresultadoañadirdirector($valor){
		if ($valor == 1) {
			echo "Director añadido: Se ha añadido el director de forma correcta";
		} else {
			echo "Fallo al añadir: No se ha añadido el director de forma correcta";
		}		
	}

	function vmostrarrescrearentrada($valor){
		if ($valor == 1) {
			mostrarmensaje("Creación correcta", "Se ha creado la entrada correctamente",1);
		} else {
			mostrarmensaje("Creación incorrecta", "No se ha podido crear la entrada correctamente",1);
		}	
	}

	function vcreardescuento($almacenamiento){
		$cadena = file_get_contents("creardescuento.html");
		if(!empty($almacenamiento)){
				$info = $almacenamiento -> fetch_assoc();
				$cadena = str_replace("##Espacio##", $info['tamano'], $cadena);
			}
			else
				$cadena = str_replace("##Espacio##", '?', $cadena);
		echo $cadena;
	}

	function vguardardescuento($resultado){
		switch ($resultado) {
			case 1:
				mostrarmensaje("Exito"," Se creado el descuento correctamente",1);
				break;
			case -1:
				mostrarmensaje("Error al crear descuento"," No se han introducido todos los campos",1);
				break;
			case -4:
				mostrarmensaje("Error al crear descuento"," La cantidad introducida no es valida",1);
				break;
			case -5:
				mostrarmensaje("Error al crear descuento"," Fecha introducida incorrectamente",1);
				break;
		}
	}

	function vexportardescuento($resultado){
		if (is_numeric($resultado)){
			switch ($resultado) {
				case 1:
					mostrarmensaje("Exito"," Se ha exportado el descuento correctamente",1);
					break;
				case -1:
					mostrarmensaje("Error al crear descuento"," No se han introducido todos los campos",1);
					break;
				case -4:
					mostrarmensaje("Error al crear descuento"," La cantidad introducida no es valida",1);
					break;
				case -5:
					mostrarmensaje("Error al crear descuento"," Fecha introducida incorrectamente",1);
					break;
			}
		}else{
			print_r($resultado[1]);
		}
	}

	function vmostrarimportardescuentos(){
		echo file_get_contents('importar_descuento.html');
	}

	function vmostrarresultadoimportar($resultado){
		switch ($resultado) {
			case 1:
				mostrarmensaje('Exito al importar','Se han importado todos los descuentos correctamente',0);
				break;
			case 0:
				mostrarmensaje('Fallo al importar','NO han importado todos los descuentos correctamente',0);
				break;
			case -1:
				mostrarmensaje('Fallo al importar','Extension del archivo incorrecta',0);
				break;
			case -2:
				mostrarmensaje('Fallo al importar','No se ha seleccionado ningun fichero',0);
				break;
		}
	}

	function vsubirimagenes($res){
		if ($res != -1){
			$cadena = file_get_contents("subir_imagenes.html");
			$cadena = str_replace("##identrada##", $res, $cadena);
			echo $cadena;
		}
		else
			echo "No se pudo generar la entrada, revise los campos";
	}
	function vmostrarpaginaentradas($almacenamiento){
		$cadena = file_get_contents("listadoentradas.html");
		if(!empty($almacenamiento)){
			$info = $almacenamiento -> fetch_assoc();
			$cadena = str_replace("##Espacio##", $info['tamano'], $cadena);
		}
		else
			$cadena = str_replace("##Espacio##", '?', $cadena);
		echo $cadena;
	}

	function vmostrarentradas($entradas,$numentradas){
		$cadena = file_get_contents("entradas_admin.html");
		$trozos = explode("##fila##",$cadena);

		$aux = "";
		$cuerpo = "";

		if(isset($_GET["pagina"])){
			$pagina_actual = $_GET["pagina"]; //Obtener la página actual 
			if(!empty($entradas)){ //Comprobar que el parámetro entradas no sea nulo
					while($entrada = $entradas->fetch_assoc()){ //Leer las entradas recuperadas y sustituirlas en el listado iterando
						$aux = $trozos[1];
						$aux = str_replace("##Titulo##", substr($entrada["titulo"],0,TAMANO_TITULO).'...', $aux);
						$aux = str_replace("##Descripcion##", substr($entrada["texto"],0,TAMANO_DESCRIPCION).'...', $aux);
						$aux = str_replace("##enlaceEntrada##", $entrada["id_entrada"], $aux);
						$cuerpo .= $aux;
					}

					$cadena = $trozos[0] . $cuerpo . $trozos[2];

					//Llegado a este punto ya estarían mostradas todas las entradas permitidas.

					//Hay que paginar la página actual
					if(!empty($numentradas)){ //Comprobar que el parámetro numusuarios no sea nulo
						$datos = $numentradas->fetch_assoc();
						$numero_elementos = $datos["num_entradas"];
						paginar($cadena,$numero_elementos,$pagina_actual);
					}
					else //Numero de entradas nulo
						mostrarmensaje("No se detectó en número de entradas", "No se recibió el número de entradas a mostrar",1);
			}
			else //Si el parámetro entradas es nulo:
				mostrarmensaje("No hay entradas para mostrar", "No se encontraron entradas para mostrar, intentelo de nuevo más tarde.",1);
		}
		else
			mostrarmensaje("No se pudo obtener la página actual", "No se encontró el número de la página actual",1);
	}

	function vmostrarreseliminarentrada($valor){
		if ($valor == 1){
			mostrarmensaje("Eliminación correcta",'Se ha eliminado la entrada correctamente',0);
		}else{
			mostrarmensaje("Eliminación fallida",'No se ha podido eliminar la entrada elegida',0);
		}
	}

	function vmostrarentrada1($resultado, $almacenamiento){
		$cadena = file_get_contents("entrada.html");
		if(!empty($almacenamiento)){
			$info = $almacenamiento -> fetch_assoc();
			$cadena = str_replace("##Espacio##", $info['tamano'], $cadena);
		}
		else
			$cadena = str_replace("##Espacio##", '?', $cadena);
		$trozos = explode("##critica##", $cadena);
		$entrada = $resultado[0];
		$actores = $resultado[1];
		$directores = $resultado[2];
		$critica = $resultado[3];
		$correo_sesion = $_SESSION['user'];
		$tipo_usuario = $_SESSION['tipo'];
		$valoracion = $resultado[4];
		$puede_escribir = $resultado[5];
		$imagenes = $resultado[6];
		$aux = "";
		$cuerpo = "";
		$cabecera = "";
		$cola = "";
		$datos = $entrada->fetch_assoc();
		$media = $valoracion->fetch_assoc();
		$aux = $trozos[0];
		$aux_titulo = wordwrap($datos['titulo'], LONGITUD_TITULO, "\n", false);
		$aux = str_replace('##titulo##', $aux_titulo, $aux);
		$aux_texto =  wordwrap($datos['texto'], LONGITUD_ENTRADA, "\n", false);
		$aux = str_replace('##texto##', $aux_texto, $aux);
		$aux = str_replace('##val_media##', $media['media'], $aux);
		$aux = str_replace('##Autor##', $datos['nick_creador'], $aux);
		if($correo_sesion == $datos["correo_creador"])
			$aux = str_replace("##tipo##", 1, $aux);
		else
			$aux = str_replace("##tipo##", 0, $aux);
		$aux = str_replace('##Fecha##', $datos['fecha_creacion'], $aux);
		$aux = str_replace('##AutorModificacion##', $datos['nick_modificacion'], $aux);
		if($correo_sesion == $datos["correo_modificacion"])
			$aux = str_replace("##tipoModificacion##", 1, $aux);
		else
			$aux = str_replace("##tipoModificacion##", 0, $aux);
		$aux = str_replace('##FechaModificacion##', $datos['fecha_modificacion'], $aux);
		$aux = str_replace('##Modificar_entrada##', 'Modificar Entrada', $aux);
		$aux = str_replace('##Entrada##', $datos['id_entrada'], $aux);
		if (isset($datos['trailer'])){
			if (!empty($datos['trailer'])){
				$url_video = $datos['trailer'];
				if(strpos($url_video,"youtube")) //Comprobar si la url es de youtube y modificarla:
					$url_video = str_replace("watch?v=","embed/", $url_video);
				
				$insert_video = "<iframe width='420' height='315' src=".$url_video." allowfullscreen frameborder='0' id='trailer'> </iframe>";
				$aux = str_replace('##trailer##', $insert_video, $aux);
				$aux = str_replace('##TrailerPelicula##', "<hr> Trailer de la película:", $aux);
			}else{
				$aux = str_replace('##trailer##', "", $aux);
				$aux = str_replace('##TrailerPelicula##', "", $aux);
			}
		}else{
			$aux = str_replace('##trailer##', " ", $aux);
		}
		//Obtenemos los actores
		$aux_actores="";
		$it_actores = 1;
		while($datos_actores = $actores->fetch_assoc()){
			if ($it_actores < mysqli_num_rows($actores)){
				if (isset($datos_actores['apellido2'])){
					$aux_actores .= $datos_actores['nombre'] . $datos_actores['apellido1']. $datos_actores['apellido2'] . ', ';
				}else{
					$aux_actores .= $datos_actores['nombre'] . $datos_actores['apellido1'].', ';
				}
			}else{
				if (isset($datos_actores['apellido2'])){
					$aux_actores .= $datos_actores['nombre'] . $datos_actores['apellido1']. $datos_actores['apellido2'];
				}else{
					$aux_actores .= $datos_actores['nombre'] . $datos_actores['apellido1'];
				}
			}
			$it_actores +=1;
		}
		$aux_actores = wordwrap($aux_actores, LONGITUD_REPATO, "\n", false);
		$aux = str_replace('##reparto##', $aux_actores, $aux);

		$aux_directores = "";
		$it_directores = 1;
		while($datos_directores = $directores->fetch_assoc()){
			if ($it_directores < mysqli_num_rows($directores)){
				if (isset($datos_directores['apellido2'])){
					$aux_directores .= $datos_directores['nombre'] . ' ' . $datos_directores['apellido1']. ' '. $datos_directores['apellido2'] . ', ';
				}else{
					$aux_directores .= $datos_directores['nombre'] . ' ' . $datos_directores['apellido1']. ', ';
				}
			}else{
				if (isset($datos_directores['apellido2'])){
					$aux_directores .= $datos_directores['nombre'] . ' ' . $datos_directores['apellido1']. ' '. $datos_directores['apellido2'];
				}else{
					$aux_directores .= $datos_directores['nombre'] . ' ' . $datos_directores['apellido1'];
				}	
			}
			$it_directores += 1;
		}

		$aux_directores = wordwrap($aux_directores, LONGITUD_DIRECCION, "\n", false);
		$aux = str_replace('##direccion##', $aux_directores, $aux);

		$it = 1;
		$num_imagenes = mysqli_num_rows($imagenes);
		switch ($num_imagenes) {
			case 1:
				$aux = str_replace('##imagen2##', " ", $aux);
				$aux = str_replace('##imagen3##', " ", $aux);
				break;
			case 2:
				$aux = str_replace('##imagen3##', " ", $aux);
				break;
			case 0:
				$aux = str_replace('##imagen1##', "<h2> No hay imágenes disponibles por el momento </h2>", $aux);
				$aux = str_replace('##imagen2##', " ", $aux);
				$aux = str_replace('##imagen3##', " ", $aux);
				break;
			default:
				break;
		}
		while ($datos_imagenes = $imagenes->fetch_assoc()){
			$imagen = $datos_imagenes['imagen'];
			if ($it == 1){
				$sus_imagen = '<img id="'.$datos_imagenes['id_imagen'].'" heigh="300" width="300" src="data:image;base64,'.$imagen.' " onclick="setPrincipal(this.id)" style="border: 2px; border-style: solid">';
			}else{
				$sus_imagen = '<img id="'.$datos_imagenes['id_imagen'].'" heigh="100" width="100" src="data:image;base64,'.$imagen.' " onclick="setPrincipal(this.id)" style="border: 2px; border-style: solid">';
			}
			$aux = str_replace('##imagen'.$it.'##', $sus_imagen, $aux);
			$it += 1;
		}


		$cabecera.=$aux;
		echo $cabecera . $trozos[2];
	}


	/** Función que permite mostrar todas las criticas asociadas a una entrada **/
	function vmostrarcriticas($criticas, $numcriticas){
		$cadena = file_get_contents("criticas.html");
		$trozos = explode("##fila##",$cadena);

		$aux = "";
		$cuerpo = "";

		if(isset($_GET["pagina"])){
			$pagina_actual = $_GET["pagina"]; //Obtener la página actual 
			if(!empty($criticas)){ //Comprobar que el parámetro entradas no sea nulo
				if(isset($_SESSION['user']))
					$correo_sesion = $_SESSION['user'];
				else
					$correo_sesion = "anonimo";
				if(isset($_SESSION['tipo']))
					$tipo_usuario = $_SESSION['tipo'];
				else
					$tipo_usuario = -1;

				while($datos_critica = $criticas->fetch_assoc()){ //Leer las entradas recuperadas y sustituirlas en el listado iterando
					$aux = $trozos[1];
					$aux = str_replace("##modificar##", "Modificar critica", $aux);
					$aux = str_replace("##eliminarcritica##", "Eliminar critica", $aux);
					$aux = str_replace("##critica##", substr($datos_critica["critica"],0,TAMANO_CRITICA).'...', $aux);
					$aux = str_replace("##identrada##", $_GET["entrada"], $aux);
					$aux = str_replace("##escritor##", $datos_critica["nick"], $aux);
					if($correo_sesion == $datos_critica['correo_creador'])
						$aux = str_replace("##tipo##", 1, $aux);
					else
						$aux = str_replace("##tipo##", 0, $aux);
					$aux = str_replace("##Fecha_critica##", $datos_critica["fecha"], $aux);
					$aux = str_replace("##valoracion##", $datos_critica["valoracion"], $aux);
					$aux = str_replace("##correo##", $datos_critica["correo_creador"], $aux);
					$cuerpo .= $aux;
				}

				$cadena = $trozos[0] . $cuerpo . $trozos[2];

				//Llegado a este punto ya estarían mostradas todas las críticas permitidas.

				//Hay que paginar la página actual
				if(!empty($numcriticas)){ //Comprobar que el parámetro numusuarios no sea nulo
					$datos = $numcriticas->fetch_assoc();
					$numero_elementos = $datos["num_criticas"];
					paginar($cadena,$numero_elementos,$pagina_actual);
				}
				else //Numero de críticas nulo
					mostrarmensaje("No se detectó en número de críticas", "No se recibió el número de críticas a mostrar",1);
			}
			else //Si el parámetro críticas es nulo:
				mostrarmensaje("No hay críticas para mostrar", "No se encontraron críticas para mostrar, intentelo de nuevo más tarde.",1);
		}
		else
			mostrarmensaje("No se pudo obtener la página actual", "No se encontró el número de la página actual",1);
	}

	function vactualizarimagenes($resultado){
		$principal = $resultado[0];
		$secundarias = $resultado[1];
		$datos_principal = $principal->fetch_assoc();
		$imagen_principal = '<img id="'.$datos_principal['id_imagen'].'" heigh="300" width="300" src="data:image;base64,'.$datos_principal['imagen'].' " onclick="setPrincipal(this.id)" style="border: 2px; border-style: solid"><br>';


		$imagenes_secundarias = "";
		while ($datos_secundarias = $secundarias->fetch_assoc()){
			$sus_imagen = '<img id="'.$datos_secundarias['id_imagen'].'" heigh="100" width="100" src="data:image;base64,'.$datos_secundarias['imagen'].' " onclick="setPrincipal(this.id)" style="border: 2px; border-style: solid"> ';
			$imagenes_secundarias .= $sus_imagen;
		}

		echo $imagen_principal . $imagenes_secundarias . '<br>';
	}

	function vmodificarentrada($datos,$almacenamiento){
		$cadena = file_get_contents("modificar_entrada.html");
		if(!empty($almacenamiento)){
			$info = $almacenamiento -> fetch_assoc();
			$cadena = str_replace("##Espacio##", $info['tamano'], $cadena);
		}
		else
			$cadena = str_replace("##Espacio##", '?', $cadena);
		if(!empty($datos)){ //Comprobar que los datos de la entrada no son nulos:
			$datos_entrada = $datos[0] -> fetch_assoc();
			$cadena = str_replace("##Titulo##", $datos_entrada['titulo'], $cadena);
			$cadena = str_replace("##Contenido##", $datos_entrada['texto'], $cadena);
			$cadena = str_replace("##Entrada##", $_POST['entrada'], $cadena);
			if(!empty($datos_entrada['trailer']) && strlen($datos_entrada['trailer']) > 0)
				$cadena = str_replace("##Url##", $datos_entrada['trailer'], $cadena);
			else
				$cadena = str_replace("##Url##", '', $cadena);
			echo $cadena;
		}
		else
			mostrarmensaje("No se pudo modificar la entrada", "No se pudo obtener la información de la entrada seleccionada.",0);
	}

	function vcheckmodificacion($valor){
		if($valor == 0)
			mostrarmensaje("Exito al modificar", "Se ha modificado la entrada correctamente",0);
		elseif ($valor == -1) {
			mostrarmensaje("No se pudo borrar la solicitud", "Hubo un error al borrar la solicitud",1);
		}
		elseif ($valor == -2) {
			mostrarmensaje("No se pudo enviar la modificación", "Hubo un error al enviar la modificación intento mas tarde.",1);
		}
		elseif ($valor == -3) {
			mostrarmensaje("El texto no cumple la longitud", "El texto debe tener un mínimo ".MIN_CONTENIDO." carácteres",1);
		}
		elseif ($valor == -4) {
			mostrarmensaje("El titulo no cumple la longitud", "La longitud del titulo debe estar entre ".MIN_TITULO." y ".MAX_TITULO." carácteres",1);
		}
		elseif ($valor == -6) {
			mostrarmensaje("El contenido es vacio", "El contenido de su modificación esta vacio, reviselo",1);
		}
		elseif ($valor == -7) {
			mostrarmensaje("El titulo es vacio", "El titulo de su modificación esta vacio, reviselo",1);
		}
		
	}

	function veditarcritica($resultado,$almacenamiento){
		if(!empty($resultado)){
			$cadena = file_get_contents("editarcritica.html");
			if(!empty($almacenamiento)){
				$info = $almacenamiento -> fetch_assoc();
				$cadena = str_replace("##Espacio##", $info['tamano'], $cadena);
			}
			else
				$cadena = str_replace("##Espacio##", '?', $cadena);

			$datos = $resultado->fetch_assoc();
			$cadena = str_replace("##titulo##", $datos["titulo"], $cadena);
			$cadena = str_replace("##critica##", $datos["critica"], $cadena);
			$cadena = str_replace("##identrada##", $datos["id_entrada"], $cadena);
			$cadena = str_replace("##Correo##", $datos["correo_creador"], $cadena);
			$cadena = str_replace("##Fecha##", $datos["fecha"], $cadena);
			$cadena = str_replace("##Creador##", $datos["nick"], $cadena);
			$correo_sesion = $_SESSION['user'];
			if($correo_sesion == $datos["correo_creador"])
				$cadena = str_replace("##tipo##", 1, $cadena);
			else
				$cadena = str_replace("##tipo##", 0, $cadena);
			$valoracion = $datos["valoracion"];
			$trozos = explode("##Puntuacion##", $cadena);
			$aux = "";
			$cuerpo="";
			for ($i = 1; $i <= PUNTUACION_MAXIMA; $i++){
				$aux = $trozos[1];
				$aux = str_replace("##Valor##", $i, $aux);
				if($i == $valoracion)
					$aux = str_replace("##Seleccionada##", "selected", $aux);
				else
					$aux = str_replace("##Seleccionada##", "", $aux);
				$cuerpo .= $aux;
			}
			
			echo $trozos[0]. $cuerpo . $trozos[2];
		}
		else
			mostrarmensaje("No se pudo obtener la crítica","No se pudo obtener la crítica seleccionada, intentelo más tarde",0);
	}

	function vmodificarcritica($valor){
		switch ($valor) {
			case 0:
				mostrarmensaje("Crítica modificada correctamente", "Se ha modificado la crítica correctamente",0);
				break;
			case -1:
				mostrarmensaje("Error al modificar la crítica", "No se ha recibido el texto a modificar",1);
				break;
			case -2:
				mostrarmensaje("Error al modificar la crítica", "No se ha detectado la entrada a la que pertenece la crítica",1);
				break;
			case -3:
				mostrarmensaje("Error al modificar la crítica", "No se ha recibido la valoración de la crítica",1);
				break;
			case -4:
				mostrarmensaje("Error al modificar la crítica", "No se ha podido almacenar la modificación de la crítica",1);
				break;
			case -5:
				mostrarmensaje("Error al modificar la crítica", "No se ha podido encontrar el correo del escritor de la crítica",1);
				break;
			case -6:
				mostrarmensaje("Error al modificar la crítica", "No cumple con la longitud establecida: minímo ".MIN_CRITICA."<br> y máximo ".MAX_CRITICA." carácteres",1);
				break;
		}
	}

	function vmuestracritica($consulta, $almacenamiento){
		if(!empty($consulta)){
			if(isset($_POST['entrada'])){
				$critica = $consulta ->fetch_assoc();

				$cadena = file_get_contents('vercritica.html');
				if(!empty($almacenamiento)){
					$info = $almacenamiento -> fetch_assoc();
					$cadena = str_replace("##Espacio##", $info['tamano'], $cadena);
				}
				else
					$cadena = str_replace("##Espacio##", '?', $cadena);

				$aux_critica = wordwrap($critica['critica'], LONGITUD_CRITICA, "\n", false);
				$cadena = str_replace('##Critica##', $critica['critica'], $cadena);
				$cadena = str_replace('##Autor##',$critica['nick'], $cadena);
				$correo_sesion = $_SESSION['user'];
				if($correo_sesion == $critica['correo_electronico'])
						$cadena = str_replace("##tipo##", 1, $cadena);
				else
						$cadena = str_replace("##tipo##", 0, $cadena);
				$cadena = str_replace('##Fecha##', $critica['fecha'], $cadena);
				$cadena = str_replace('##valoracion##', $critica['valoracion'], $cadena);
				$cadena = str_replace('##Entrada##', $_POST['entrada'], $cadena);

				echo $cadena;
			}
			else
				mostrarmensaje("No se ha encontrado la entrada", "No se pudo obtener la entrada que contiene la crítica seleccionada",0);
		}
		else
			mostrarmensaje("No se ha encontrado la crítica", "No se pudo obtener la información de la crítica seleccionada",0);
	}

	/** Función que muestra el resultado de eliminar una critica **/
	function veliminarcritica($valor){
		switch ($valor) {
			case 0:
				mostrarmensaje("Crítica eliminada correctamente", "Se ha eliminado la crítica correctamente",0);
				break;
			case -3:
				mostrarmensaje("Error al eliminar la crítica", "No se ha podido encontrar el correo del escritor de la crítica",1);
				break;
			case -5:
				mostrarmensaje("Error al eliminar la crítica", "No se ha detectado la entrada a la que pertenece la crítica",1);
				break;
			case -4:
				mostrarmensaje("Error al eliminar la crítica", "No se ha podido elimiar la crítica",1);
				break;
		}
	}

	function vcambiodatosperfil($resultado){
		switch($resultado){
			case 0:
				echo("Datos personales modificados correctamente");
				break;
			case 1:
				echo("No ha modificado ningún dato personal");
				break;
			case -1:
				mostrarmensaje("Error al modificar sus datos","El nombre solo puede tener letras y números",1);
				break;
			case -2:
				mostrarmensaje("Error al modificar sus datos", "La longitud del nombre debe ser de al menos ".MIN_NOMBRE." y como mucho ".MAX_NOMBRE." carácteres",1);
				break;
			case -3:
				mostrarmensaje("Fallo al modificar sus datos", "El primer apellido solo puede contener letras mayúsculas y minúsculas",1);
				break;
			case -4:
				mostrarmensaje("Fallo al modificar sus datos", "El primer apellido debe tener al menos ".MIN_APELLIDO." y como maximo ".MAX_APELLIDO." caracteres",1);
				break;
			case -5:
				mostrarmensaje("Fallo al modificar sus datos", "El segundo apellido solo puede contener letras mayúsculas y minúsculas",1);
				break;
			case -6:
				mostrarmensaje("Fallo al modificar sus datos", "El segundo apellido debe tener al menos ".MIN_APELLIDO." y como maximo ".MAX_APELLIDO." caracteres",1);
				break;
			case -7:
				mostrarmensaje("Fallo al modificar sus datos", "El nick seleccionado ya está en uso",1);
				break;
			case -8:
				mostrarmensaje("Fallo al modificar sus datos", "No se pudo comprobar el nick seleccionado, intentelo más tarde.",1);
				break;
			case -9:
				mostrarmensaje("Fallo al modificar sus datos", "El nick solo puede contener letras y números",1);
				break;
			case -10:
				mostrarmensaje("Fallo al modificar sus datos", "La longitud del nick debe estar entre ".MIN_NICK." y ".MAX_NICK." caracteres",1);
				break;
			case -11:
				mostrarmensaje("Fallo al modificar sus datos", "La dirección debe tener al menos ".MIN_DIRECCION." y como máximo ".MAX_DIRECCION." carácteres",1);
				break;
			case -15:
				mostrarmensaje("Fallo al modificar sus datos", "No se han podido guardar los cambios, intentelo de nuevo más tarde.",1);
				break;
			case -13:
				mostrarmensaje("Fallo al modificar sus datos", "Debe seleccionar un municipio si desea cambiar de provincia.",1);
				break;
			case -33:
				mostrarmensaje("Fallo al modificar sus datos", "No se ha encontrado la provincia seleccionada.",1);
				break;
			case -34:
				mostrarmensaje("Fallo al modificar sus datos", "No se ha podido comprobar la provincia seleccionada.",1);
				break;
			case -43:
				mostrarmensaje("Fallo al modificar sus datos", "No se ha encontrado el municipio seleccionado.",1);
				break;
			case -44:
				mostrarmensaje("Fallo al modificar sus datos", "No se ha podido comprobar el municipio seleccionado.",1);
				break;
		}
	}

	/** Función que permite mostrar las provincias obtenidas de la BBDD **/
	function vmostrarprovincias($provincias){
		if(!empty($provincias)){ //Comprobar que las pronvicias no sean vacias:
			$cadena = file_get_contents('provincia.html');

			$trozos = explode('##fila##', $cadena);

			$aux = "";
			$cuerpo = "";
			while ($provincia = $provincias->fetch_assoc()) {
				$aux = $trozos[1];
				$aux = str_replace('##idprovincia##', $provincia['id_provincia'], $aux);
				$aux = str_replace('##provincia##',  $provincia['provincia'], $aux);

				$cuerpo .= $aux;
			}
			$cadena = $trozos[0] . $cuerpo . $trozos[2];

			echo $cadena;
		}	
		else //Provincias no detectadas.
			mostrarmensaje("No se han podido obtener las provincias","No se ha podido obtener el listado con las provincias",1);
	}

	/** Funcion que permite mostrar los municipios recogidos de la BBDD de acuerdo a un id de provincia seleccionado por el usuario **/
	function vmostrarmunicipios($municipios){
		if(!empty($municipios)){ //Comprobar que las pronvicias no sean vacias:
			$cadena = file_get_contents('municipio.html');

			$trozos = explode('##fila##', $cadena);

			$aux = "";
			$cuerpo = "";
			while ($municipio = $municipios->fetch_assoc()) {
				$aux = $trozos[1];
				$aux = str_replace('##idmunicipio##', $municipio['id_municipio'], $aux);
				$aux = str_replace('##municipio##',  $municipio['municipio'], $aux);

				$cuerpo .= $aux;
			}
			$cadena = $trozos[0] . $cuerpo . $trozos[2];

			echo $cadena;
		}	
		else //Municipios no detectados.
			mostrarmensaje("No se han podido obtener los municipios","No se ha podido obtener el listado con los municipios para la provincia seleccionada",1);

	}

	/** Funcion que permite mostrar el perfil de un usuario ajeno **/
	function vmostrarperfilajeno($usuario, $almacenamiento){
		if(!empty($usuario)){
			$cadena = file_get_contents("perfil_ajeno.html");
			if(!empty($almacenamiento)){
				$info = $almacenamiento -> fetch_assoc();
				$cadena = str_replace("##Espacio##", $info['tamano'], $cadena);
			}
			else
				$cadena = str_replace("##Espacio##", '?', $cadena);

			$datos_usuario = $usuario -> fetch_assoc();
			$cadena = str_replace("##Nick##", $datos_usuario['nick'], $cadena);
			$cadena = str_replace("##nombre##", $datos_usuario['nombre'], $cadena);
			$cadena = str_replace("##apellido1##", $datos_usuario['apellido1'], $cadena);
			$cadena = str_replace("##apellido2##", $datos_usuario['apellido2'], $cadena);
			$cadena = str_replace("##imagen##", base64_encode($datos_usuario['imagen_perfil']), $cadena);
			if($datos_usuario['Tipo'] == 2 || $datos_usuario['Tipo'] == 3)
				$cadena = str_replace("##verificado##", 'Si', $cadena);
			elseif($datos_usuario['Tipo'] == 1)
				$cadena = str_replace("##verificado##", 'Pendiente de verificación', $cadena);
			elseif($datos_usuario['Tipo'] == 0)
				$cadena = str_replace("##verificado##", 'No', $cadena);
			$cadena = str_replace("##Correo##", $datos_usuario['correo_electronico'], $cadena);
			$cadena = str_replace("##Provincia##", $datos_usuario['provincia'], $cadena);
			$cadena = str_replace("##Municipio##", $datos_usuario['municipio'], $cadena);
			$cadena = str_replace("##Direccion##", $datos_usuario['direccion'], $cadena);
			if($datos_usuario['bloqueado'] == 1){
				$cadena = str_replace("##Bloqueado##",'Si', $cadena);
				$cadena = str_replace("##Accion##",'Desbloquear', $cadena);
				$cadena = str_replace("##id##",'4', $cadena);
			}
			else{
				$cadena = str_replace("##Bloqueado##",'No', $cadena);
				$cadena = str_replace("##Accion##",'Bloquear', $cadena);
				$cadena = str_replace("##id##",'3', $cadena);
			}
			echo $cadena;
		}
		else
			mostrarmensaje("No se ha encontrado al usuario", "No se ha encontrado al usuario seleccionado",0);
	}

	/** Función que muestra el resultado de escribir un comentario en un post del foro **/
	function vescribircomentario($resultado){
		switch($resultado){
			case 0:
				echo("Comentario añadido correctamente");
				break;
			case -1:
				echo("No se ha podido añadir el comentario, intentlo más tarde");
				break;
			case -2:
				echo("El comentario no se ha podido añadir no se ha recibido el id del post, no cumple la longitud establecida (mínimo ".MIN_COMENTARIO." y máximo ".MAX_COMENTARIO." carácteres)");
				break;
			case -3:
				echo("El comentario no se ha podido añadir no se ha recibido el id del post.");
				break;
			case -4:
				echo("El comentario no se ha podido añadir no se ha recibido el id del post, el comentario está vacio");
				break;
		}
	}

	/** Función que muestra el resultado de eliminar una entrada temporal (en proceso de creación) **/
	function velminarentradaalmacenada($resultado){
		switch ($resultado) {
			case 0:
				echo "La entrada ha sido descartada correctamente";
				break;
			case -1:
				echo "La entrada no ha sido descartada correctamente, no se ha detectado el identificador de la misma";
				break;
			case -2:
				echo "La entrada no ha sido descartada correctamente";
				break;
		}
	}

	/**Función que permite mostrar la pantalla de descuentos sobre la que se colocarán los descuentos disponibles **/
	function vmostrarlistadodescuentos($almacenamiento){
		$cadena =  file_get_contents("listadescuentos.html");
		if(!empty($almacenamiento)){
			$info = $almacenamiento -> fetch_assoc();
			$cadena = str_replace("##Espacio##", $info['tamano'], $cadena);
		}
		else
			$cadena = str_replace("##Espacio##", '?', $cadena);
		echo $cadena;
	}

	/** Función que permite mostrar los descuentos disponibles en la página web **/
	function vmostrardescuentos($descuentos,$numdescuentos){
		$cadena = file_get_contents("descuentos.html");
		$trozos = explode("##fila##",$cadena);

		$aux = "";
		$cuerpo = "";

		if(isset($_GET["pagina"])){
			$pagina_actual = $_GET["pagina"]; //Obtener la página actual 
			if(!empty($descuentos)){ //Comprobar que el parámetro entradas no sea nulo
					while($descuento = $descuentos->fetch_assoc()){ //Leer las entradas recuperadas y sustituirlas en el listado iterando
						$aux = $trozos[1];
						$aux = str_replace("##pelicula##", $descuento["pelicula"], $aux);
						$aux = str_replace("##cantidad##", $descuento["cantidad"], $aux);
						$aux = str_replace("##cine##", $descuento["cine"], $aux);
						$aux = str_replace("##dia##", $descuento["dia"], $aux);
						$aux = str_replace("##mes##", $descuento["mes"], $aux);
						$aux = str_replace("##ano##", $descuento["ano"], $aux);
						$aux = str_replace("##id_descuento##", $descuento["id_descuento"], $aux);
						$cuerpo .= $aux;
					}

					$cadena = $trozos[0] . $cuerpo . $trozos[2];
					$letra_cine = "";
					if($_GET['letra_cine'])
						$letra_cine = $_GET['letra_cine'];
					$letra_pelicula = "";
					if($_GET['letra_pelicula'])
						$letra_pelicula = $_GET['letra_pelicula'];
					//Al tener dos busquedas hacer aqui la sustitución de las letras:
					$cadena = str_replace("##letra_pelicula##", $letra_pelicula, $cadena);
					$cadena = str_replace("##letra_cine##", $letra_cine, $cadena);

					//Hay que paginar la página actual
					if(!empty($numdescuentos)){ //Comprobar que el parámetro numdescuentos no sea nulo
						$datos = $numdescuentos->fetch_assoc();
						$numero_elementos = $datos["num_descuentos"];
						paginar($cadena,$numero_elementos,$pagina_actual);
					}
					else //Numero de entradas nulo
						mostrarmensaje("No se detectó en número de descuentos", "No se recibió el número de descuentos a mostrar",1);
			}
			else //Si el parámetro entradas es nulo:
				mostrarmensaje("No hay descuentos disponibles", "Actualmente no hay ningún descuento disponible.",1);
		}
		else
			mostrarmensaje("No se pudo obtener la página actual", "No se encontró el número de la página actual",1);
	}

	/** Función que muestra el resultado de eliminar un descuento **/
	function veliminardescuento($resultado){
		switch ($resultado) {
			case 0:
				echo "El descuento ha sido eliminado correctamente";
				break;
			case -1:
				echo "El descuento no ha sido eliminado, intentelo más tarde";
				break;
			case -2:
				echo "El descuento no ha sido eliminado, no se ha detectado el id del mismo";
				break;
		}
	}
?>