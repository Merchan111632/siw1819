<?php 
	session_start(); //Iniciar sesion.

	include "modelo.php";
	include "vista.php";

	//Comprobamos en primer lugar cómo se introducen los datos, si por URL o mediante formulario.
	if (isset($_GET['accion'])) {
		$accion = $_GET['accion'];
		if(isset($_GET['id']))
			$id = $_GET['id'];
		elseif(isset($_POST['id']))
			$id = $_POST['id'];
		else
			$id = 1;
	} else {
		if (isset($_POST['accion'])) {
			$accion = $_POST['accion'];
			if(isset($_GET['id']))
				$id = $_GET['id'];
			elseif(isset($_POST['id']))
				$id = $_POST['id'];
			else
				$id = 1;
		} else {
			//Generamos una opción por defecto para la primera visita a la página
			$accion = 'menu';
			$id = 1;
		}
	}

	$result = mcomprobaradmin(); //Comprobar si hay un admin conectado o no y su estado.
	if($result == -4){ //Si no hay un usuario conectado:
		if ($accion == "login"){
			switch ($id) {
				case 1:
					vloginadmin();
					break;
				case 2:
					vmostrarloginadmin(mvalidarloginadmin());
					break;
				default:
					vpaginanoencontrada(1);
					break;
			}
		}
		elseif ($accion == "menu") {
			switch ($id) {
				case 1: //Mostrar el menu del admin (login)
					vloginadmin();
					break;
				default:
					vpaginanoencontrada(1);
					break;
			}
		}
		else{
			vpaginanoencontrada(1);
		}
	}
	elseif ($result == 0) { //Si hay un admin conectado
		//La opción por defecto va a ser mostrar la página principal
		if ($accion == "menu") {
			switch ($id) {
				case 1: //Mostrar el menu de opciones del administrador conectado.
					vmostraropcionesadmin(mestadisticas(),malmacenamiento());
					break;
				default:
					vpaginanoencontrada(malmacenamiento());
					break;
			}
		}
		elseif ($accion == "sesion"){
			switch ($id) {
				case 1:
					vmostrarsesion(mcierrasesion());
					break;
				default:
					vpaginanoencontrada(malmacenamiento());
					break;
			}
		}
		elseif ($accion == "bloquear"){
			switch ($id) {
				case 1: //El administrador desea bloquear a un usuario:
					vlistadousuarios(malmacenamiento());
					break;
				case 2: //Mostrar la lista de usuarios actuales de la página web:
					vmostrarusuarios(mrecuperausuarios(),numusuarios());
					break;
				case 3:
					vresultadobloqueo(mbloquearusuario());
					break;
				case 4:
					vresultadodesbloqueo(mdesbloquearusuario());
					break;
				default:
					vpaginanoencontrada(malmacenamiento());
					break;
			}
		}
		elseif ($accion == "verificar"){
			switch ($id) {
				case 1: //El administrador desea ver la lista de verificaciones de los usuarios:
					vlistaverificaciones(malmacenamiento());
					break;
				case 2: //Mostrar la lista de verificaciones de usuarios de la página web:
					vmostrarverificaciones(mrecuperaverificaciones(),numverificaciones());
					break;
				case 3: //Mostrar la verificación seleccionada
					vmostrarsolicitud(mobtenersolicitud(), malmacenamiento());
					break;
				case 4: //Aceptar verificación
					vresultadoverificar(mverficarusuario());
					break;
				case 5: //Rechazar verificación
					vresultadorechazar(mrechazarsolicitud());
					break;
				default:
					vpaginanoencontrada(malmacenamiento());
					break;
			}
		}
		elseif ($accion == "modificar"){
			switch ($id) {
				case 1: //El administrador desea ver la lista de modificaciones que hay
					vlistadomodificaciones(malmacenamiento());
					break;
				case 2: //Mostrar la lista de solicitudes de modificación de usuarios de la página web:
					vmostrarmodificaciones(mrecuperamodificaciones(),nummodificaciones());
					break;
				case 3: //Mostrar la solicitud de modificación seleccionada
					vmostrarmodificacion(mobtenermodificacion(),mobteneroriginal(),malmacenamiento());
					break;
				case 4: //Admin acepta la modificación y la envía:
					vaceptarmodificacion(mcheckmodificacion());
					break;
				case 5: //Admin rechaza la modificación y la elimina
					vrechazarmodificacion(meliminarmodificacion());
					break;
				default:
					vpaginanoencontrada(malmacenamiento());
					break;
			}
		}
		elseif ($accion == "perfil") {
			switch ($id) {
				case 1: //Mostrar pagina de perfil del usuario
					vmostrarperfil(mrecuperaimagen(),mrecuperaprovincia(),mrecuperamunicipio(),malmacenamiento());
					break;
				case 2: //Cambio de contraseña:
					vcambiopassword(mcambiopassword());
					break;
				case 3: //Cambio imagen
					vcambioimagen(mcambioimagen());
					break;
				case 4: //Cambio datos personales:
					vcambiodatosperfil(mcambiodatosperfil());
					break;
				case 5:
					//Obtener las provincias:
					vmostrarprovincias(mobtenerprovincias());
					break;
				case 6:
					//Obtener los municipios de acuerdo a la provincia seleccionada
					vmostrarmunicipios(mobtenermunicipios());
					break;
				case 7: //Ver el perfil de otro usuario
					vmostrarperfilajeno(mobtenerperfilajeno(),malmacenamiento());
					break;
				default:
					vpaginanoencontrada(malmacenamiento());
					break;
			}
		}
		elseif ($accion == "ver_foros"){
			switch ($id) {
				case 1:
					#Vemos una lista con una informacion breve sobre todos los foros
					vmostrarforos(mlistapost(),malmacenamiento());
					break;
				case 2:
					#Mostramos la informacion completa del foro, con titulos y comentarios
					vmostrarforo(mforoelegido(),malmacenamiento());
					break;
				case 3:
					#Funcion que gestiona la escritura de un nuevo comentario
					vescribircomentario(mescribircomentario());
					break;
				case 4:
					#Funcion que gestiona la creacion de un nuevo foro
					vcrearforo(malmacenamiento());
					break;
				case 5:
					#Mostramos de nuevo la lista con el nuevo foro
					vmostrarforos(mnuevoforo(),malmacenamiento());
					break;
				case 6:
					#Funcion Ajax que gestiona la busqueda de foros por titulo
					vbuscarforo(mbuscarforo(),mnumeropost());
					break;
				case 7:
					#Eliminar un post:
					veliminarpost(meliminarpost());
					break;
				case 8:
					#Eliminar un comentario de un post:
					veliminarcomentario(meliminarcomentario());
					break;
				case 9:
					#Recuperar los comentarios asociados a un post:
					vmuestracomentarios(mrecuperacomentarios());
					break;
				default:
					vpaginanoencontrada(malmacenamiento());
					break;
			}
		}
		elseif ($accion == "tiempo") {
			switch ($id) {
				case 1:
					mactualizatiempo();
					break;
				default:
					vpaginanoencontrada(malmacenamiento());
					break;
			}
		}
		elseif ($accion == 'crearentrada'){
			switch ($id) {
				case 1:
					vmostrarcrearentrada(malmacenamiento());
					break;
				case 2:
					vsubirimagenes(mguardarentrada());
					break;
				case 3:
					velminarentradaalmacenada(meliminarentradaalmacenada());
					break;
				default:
					vpaginanoencontrada(malmacenamiento());
					break;
			}
		}
		elseif ($accion == "listado") {
			switch ($id) {
				case 1: //Mostrar pagina de entradas
					vmostrarpaginaentradas(malmacenamiento());
					break;
				case 2: //Actualizar las entradas y mostrarlas
					vmostrarentradas(mrecuperaentradas(),mnumentradas());
					break;
				case 3: //Ver una entrada
					vmostrarentrada1(mselecentrada(),malmacenamiento());
					break;
				case 4:
					vmodificarentrada(mrecuperaentrada(),malmacenamiento());
					break;
				case 5:
					if(!empty(mrecuperaentrada())){
						vcheckmodificacion(mcheckmodificacion());
					}
					else{
						mostrarmensaje("No se pudo modificar la entrada", "No se pudo obtener la información de la entrada seleccionada.",0);
					}
					break;
				case 6: //Obtener el listado de criticas:
					vmostrarcriticas(mrecuperacriticas(),mnumerocriticas());
					break;
				case 7:
					vmostrarreseliminarentrada(meliminarentrada());
					break;
				default:
					vpaginanoencontrada(malmacenamiento());
					break;
			}
		}
		elseif ($accion == "escribircritica"){
			switch ($id) {
				case 1:
					#Funcion que gestiona la insercion de una critica en la BBDD
					mescribircritica();
					break;
				case 2:
					#Funcion muestra una critica que puede ser modificada
					veditarcritica(meditarcritica(),malmacenamiento());
					break;
				case 3:
					#Funcion que gestiona la modificacion de una critica ya realizada
					vmodificarcritica(mmodificarcritica());
					break;
				case 4:
					#Funcion que se encarga de la eliminacion de las criticas presentes
					veliminarcritica(meliminarcritica());
					break;
				case 5:
				#Función que muestra la crítica saleccionada por el usuario.
					vmuestracritica(mrecuperacritica(),malmacenamiento());
					break;
				default:
					vpaginanoencontrada(malmacenamiento());
					break;
			}
		}
		elseif ($accion == "actores") {
			switch ($id) {
				case 1: 
					vbuscaractores(mbuscaractores());
					break;
				case 2:
					vresultadoañadiractor(mnuevoactor());
					break;
				default:
					vpaginanoencontrada(malmacenamiento());
					break;
			}
		}
		elseif ($accion == "directores") {
			switch ($id) {
				case 1: 
					vbuscardirectores(mbuscardirectores());
					break;
				case 2:
					vresultadoañadirdirector(mnuevodirector());
					break;
				default:
					vpaginanoencontrada(malmacenamiento());
					break;
			}
		}
		elseif ($accion == "descuentos"){
			switch ($id) {
				case 1:
					vcreardescuento(malmacenamiento());
					break;
				case 2:
					vguardardescuento(mguardardescuento());
					break;
				case 3:
					vexportardescuento(mexportardescuento());
					break;
				case 4:
					vmostrarimportardescuentos();
					break;
				case 5:
					vmostrarresultadoimportar(mmostrarresultadoimportar());
					break;
				default:
					vpaginanoencontrada(malmacenamiento());
					break;
			}
		}
		elseif ($accion == "eliminar_descuentos"){
				switch ($id) {
					case 1:
						vmostrarlistadodescuentos(malmacenamiento());
						break;
					case 2:
						veliminardescuento(meliminardescuento());
						break;
					case 3:
						vmostrardescuentos(mrecuperadescuentos(), mnumerodescuentos());
						break;
					default:
						vpaginanoencontrada(malmacenamiento());
						break;
			}
		}
	    elseif($accion=="guardar_imagenes"){
			switch ($id) {
				case 1:
					vsubirimagenes();
					break;
				case 2:
					mguardarimagen();
					break;
				default:
					vpaginanoencontrada(malmacenamiento());
					break;
			}
		}
		elseif ($accion == "imagenes"){
			if ($id == 1){
				vactualizarimagenes(mactualizarimagenes());
			}
			else
				vpaginanoencontrada(malmacenamiento());
		}
		else{
			vpaginanoencontrada(malmacenamiento());
		}
	}
	else
		vmostrarsesion($result);
 ?>