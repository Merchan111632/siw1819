<?php 
	//Incluir el fichero con constantes:
	include_once("constantes.php");
	//Añadir la libreria para los Captcha
	require_once "recaptchalib.php";

	/** Funcion que realiza la conexión con la base de datos **/
	function conexion() {
		mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT); //Permitir que mysli lance excepciones.
		try{
		    //$con = mysqli_connect("localhost", "root", "","db_grupo27");
			$con = mysqli_connect("dbserver", "grupo27", "EY9iapae0e","db_grupo27");
		}catch(mysqli_sql_exception $e){
			return null;
		}
		return $con;
	}

	/** Funcion que se encarga de cerrar la sesión del admin. **/
	function mcierrasesion(){
		if(session_destroy()){
			return 0;
		}
		else
			return -1;
	}

	/*** Funcion que permite comprobar el tiempo de sesion de un admin, si este excede el limite se le cerrará sesión automaticamente.***/
	function mcomprobaradmin() {
		$con = conexion();
		
		if($con == null){ //No hay conexión
			mcierrasesion();
			return -8080;
		}

		if (!isset($_SESSION['user'])) { //Comprobar que la sesion no el vacia
			return -4;
		}

		$resta = time() - $_SESSION['tiempo']; //Calcular el tiempo que lleva conectado el usuario

		if ($resta > MAX_TIEMPO_SESION) {
			mcierrasesion(); //Destruir la sesion (Borra todas las variables de sesión).
			return -6; //Tiempo de sesión execedido
		}

		//Actualizar el tiempo de sesion, si aun no se ha excedido:
		$_SESSION['tiempo'] = time();

		//Comprobar que ese usuario aun es valido y existe en la BBDD
		$consulta = "select * from final_usuarios where correo_electronico = '" . $_SESSION['user'] . "'";

		if ($resultado = $con->query($consulta)) {
			if (mysqli_num_rows($resultado) == 1){ //Usuario encontrado
				$datos_usuario = $resultado->fetch_assoc();
				if($datos_usuario['bloqueado'] == 0){ //Comprobar que el usuario no este bloqueado:
					if($datos_usuario['Tipo'] == 3){ //Comprobar que es un administrador
						//Guardar info del usuario
						$_SESSION['tipo'] = $datos_usuario['Tipo'];
						$_SESSION['password'] = $datos_usuario['password'];
						$_SESSION['nombre'] = $datos_usuario['nombre'];
						$_SESSION['ap1'] = $datos_usuario['apellido1'];
						$_SESSION['ap2'] = $datos_usuario['apellido2'];
						$_SESSION['provincia'] = $datos_usuario['provincia'];
						$_SESSION['municipio'] = $datos_usuario['municipio'];
						$_SESSION['direccion'] = $datos_usuario['direccion'];
						$_SESSION['nick'] = $datos_usuario['nick'];
						return 0; 
					}
					else{ //Usuario no es admin:
						mcierrasesion();
						return -4;
					}
				}
				else{ //Usuario bloqueado
					mcierrasesion();
					return -3;
				}
			} 
			else {//Error usuario no encontrado:
				return -1;
				session_destroy(); //Destruir la sesion (Borra todas las variables de sesión).
			}
		} 
		else //Error al realizar la query:
			return -2;	
	}

	/*** Funcion que pemite validar el login de un administrador si el login se lleva a cabo con éxito se inicia una sesión 
	para dicho admin ***/
	function mvalidarloginadmin(){
		$con = conexion();

		if($con == null){ //No hay conexión
			mcierrasesion();
			return -8080;
		}

		//Leer el correo y la contraseña que envía el usuario a través del formulario
		
		if(isset($_POST["correo"])){ //Comprobar que el correo no es nulo:
			$correo =  $_POST["correo"];
			if(strlen($correo) <= MAX_CORREO){ //Comprobar la longitud del correo:
				if(mvalidaremail($correo)){ //Comprobar que es un correo valido:
					$check_passwd = mcheckpassword("password");
					if($check_passwd == 0){ //Si contraseña correcta:
						$captcha = mcheckcaptcha();
						if($captcha == 0){ //El usuario ha hecho click en el Captcha
							$passwd = $_POST["password"];
							$passwd = md5($passwd); //Encriptar la constraseña para buscarla en la BBDD
							$consulta = "select * from final_usuarios where correo_electronico = '$correo' and password = '$passwd'";
							if ($resultado = $con->query($consulta)){
								//Contamos el numero de lineas devueltas por la query, si es igual a 1 el inicio de sesion saerá correcto.
								if (mysqli_num_rows($resultado) == 1){//Usuario encontrado en la base de datos:
									$datos_usuario = $resultado->fetch_assoc();
									if($datos_usuario['bloqueado'] == 0){ //Si no es un usuario bloqueado
										if($datos_usuario['Tipo'] == 3){ //Si es admin
											//Iniciar sesion para el admin:
											$_SESSION['user'] = $correo;
											$_SESSION['password'] = $passwd;
											$_SESSION['tipo'] = $datos_usuario['Tipo'];
											$_SESSION['nombre'] = $datos_usuario['nombre'];
											$_SESSION['ap1'] = $datos_usuario['apellido1'];
											$_SESSION['ap2'] = $datos_usuario['apellido2'];
											$_SESSION['provincia'] = $datos_usuario['provincia'];
											$_SESSION['municipio'] = $datos_usuario['municipio'];
											$_SESSION['direccion'] = $datos_usuario['direccion'];
											$_SESSION['nick'] = $datos_usuario['nick'];
											$_SESSION['tiempo'] = time();
											return 0;
										}
										else //Si se ha intentado acceder sin ser administrador:
											return -7;
									}
									else //Si el usuario está bloqueado:
										return -9;
								}
								else //Inicio de sesión incorrecto.
									return -1;
							}
							else //Error en la query
								return -2;
						}
						else return $captcha;
					}
					else //Contraseña incorrecta
						return $check_passwd;
				}
				else //Correo no valido
					return -5;
			}
			else //Longitud de correo incorrecta
				return -4;
		}
		else //Correo vacio
			return -3;
	}

	/** Función que comprueba una única contraseña **/
	function mcheckpassword($nombre){
		if(isset($_POST[$nombre])){ //Comprobar que la contraseña de confirmacion no sea nula
			$passwd = $_POST[$nombre];
			//Comprobar que la contraseña tienen la longitud adecuada:
			if(strlen($passwd) >= MIN_PASSWD && strlen($passwd) <= MAX_PASSWD){
				//Comprobar que la contraseña solo tiene letras (mayusculas o minusculas) y numeros
				if(!preg_match('/[^A-Za-z0-9]/', $passwd)){
					return 0;
				}
				else //Contraseña tiene caracteres incorrectos
					return -6;
			}
			else //Password no cumple con las longitudes
				return -8;
		}
		else //Password nula
			return -13;
	}


	/*** Funcion que permite validar un email ***/
	function mvalidaremail($email){
		
		// Borramos los caracteres no válidos del email
		$email = filter_var($email, FILTER_SANITIZE_EMAIL);
		
		// Realizamos la validación del email
		return filter_var($email, FILTER_VALIDATE_EMAIL);
	}

	/** Función que permite obtener el número de usuarios que hay en la BBDD **/
	function numusuarios(){
		$con = conexion();

		if($con == null){ //No hay conexión
			mcierrasesion();
			return -8080;
		}

		//Comprobar si el admin está buscando una entrada:
		if(isset($_GET["letra"])){
			$letra = $_GET["letra"];

			$consulta = "select count(correo_electronico) as num_usuarios from final_usuarios where nick like '$letra%' and not Tipo = '3'"; //Contar el número de usuarios en la BBDD cuyo correo empieze por letra y devolver el número como una variable llamada num_usuarios
		}
		else//Si no es así obtener todas las entradas:
			$consulta = "select count(correo_electronico) as num_usuarios from final_usuarios where not Tipo = '3'"; //Contar el número de usuarios en la BBDD y devolver el número como una variable llamada num_usuarios

		return $con->query($consulta);
	}

	/** Función que permite obtener los usuarios de la BBDD en función de la página en la que se encuentra el admin y si está buscando,
	también en función de la letra **/
	function mrecuperausuarios(){
		$con = conexion();

		if($con == null){ //No hay conexión
			mcierrasesion();
			return -8080;
		}

		if(isset($_GET["pagina"])){ //Comprobar que el parámetro página no sea nulo.
			$pagina_actual = $_GET["pagina"]; //Obtener el número de página actual en la que se encuentra el admin.
			$pagina_actual--; //Como el número de página comienza por 1 en vez de por 0 reducir en 1 el número de la página.
			$desplazamiento = $pagina_actual * ELEMENTOS_PAGINA; //Calular el desplazamiento en función de la página actual * el número de entradas a mostrar por página

			//Comprobar si el admin está buscando un usuario:
			if(isset($_GET["letra"])){
				$letra = $_GET["letra"];

				//Limit permitirá determinar con el primer parámetro desde que usuario coger de la BBDD y con el segundo parámetro cuantos usuarios coger.
				$consulta = "select correo_electronico, nick, nombre, apellido1, apellido2, bloqueado from final_usuarios where nick like '$letra%' and not Tipo = '3' order by nombre, apellido1, apellido2 limit $desplazamiento, ".ELEMENTOS_PAGINA;
			}
			else
				//Limit permitirá determinar con el primer parámetro desde que usuario coger de la BBDD y con el segundo parámetro cuantos usuarios coger.
				$consulta = "select correo_electronico, nick, nombre, apellido1, apellido2, bloqueado from final_usuarios where not Tipo = '3' order by nombre, apellido1, apellido2 limit $desplazamiento, ".ELEMENTOS_PAGINA;

			return $con->query($consulta); //Devolver los usuarios seleccionados
		}
	}

	/** Función que permite bloquear a un usuario que ha sido seleccionado por el administrador **/
	function mbloquearusuario(){
		$con = conexion();

		if($con == null){ //No hay conexión
			mcierrasesion();
			return -8080;
		}

		if(isset($_GET["usuario"])){ //Comprobar qur el parámetro del usuario no sea nulo.
			$correo = $_GET["usuario"];
			$consulta = "UPDATE final_usuarios SET bloqueado = '1' WHERE final_usuarios.correo_electronico = '$correo'";

			if($con->query($consulta)){ //Si se bloqueó correctamente a el usuario:
				return 0;
			}
			else //No se pudo bloquear al usuario seleccionado 
				return -1;
		}
	}

	/** Función que permite desbloquear a un usuario que ha sido seleccionado por el administrador **/
	function mdesbloquearusuario(){
		$con = conexion();

		if($con == null){ //No hay conexión
			mcierrasesion();
			return -8080;
		}

		if(isset($_GET['usuario'])){ //Comprobar qur el parámetro del usuario no sea nulo.
			$correo = $_GET['usuario'];
			$consulta = "UPDATE final_usuarios SET bloqueado = '0' WHERE final_usuarios.correo_electronico = '$correo'";

			if($con->query($consulta)){ //Si se desbloqueó correctamente a el usuario:
				return 0;
			}
			else //No se pudo desbloquear al usuario seleccionado 
				return -1;
		}
	}

	/** Función que permite obtener el número de solicitudes de verificación que hay en la BBDD **/
	function numverificaciones(){
		$con = conexion();

		if($con == null){ //No hay conexión
			mcierrasesion();
			return -8080;
		}

		//Comprobar si el admin está buscando una solicitud:
		if(isset($_GET['letra'])){
			$letra = $_GET['letra'];

			$consulta = "select count(final_solicitudes.correo_electronico) as num_solicitudes from final_solicitudes inner join final_usuarios on(final_usuarios.correo_electronico = final_solicitudes.correo_electronico) where final_usuarios.nick like '$letra%'"; //Contar el número de solicitudes en la BBDD cuyo correo empieze por letra y devolver el número como una variable llamada num_solicitudes
		}
		else//Si no es así obtener todas las entradas:
			$consulta = "select count(correo_electronico) as num_solicitudes from final_solicitudes"; //Contar el número de usuarios en la BBDD y devolver el número como una variable llamada num_solicitudes

		return $con->query($consulta);
	}

	/** Función que permite obtener las solicitudes de verificación de la BBDD en función de la página en la que se encuentra el admin y si está buscando,
	también en función de la letra **/
	function mrecuperaverificaciones(){
		$con = conexion();

		if($con == null){ //No hay conexión
			mcierrasesion();
			return -8080;
		}

		if(isset($_GET['pagina'])){ //Comprobar que el parámetro página no sea nulo.
			$pagina_actual = $_GET['pagina']; //Obtener el número de página actual en la que se encuentra el admin.
			$pagina_actual--; //Como el número de página comienza por 1 en vez de por 0 reducir en 1 el número de la página.
			$desplazamiento = $pagina_actual * ELEMENTOS_PAGINA; //Calular el desplazamiento en función de la página actual * el número de entradas a mostrar por página

			//Comprobar si el usuario está buscando una entrada:
			if(isset($_GET['letra'])){
				$letra = $_GET['letra'];

				//Limit permitirá determinar con el primer parámetro desde que usuario coger de la BBDD y con el segundo parámetro cuantas solicitudes coger.
				$consulta = "select final_usuarios.nick, final_solicitudes.correo_electronico, fecha from final_solicitudes inner join final_usuarios on(final_usuarios.correo_electronico = final_solicitudes.correo_electronico) where final_usuarios.nick like '$letra%' order by fecha limit $desplazamiento, ".ELEMENTOS_PAGINA;
			}
			else
				//Limit permitirá determinar con el primer parámetro desde que usuario coger de la BBDD y con el segundo parámetro cuantas solicitudes coger.
				$consulta = "select final_usuarios.nick, final_solicitudes.correo_electronico, fecha from final_solicitudes inner join final_usuarios on(final_usuarios.correo_electronico = final_solicitudes.correo_electronico) order by fecha limit $desplazamiento, ".ELEMENTOS_PAGINA;

			return $con->query($consulta); //Devolver las solicitudes seleccionadas.
		}
	}

	/** Función que permite obtener la solicitud de verificación seleccionada por el administrador **/
	function mobtenersolicitud(){
		$con = conexion();

		if($con == null){ //No hay conexión
			mcierrasesion();
			return -8080;
		}
	
		if(isset($_POST['solicitud'])){ //Comprobar qur el parámetro de la solicitud no sea nulo.
			$correo = $_POST['solicitud'];
			$consulta = "select final_solicitudes.*, final_usuarios.nick from final_solicitudes inner join final_usuarios on (final_solicitudes.correo_electronico = final_usuarios.correo_electronico) where final_solicitudes.correo_electronico = '$correo'";

			if($resultado = $con->query($consulta)){ //Devolver la entrada seleccionada.
				if(mysqli_num_rows($resultado) == 1) //Solicitud encontrada en la base de datos
					return $resultado;
			}
			return null;
		}
	}

	/** Función que permite realizar la verificación de un usuario**/
	function mverficarusuario(){
		$con = conexion();

		if($con == null){ //No hay conexión
			mcierrasesion();
			return -8080;
		}

		if(isset($_POST['solicitud'])){ //Comprobar que el parámetro de solicitud no sea nulo.
			$correo = $_POST['solicitud'];
			$consulta = "UPDATE final_usuarios SET Tipo = '2' WHERE final_usuarios.correo_electronico = '$correo'";

			if($con->query($consulta)){ //Si se verificó correctamente a el usuario:
				$consulta2 = "DELETE FROM final_solicitudes WHERE final_solicitudes.correo_electronico = '$correo'";
				if($con->query($consulta2)){//Se ha eliminado correctamente la entrada de la lista de verificaciones
					$explicacion = "";
					if($_POST['textoexplicacion'])
						$explicacion = $_POST['textoexplicacion'];
					$consulta2 = "insert into final_notificaciones(correo_electronico,titulo,texto) values('$correo', 'Su solicitud de verificación ha sido aceptada','Su solicitud fue revisada y aceptada. Comentarios adicionales: $explicacion')";
					$con->query($consulta2);
					return 0;
				}
				else //No se ha podido eliminar la solicitud de la base de datos
					return -1;
			}
			else //No se pudo verificar al usuario seleccionado 
				return -2;
		}
		else //Solicitud no detectada
			return -3;
	}

	/** Función que permite eliminar una solicitud de verificación de un usuario de la BBDD**/
	function mrechazarsolicitud(){
		$con = conexion();

		if($con == null){ //No hay conexión
			mcierrasesion();
			return -8080;
		}

		if(isset($_POST['solicitud'])){ //Comprobar que el parámetro de solicitud no sea nulo.
			$correo = $_POST['solicitud'];
			$consulta = "UPDATE final_usuarios SET Tipo = '0' WHERE final_usuarios.correo_electronico = '$correo'";

			if($con->query($consulta)){ //Si se modifico el tipo del usuario correctamente:
				$consulta2 = "DELETE FROM final_solicitudes WHERE final_solicitudes.correo_electronico = '$correo'";
				if($con->query($consulta2)){ //Se ha eliminado correctamente la entrada de la lista de verificaciones
					$explicacion = "";
					if($_POST['textoexplicacion'])
						$explicacion = $_POST['textoexplicacion'];
					$consulta2 = "insert into final_notificaciones(correo_electronico,titulo,texto) values('$correo', 'Su solicitud de verificación ha sido rechazada','Su solicitud fue revisada y rechazada. Comentarios adicionales: $explicacion')";
					$con->query($consulta2);
					return 0;
				}
				else //No se ha podido eliminar la solicitud de la base de datos
					return -1;
			}
			else //Si no se modifico el tipo del usuario correctamente:
				return -2;
		}
		else //Solicitud no detectada
			return -3;
	}

	/** Función que permite obtener el número de solicitudes de modificación que hay en la BBDD **/
	function nummodificaciones(){
		$con = conexion();

		if($con == null){ //No hay conexión
			mcierrasesion();
			return -8080;
		}

		//Comprobar si el admin está buscando una solicitud:
		if(isset($_GET['letra'])){
			$letra = $_GET['letra'];

			$consulta = "select count(final_entradas_modificadas.correo_creador) as num_solicitudes from final_entradas_modificadas,final_usuarios where final_usuarios.nick like '$letra%' and final_usuarios.correo_electronico = final_entradas_modificadas.correo_creador"; //Contar el número de solicitudes en la BBDD cuyo correo empieze por letra y devolver el número como una variable llamada num_solicitudes
		}
		else//Si no es así obtener todas las entradas:
			$consulta = "select count(correo_electronico) as num_solicitudes from final_solicitudes"; //Contar el número de usuarios en la BBDD y devolver el número como una variable llamada num_solicitudes

		return $con->query($consulta);
	}

	/** Función que permite obtener las solicitudes de modificación de la BBDD en función de la página en la que se encuentra el admin y si está buscando,también en función de la letra **/
	function mrecuperamodificaciones(){
		$con = conexion();

		if($con == null){ //No hay conexión
			mcierrasesion();
			return -8080;
		}

		if(isset($_GET['pagina'])){ //Comprobar que el parámetro página no sea nulo.
			$pagina_actual = $_GET['pagina']; //Obtener el número de página actual en la que se encuentra el admin.
			$pagina_actual--; //Como el número de página comienza por 1 en vez de por 0 reducir en 1 el número de la página.
			$desplazamiento = $pagina_actual * ELEMENTOS_PAGINA; //Calular el desplazamiento en función de la página actual * el número de entradas a mostrar por página

			//Comprobar si el usuario está buscando una entrada:
			if(isset($_GET['letra'])){
				$letra = $_GET['letra'];

				//Limit permitirá determinar con el primer parámetro desde que usuario coger de la BBDD y con el segundo parámetro cuantas solicitudes coger.
				$consulta = "select final_usuarios.nick, id_entrada, correo_creador, fecha from final_entradas_modificadas,final_usuarios where final_usuarios.nick like '$letra%' and final_usuarios.correo_electronico = final_entradas_modificadas.correo_creador order by fecha limit $desplazamiento, ".ELEMENTOS_PAGINA;
			}
			else
				//Limit permitirá determinar con el primer parámetro desde que usuario coger de la BBDD y con el segundo parámetro cuantas solicitudes coger.
				$consulta = "select final_usuarios.nick, id_entrada, correo_creador, fecha from final_entradas_modificadasfinal_,usuarios where final_usuarios.correo_electronico = final_entradas_modificadas.correo_creador order by fecha limit $desplazamiento, ".ELEMENTOS_PAGINA;

			return $con->query($consulta); //Devolver las solicitudes de modificación seleccionadas.
		}
		return null;
	}

	/** Función que permite obtener la solicitud de modificación seleccionada por el administrador **/
	function mobtenermodificacion(){
		$con = conexion();

		if($con == null){ //No hay conexión
			mcierrasesion();
			return -8080;
		}

		if(isset($_POST['solicitud'])){ //Comprobar que el parámetro de la solicitud no sea nulo.
			if(isset($_POST['user'])){ //Comprobar que el parámetro de usuario no sea nulo
				$entrada = $_POST['solicitud'];
				$user = $_POST['user'];
				$consulta = "select final_entradas_modificadas.*,final_usuarios.nick from final_entradas_modificadas,final_usuarios where id_entrada = '$entrada' and  final_usuarios.correo_electronico = final_entradas_modificadas.correo_creador AND correo_creador = '$user'";

				if($resultado = $con->query($consulta)){ //Devolver la entrada seleccionada.
					if(mysqli_num_rows($resultado) == 1) //Solicitud encontrada en la base de datos
						return $resultado;
				}
			}
		}
		return null;
	}

	/** Función que permite obtener la entrada original que se corresponde con la modificación seleccionada por el administrador **/
	function mobteneroriginal(){
		$con = conexion();

		if($con == null){ //No hay conexión
			mcierrasesion();
			return -8080;
		}

		if(isset($_POST['solicitud'])){ //Comprobar qur el parámetro de la solicitud no sea nulo.
			$entrada = $_POST['solicitud'];
			$consulta = "select final_entradas.*, final_usuarios.nick from final_entradas,final_usuarios where id_entrada = '$entrada' and final_usuarios.correo_electronico = final_entradas.correo_creador";

			if($resultado = $con->query($consulta)){ //Devolver la entrada seleccionada.
				if(mysqli_num_rows($resultado) == 1) //Solicitud encontrada en la base de datos
					return $resultado;
			}
			return null;
		}
	}

	
	/** Función que permite comprobar una modificación enviada por un administrador, en caso de ser correcta la almacena en la 
	base de datos **/
	function mcheckmodificacion(){
		$con = conexion();

		if($con == null){ //No hay conexión
			mcierrasesion();
			return -8080;
		}

		if(isset($_POST['tituloModificar'])){ //Comprobar que el titulo de la modificación no es vacio:
			if(isset($_POST['contenidoModificar'])){ //Comprobar que el contenido de la modificación no es vacio:
				$titulo = $_POST['tituloModificar'];
				$contenido = $_POST['contenidoModificar'];
				if(strlen($titulo) >= MIN_TITULO &&  strlen($titulo) <= MAX_TITULO){ //Comprobar la longitud del titulo de la modificación:
					if(strlen($contenido) >= MIN_CONTENIDO){ //Comprobar la longitud del contenido de la modificación:
						$id_entrada = $_POST['entrada'];
						if(isset($_POST['user'])){
							$correo = $_POST['user'];
						}
						else
							$correo = $_SESSION['user'];
						if(isset($_POST['url']) && strlen($_POST['url']) > 0){
							$url = $_POST['url'];
							$consulta = "UPDATE final_entradas SET titulo = '$titulo', texto = '$contenido', correo_modificacion = '$correo', trailer = '$url' WHERE final_entradas.id_entrada = $id_entrada";
						}
						else
							$consulta = "UPDATE final_entradas SET titulo = '$titulo', texto = '$contenido', correo_modificacion = '$correo' WHERE final_entradas.id_entrada = $id_entrada";
						if($con->query($consulta)){
							if($correo != $_SESSION['user']){
								$consulta2 = "DELETE FROM final_entradas_modificadas WHERE final_entradas_modificadas.id_entrada = '$id_entrada' AND final_entradas_modificadas.correo_creador = '$correo'";
								if($con->query($consulta2)){//Delete correcto
									if(isset($_POST['textoexplicacion']))
										$explicacion = $_POST['textoexplicacion'];

									$consulta3 ="insert into final_notificaciones(correo_electronico,titulo,texto) values('$correo', 'Su modificación de la entrada $titulo ha sido aceptada','Su modificación fue revisada y aceptada. Comentarios adicionales: $explicacion')";

										$con->query($consulta3);
									return 0;
								}
								else //Error delete
									return -1;
							}
							else //Admin modificando
								return 0;
						}
						else //Error en el insert
							return -2;
					}
					else //Contenido de la modificación no cumple la longitud:
						return -3;
				}
				else { //Titulo de la modificación no cumple la longitud:
					return -4;
				}
			} else { //Contenido de la modificación es vacio:
				return -6;
			}
		}
		else //Titulo de la modificación es vacio:
			return -7;
	}

	/** Función que permite eliminar una wolicitud de modificación de un usuario **/
	function meliminarmodificacion(){
		$con = conexion();

		if($con == null){ //No hay conexión
			mcierrasesion();
			return -8080;
		}

		if(isset($_POST['solicitud'])){ //Comprobar el número de la solicitud de modificación
			if(isset($_POST['user'])){ //Comprobar que el contenido de user no es vacio:
				$id_entrada = $_POST['solicitud'];
				$correo = $_POST['user'];
				$consulta = "DELETE FROM entradas_modificadas WHERE entradas_modificadas.id_entrada = '$id_entrada' AND entradas_modificadas.correo_creador = '$correo'";
				if($con->query($consulta)){//Delete correcto
					//Enviar notificación a el usuario de la página web:
					$explicacion = "";
					$titulo="";
					if(isset($_POST['textoexplicacion']))
						$explicacion = $_POST['textoexplicacion'];
					if(isset($_POST['tituloModificar']))
						$titulo = $_POST['tituloModificar'];
					$consulta2 = "insert into final_notificaciones(correo_electronico,titulo,texto) values('$correo', 'Su modificación de la entrada $titulo ha sido rechazada','Su modificación fue revisada y rechazada. Comentarios adicionales: $explicacion')";
					$con->query($consulta2);
					return 0;
				}
				else //Delete incorrecto
					return -1;
			}
			else //Campo user es vacio.
				return;
		}
		else //Campo de solicitud nulo.
			return;
	}

	/** Función que permite recuperar la imagen de perfil de un usuario de la base de datos : **/
	function mrecuperaimagen(){
		$con = conexion();

		if($con == null){ //No hay conexión
			mcierrasesion();
			return -8080;
		}

		$correo = $_SESSION['user'];
		$consulta = "Select imagen_perfil from final_usuarios where correo_electronico = '$correo'";

		return $con->query($consulta);
	}

	/** Función que permite recuperar el nombre de la provincia del perfil del usuario **/
	function mrecuperaprovincia(){
		$con = conexion();

		if($con == null){ //No hay conexión
			mcierrasesion();
			return -8080;
		}

		$id_provincia = $_SESSION['provincia'];
		$consulta = "select provincia from final_provincias where id_provincia = '$id_provincia'";

		if($result = $con->query($consulta)){ //Si la query ha ido bien:
			return $result;
		}
		return null;
	}

	/** Función que permite recuperar el nombre de el municipio del perfil del usuario **/
	function mrecuperamunicipio(){
		$con = conexion();

		if($con == null){ //No hay conexión
			mcierrasesion();
			return -8080;
		}

		$id_municipio = $_SESSION['municipio'];
		$consulta = "select municipio from final_municipios where id_municipio = '$id_municipio'";

		if($result = $con->query($consulta)){ //Si la query ha ido bien:
			return $result;
		}
		return null;
	}

	/** Funcion que permite controlar el cambio de contraseña de un admin:**/
	function mcambiopassword(){
		$con = conexion();

		if($con == null){ //No hay conexión
			mcierrasesion();
			return -8080;
		}
		
		//Comprobar la password actual:
		$check_actual = mcheckpassword("password_actual");

		if($check_actual == 0){ 
			$passwd_actual = $_POST['password_actual'];
			if(strcmp(md5($passwd_actual),$_SESSION['password']) == 0){ //Comparar la password actual
			
				//Chequear las passwords nuevas:
				$check_passwords = mcheckpasswords('password','password_conf');

				//Si las passwords son correctas:
				if($check_passwords == 0){
					$password = $_POST['password'];
					if(strcmp($password,$passwd_actual) != 0){ //Comprobar que la password nueva no sea igual a la anterior:
						$correo = $_SESSION['user'];
						$password = md5($password);
						$consulta = "UPDATE final_usuarios SET password = '$password' WHERE final_usuarios.correo_electronico = '$correo'";

						if($con->query($consulta)){
							mcierrasesion(); //Cerrar la sesión del usuario.
							return 0; //Modificación correcta
						}
						else{
							return -1; //Error en el update
						}
					}
					else //Password nueva igual a la anterior:
						return -3;
				}
				else //Constraseñas no correctas:
					return $check_passwords;
			}
			else //Password actual no coincide
				return -2;
		}
		else //Contraseña actual incorrecta
			return $check_actual-10;
	}

	/** Función que permite controlar el cambio de imagen de perfil de un usuario **/ 
	function mcambioimagen(){
		$con = conexion();

		if($con == null){ //No hay conexión
			mcierrasesion();
			return -8080;
		}

		if(isset($_FILES['fichero']) && !empty($_FILES['fichero']['tmp_name'])){ //Comprobar que el fichero no es vacio
			//Comprobar si el fichero que ha subido el usuario es una imagen o no:
			$check = getimagesize($_FILES['fichero']['tmp_name']);
   			if($check == true) { //El fichero es una imagen:
   				//Comprobar que el tamaño no excede el máximo:
        		if ($_FILES['fichero']['size'] <= MAX_IMAGEN) {
				    //Comprobar el formato del fichero:
					$trozos = explode("/", $_FILES["fichero"]["type"]);
				    $imageFileType = strtolower($trozos[1]);
				    if($imageFileType == "jpg" || $imageFileType == "png" || $imageFileType == "jpeg"){
				    	$imagen= addslashes (file_get_contents($_FILES['fichero']['tmp_name']));
				    	$correo = $_SESSION['user'];
				    	$consulta = "UPDATE final_usuarios SET imagen_perfil = '$imagen' WHERE final_usuarios.correo_electronico = '$correo'";

				    	if($con->query($consulta)){ //Foto actualizada correctamente:
				    		return 0;
				    	} 
				    	else //Error en el update:
				    		return -1;
				    }
				    else //Formato no valido
				    	return -2;
				}
				else //Imagen supera el límite de tamaño:
					return -3;
   			}
   			else //El fichero no es una imagen
		    	return -4;
		}
		else //Fichero subido es vacio:
			return -5;
	}

	/** Función que permite comprobar si un usuario ha aceptado o no el captcha **/
	function mcheckcaptcha(){
		//Clave secreta del Captcha
		$secret = "6LcJhqIUAAAAANhHZOjtygHU_uj100cy6gDV-85z";
 
		//Respuesta del Captcha:
		$response = null;
 
		//Comprobar la clave secreta del Captcha
		$reCaptcha = new ReCaptcha($secret);
		

		//Comprobhar que se ha hecho click en el Captcha y verificar la respuesta:
		if ($_POST["g-recaptcha-response"]) {
		    $response = $reCaptcha->verifyResponse(
		        $_SERVER["REMOTE_ADDR"],
		        $_POST["g-recaptcha-response"]
		    );
		}

		//Si la respuesta no es nula u el valor es correcto:
		if ($response != null && $response->success) {
			return 0;
		}
		else
			return -31;
	}

		function mlistapost(){
		/*
			Devuelve: informacion sobre todos los posts existentes
		*/
		$bd = conexion();

		if($bd == null){ //No hay conexión
			mcierrasesion();
			return -8080;
		}

		$consulta = "select final_posts.*,final_usuarios.nick, count(final_comentarios.id_post) as 'ncomentarios' from final_posts,final_comentarios,final_usuarios where final_posts.id_post = final_comentarios.id_post and final_usuarios.correo_electronico = final_posts.correo_creador GROUP BY final_posts.id_post,final_comentarios.id_post order by final_posts.fecha desc";
		return $bd->query($consulta);
	}

	function mforoelegido(){
		/*
			Devuelve: 
			- Informacion sobre el foro elegido
		*/
		$bd = conexion();

		if($bd == null){ //No hay conexión
			mcierrasesion();
			return -8080;
		}

		if(isset($_POST['idpost'])){
			$idpost = $_POST["idpost"];
			$consulta1 = "select final_posts.*, final_usuarios.nick, count(final_comentarios.id_post) as num_comentarios from final_posts inner join final_usuarios inner join final_comentarios on (final_posts.correo_creador = final_usuarios.correo_electronico and final_comentarios.id_post = final_posts.id_post) where final_posts.id_post = $idpost";
			if($result = $bd->query($consulta1))
				return $result;
		}
		return null;
	}

	/** Función que permite recuperar los comentarios asociados a un post determinado **/
	function mrecuperacomentarios(){
		$con = conexion();

		if($con == null){ //No hay conexión
			mcierrasesion();
			return -8080;
		}

		if(isset($_POST['pagina_actual'])){ //Comprobar que el parámetro página no sea nulo.
			if (isset($_POST['idpost'])) { //Comprobar el id del post
				$id_post = $_POST['idpost'];
				$pagina_actual = $_POST['pagina_actual']; //Obtener el número de página actual en la que se encuentra el usuario.
				$pagina_actual--; //Como el número de página comienza por 1 en vez de por 0 reducir en 1 el número de la página.
				$desplazamiento = $pagina_actual * COMENTARIOS_PAGINA; //Calular el desplazamiento en función de la página actual * el número de comentarios a mostrar por página

				$consulta = "select final_comentarios.*, final_usuarios.nick from final_comentarios inner join final_usuarios on (final_usuarios.correo_electronico = final_comentarios.correo_escritor) where id_post = '$id_post' order by fecha desc limit $desplazamiento, ".COMENTARIOS_PAGINA;
				if($result = $con->query($consulta))
					return $result;
			}
		}
		return null;
	}

	function mescribircomentario(){
		/*
			Funcion que inserta un nuevo comentario en la BBDD
		*/
		$bd = conexion();

		if($bd == null){ //No hay conexión
			mcierrasesion();
			return -8080;
		}

		if(isset($_POST["comentario"]) && !empty($_POST["comentario"])){
			$comentario = $_POST["comentario"];
			$aux = strlen(preg_replace('/\s/', '', $comentario)); //Eliminar todos los espacios en blanco
			if(isset($_POST["idpost"])){
				if($aux >= MIN_COMENTARIO && $aux <= MAX_COMENTARIO){
					$correo_usuario = $_SESSION['user'];
					$idpost = $_POST["idpost"];
					$consulta = "insert into final_comentarios (id_post,comentario,correo_escritor) values ($idpost,'$comentario','$correo_usuario')";
					if($bd->query($consulta))
						return 0;
					else
						return -1;
				}
				else
					return -2;
			}
			else
				return -3;
		}
		else
			return -4;
	}

	function mnuevoforo(){
		/*
			Generamos un nuevo post con un primer comentario y mostramos la lista de los foros.
		*/
		$bd = conexion();

		if($bd == null){ //No hay conexión
			mcierrasesion();
			return -8080;
		}

		$comentario = $_POST["comentario"];
		$titulo_post = $_POST["titulo_post"];
		$correo_creador = $_SESSION['user'];
		$correo_escritor = $correo_creador;
		$max_id_c = "select max(id_post) 'max' from final_posts";
		$res = $bd->query($max_id_c);
		$datos = $res->fetch_assoc();
		$max_id = $datos["max"];
		$max_id = $max_id + 1;
		$consulta1 = "insert into final_posts (id_post,correo_creador,titulo) values ($max_id,'$correo_creador','$titulo_post')";
		$bd->query($consulta1);
		$consulta2 = "insert into final_comentarios (id_post,comentario,correo_escritor) values ($max_id,'$comentario','$correo_escritor')";
		$bd->query($consulta2);
		return mlistapost();
	}

	function mbuscarforo(){
		/*
			Devuelve: Informacion de los foros cuyo titulo comienza con las letras introducidas en un buscador.
		*/
		$bd = conexion();

		if($bd == null){ //No hay conexión
			mcierrasesion();
			return -8080;
		}
		if(isset($_GET["pagina"])){ //Comprobar que el parámetro página no sea nulo.
			$pagina_actual = $_GET["pagina"]; //Obtener el número de página actual en la que se encuentra el usuario.
			$pagina_actual--; //Como el número de página comienza por 1 en vez de por 0 reducir en 1 el número de la página.
			$desplazamiento = $pagina_actual * ELEMENTOS_PAGINA; //Calular el desplazamiento en función de la página actual * el número de entradas a mostrar por página

			if(isset($_GET["letras"]) && strcmp($_GET["letras"]," ") != 0){
				$letras = $_GET["letras"];

				$consulta = "select final_posts.*, final_usuarios.nick, count(final_comentarios.id_post) as 'ncomentarios' from final_posts,final_comentarios,final_usuarios where final_posts.titulo like '$letras%' and final_posts.id_post = final_comentarios.id_post and final_usuarios.correo_electronico = final_posts.correo_creador GROUP BY final_posts.id_post,final_comentarios.id_post order by final_posts.fecha desc limit $desplazamiento,".ELEMENTOS_PAGINA;
			}
			else
				$consulta = "select final_posts.*,final_usuarios.nick, count(final_comentarios.id_post) as 'ncomentarios' from final_posts,final_comentarios,final_usuarios where final_posts.id_post = final_comentarios.id_post and final_usuarios.correo_electronico = final_posts.correo_creador GROUP BY final_posts.id_post,final_comentarios.id_post order by final_posts.fecha desc limit $desplazamiento,".ELEMENTOS_PAGINA;
			return $bd->query($consulta);
		}
		return null;
	}

	/** Función que permite a un administrador eliminar un post **/
	function meliminarpost(){
		$bd = conexion();

		if($bd == null){ //No hay conexión
			mcierrasesion();
			return -8080;
		}

		if(isset($_POST['idpost'])){
			$id_post = $_POST['idpost'];

			$consulta = "delete from final_posts where id_post = '$id_post'";

			if($bd -> query($consulta)){ //Delete correcto
				return 0;
			}
			else //Error al realizar el borrado
				return -1;
		}
		else //Post no encontrado
			return -3;
	}

	function meliminarcomentario(){
		$bd = conexion();

		if($bd == null){ //No hay conexión
			mcierrasesion();
			return -8080;
		}

		if(isset($_POST['idpost'])){
			if(isset($_POST['idfecha'])){
				if(isset($_POST['idcreador'])){
					$id_post = $_POST['idpost'];
					$id_fecha = $_POST['idfecha'];
					$id_creador = $_POST['idcreador'];

					$consulta = "delete from final_comentarios where id_post = '$id_post' and correo_escritor = '$id_creador' and fecha = '$id_fecha'";

					if($bd -> query($consulta)){ //Delete correcto
						return 0;
					}
					else //Error al realizar el borrado
						return -1;
				}
				else //Creador no encontrado
					return -3;
			}
			else //Fecha no encontrada
				return -4;
		}
		else //Comentario no encontrado
			return -5;
	}


	function mbuscaractores(){
		$bd = conexion();

		if($bd == null){ //No hay conexión
			mcierrasesion();
			return -8080;
		}

		$letras = $_GET["letras"];

		$consulta = "select * from final_actores where final_actores.nombre like '$letras%' or final_actores.apellido1 like '$letras%' or final_actores.apellido2 like '$letras%'";
		return $bd->query($consulta);
	}

	function mbuscardirectores(){
		$bd = conexion();

		if($bd == null){ //No hay conexión
			mcierrasesion();
			return -8080;
		}

		$letras = $_GET["letras"];

		$consulta = "select * from final_directores where final_directores.nombre like '$letras%' or final_directores.apellido1 like '$letras%' or final_directores.apellido2 like '$letras%'";
		return $bd->query($consulta);
	}

	function mnuevoactor(){
		$bd = conexion();

		if($bd == null){ //No hay conexión
			mcierrasesion();
			return -8080;
		}

		if (isset($_POST['nombre']) and isset($_POST['apellido1'])){
			$bd = conexion();
			$nombre = $_POST['nombre'];
			$apellido1 = $_POST['apellido1'];
			$consulta = "";
			if (isset($_POST['apellido2'])){
				$apellido2 = $_POST['apellido2'];
				$consulta = "insert into final_actores (nombre,apellido1,apellido2) values ('$nombre','$apellido1','$apellido2')";
			}else{
				$consulta = "insert into final_actores (nombre,apellido1) values '$nombre','$apellido1";
			}
			$bd->query($consulta);
			return 1;
		}else{
			return -1;
		}
	}

	function mnuevodirector(){
		$bd = conexion();

		if($bd == null){ //No hay conexión
			mcierrasesion();
			return -8080;
		}

		if (isset($_POST['nombre']) and isset($_POST['apellido1'])){
			$bd = conexion();
			$nombre = $_POST['nombre'];
			$apellido1 = $_POST['apellido1'];
			$consulta = "";
			if (isset($_POST['apellido2'])){
				$apellido2 = $_POST['apellido2'];
				$consulta = "insert into final_directores (nombre,apellido1,apellido2) values ('$nombre','$apellido1','$apellido2')";
			}else{
				$consulta = "insert into final_directores (nombre,apellido1) values '$nombre','$apellido1";
			}
			$bd->query($consulta);
			return 1;
		}else{
			return -1;
		}
	}

	function mguardarentrada(){
		if (isset($_GET['titulo'])){
			$titulo = $_GET['titulo'];
			if (isset($_GET['actores'])){
				$actores = $_GET['actores'];
				if (isset($_GET['directores'])){
					$directores = $_GET['directores'];
					if (isset($_GET['sinopsis'])){
						$bd = conexion();

						if($bd == null){ //No hay conexión
							mcierrasesion();
							return -8080;
						}

						$sinopsis = $_GET['sinopsis'];
						$correo_creador = $_SESSION['user'];

						if(!empty($sinopsis) && !empty($titulo) && strlen($titulo) >= MIN_TITULO && strlen($titulo) <= MAX_TITULO && strlen($sinopsis) >= MIN_CONTENIDO){
							//Creamos la entrada en primer lugar para que no falle lo demas.
							if (isset($_GET['trailer']) or !empty($_GET['trailer'])){
								$trailer = $_GET['trailer'];
								$consulta = "insert into final_entradas (correo_creador,titulo,texto,trailer) values ('$correo_creador','$titulo','$sinopsis','$trailer')";
								$bd->query($consulta);
							}else{
								$consulta = "insert into final_entradas (correo_creador,titulo,texto) values ('$correo_creador','$titulo','$sinopsis')";
								$bd->query($consulta);
							}
							$ent_id = $bd->insert_id;
							for($i = 0;$i < sizeof($actores);$i++){
								$consulta = "insert into final_entradas_actores (id_entrada,id_actor) values ($ent_id,$actores[$i])";
								$bd->query($consulta);
							}
							for($i = 0;$i < sizeof($directores);$i++){
								$consulta = "insert into final_entradas_directores (id_entrada,id_director) values ($ent_id,$directores[$i])";
								$bd->query($consulta);
							}
							return $ent_id;
						}
						else
							return -1;
					}
				}else{
					return -1;
				}
			}else{
				return -1;
			}
		}
	}

	/** función que permite actualizar el tiempo de session del admin **/
	function mactualizatiempo(){
		$_SESSION['tiempo'] = time();
	}

	function mmostrarresultadoimportar(){

		if (isset($_FILES['fichero']) and (empty($_FILES['fichero']['name']) != 1)){

			$fileName = $_FILES['fichero']['name'];
			$fileTmpName = $_FILES['fichero']['tmp_name'];

			//Comprobamos si la extension del fichero es la correcta.
			$fileExtension = pathinfo($fileName,PATHINFO_EXTENSION);

			$extension = array('csv');

			if (!in_array($fileExtension, $extension)){
				return -1; // Extension del archivo incorrecta'
			}else{
				$bd = conexion();
				if($bd == null){ //No hay conexión
					mcierrasesion();
					return -8080;
				}
				$file = fopen($fileTmpName, 'r');
				$datos = fgetcsv($file,10000,';');
				$aciertos = 0;
				$num_desc = 0;
				while (($datos = fgetcsv($file,10000,';')) != FALSE){
					if (!isset($datos) or $datos[0] == null or empty($datos[0]))
						break;
					$cine = $datos[0];
					$pelicula = $datos[1];
					$cantidad = $datos[2];
					$fecha = $datos[3];
					$fecha_troz = explode('/', $fecha);
					$dia = $fecha_troz[0];
					$mes = $fecha_troz[1];
					$ano = $fecha_troz[2];
					$consulta = "insert into final_descuentos (cantidad,cine,pelicula,dia,mes,ano) values ('$cantidad','$cine','$pelicula','$dia','$mes','$ano')";
					if ($bd->query($consulta) == TRUE) {
						$aciertos = $aciertos + 1;
					}
					$num_desc = $num_desc + 1;
				}
				if ($num_desc == $aciertos){
					return 1;
				}else{
					return 0;
				}
			}
		}else{
			return -2; //No se ha seleccionado ningun fichero;
		}
	}

	function mguardardescuento(){
		$bd = conexion();
		if($bd == null){ //No hay conexión
			mcierrasesion();
			return -8080;
		}
		if (empty($_GET['cine']) or empty($_GET['cantidad']) or empty($_GET['fecha']) or empty($_GET['pelicula'])){
			return -1;
		}else{
			$cine = $_GET['cine'];
			$cantidad = $_GET['cantidad'];
			$pelicula = $_GET['pelicula'];
			$fecha = $_GET['fecha'];
			$fecha_troz = explode('/', $fecha);
			if (count($fecha_troz) == 3){
				$dia = $fecha_troz[0];
				$mes = $fecha_troz[1];
				$ano = $fecha_troz[2];
			}else{
				return -5;
			}
			if (is_numeric($cantidad) and is_numeric($dia) and is_numeric($mes) and is_numeric($ano) and is_string($cine) and is_string($pelicula)){
				$consulta = "insert into final_descuentos (cantidad,cine,pelicula,dia,mes,ano) values ($cantidad,'$cine','$pelicula',$dia,$mes,$ano)";
				$bd->query($consulta);
				return 1;
			}else{
				return -4;
			}
		}
	}

	function mexportardescuento(){
		$bd = conexion();
		if($bd == null){ //No hay conexión
			mcierrasesion();
			return -8080;
		}
		if (empty($_POST['cine']) or empty($_POST['cantidad']) or empty($_POST['fecha']) or empty($_POST['pelicula'])){
			return -1;
		}else{
			$cine = $_POST['cine'];
			$cantidad = $_POST['cantidad'];
			$fecha = $_POST['fecha'];
			$pelicula = $_POST['pelicula'];
			$fecha_troz = explode('/', $fecha);
			if (count($fecha_troz) == 3){
				$dia = $fecha_troz[0];
				$mes = $fecha_troz[1];
				$ano = $fecha_troz[2];
			}else{
				return -5;
			}
			if (is_numeric($cantidad) and is_numeric($dia) and is_numeric($mes) and is_numeric($ano) and is_string($cine) and is_string($pelicula)){
				$myArray = array('cine'=> $cine, 'pelicula'=> $pelicula,'cantidad' => $cantidad, 'fecha' => $fecha);
				$data = json_encode($myArray);
				return array(1,$data);
			}else{
				return -4;
			}
		}
	}

	/** Función que permite recuperar el número de posts del foro **/
	function mnumeropost(){
		$con = conexion();

		if($con == null){ //No hay conexión
			mcierrasesion();
			return -8080;
		}

		if(isset($_GET['letras']) && strcmp($_GET['letras'], " ") != 0){
			$letras = $_GET['letras'];
			$consulta = "select count(id_post) as num_posts from final_posts where titulo like '$letras%'";
		}
		else
			$consulta = "select count(id_post) as num_posts from final_posts";

		if($result = $con -> query($consulta))
			return $result;
		else
			return null;
	}

	function mguardarimagen(){
		$con = conexion();
		if($con == null){ //No hay conexión
			mcierrasesion();
			return -8080;
		}
		if(isset($_FILES["file"]) == 1){ //Comprobar que el fichero no es vacio
			//Comprobar si el fichero que ha subido el usuario es una imagen o no:
			$check = getimagesize($_FILES['file']['tmp_name']);
   			if($check == true) { //El fichero es una imagen:

   				//Comprobar que el tamaño no excede el máximo:
        		if ($_FILES['file']['size'] <= MAX_IMAGEN) {
				    //Comprobar el formato del fichero:
					$trozos = explode("/", $_FILES["file"]["type"]);
				    $imageFileType = strtolower($trozos[1]);
				    if($imageFileType == "jpg" || $imageFileType == "png" || $imageFileType == "jpeg"){
				    	//Si hemos llegado hasta aquí, podemos guardar los datos de la imágen.
				    	$img_data = base64_encode((file_get_contents($_FILES["file"]["tmp_name"])));
				    	$img_name = addslashes(($_FILES["file"]["name"]));
				    	$identrada = $_POST["identrada"];
				    	$consulta = "insert into final_imagenes (nombre,imagen,id_entrada) values ('$img_name','$img_data',$identrada)";
				    	$con->query($consulta);
				    }
				    else //Formato no valido
				    	echo "formato no valido";
				}
				else {//Imagen supera el límite de tamaño:
					echo "imagen superar el limite";
				}
   			}
   			else //El fichero no es una imagen
		    	echo "fichero no es imagen";
		}
		else {//Fichero subido es vacio:
			echo "fichero vacio";
		}
	}
	function mrecuperaentrada(){
		$con = conexion();

		if($con == null){ //No hay conexión
			mcierrasesion();
			return -8080;
		}

		if(isset($_POST["entrada"])){ //Comprobar qur el parámetro de la entrada no sea nulo.
			$entrada = $_POST["entrada"];
			$consulta = "select * from final_entradas where id_entrada = '$entrada'";
			$consulta_actores = "select final_actores.* from final_actores NATURAL JOIN final_entradas_actores where final_entradas_actores.id_entrada = $entrada";
			$consulta_directores = "select final_directores.* from final_directores NATURAL JOIN final_entradas_directores where final_entradas_directores.id_entrada = $entrada";
			if($resultado = $con->query($consulta) and $resultado1 = $con->query($consulta_actores) and $resultado2 = $con->query($consulta_directores)){ //Devolver la entrada seleccionada.
				return array($resultado,$resultado1,$resultado2);
			}
		}
		return null;
	}

	function mactualizarimagenes(){
		$bd = conexion();
		$identrada = $_POST['identrada'];
		$im_principal = $_POST['imagen_principal'];
		$principal = "select * from final_imagenes where id_entrada = $identrada and id_imagen = $im_principal";
		$secundarias = "select * from final_imagenes where id_entrada = $identrada and id_imagen <> $im_principal";
		return array($bd->query($principal),$bd->query($secundarias));
	}
	function mrecuperaentradas(){
		$con = conexion();

		if($con == null){ //No hay conexión
			mcierrasesion();
			return -8080;
		}

		if(isset($_GET["pagina"])){ //Comprobar que el parámetro página no sea nulo.
			$pagina_actual = $_GET["pagina"]; //Obtener el número de página actual en la que se encuentra el usuario.
			$pagina_actual--; //Como el número de página comienza por 1 en vez de por 0 reducir en 1 el número de la página.
			$desplazamiento = $pagina_actual * ELEMENTOS_PAGINA; //Calular el desplazamiento en función de la página actual * el número de entradas a mostrar por página

			//Comprobar si el usuario está buscando una entrada:
			if(isset($_GET["letra"])){
				$letra = $_GET["letra"];

				//Limit  permitirá determinar con el primer parámetro desde que entrada coger de la BBDD y con el segundo parámetro cuantas entradas coger.
				$consulta = "select final_entradas.* from final_entradas where titulo like '$letra%' order by fecha_creacion limit $desplazamiento, ".ELEMENTOS_PAGINA;
			}
			else
				//Limit  permitirá determinar con el primer parámetro desde que entrada coger de la BBDD y con el segundo parámetro cuantas entradas coger.
				$consulta = "select final_entradas.* from final_entradas order by fecha_creacion limit $desplazamiento, ".ELEMENTOS_PAGINA;

			return $con->query($consulta); //Devolver las entradas seleccionadas.
		}
	}
	function mnumentradas(){
		$con = conexion();

		if($con == null){ //No hay conexión
			mcierrasesion();
			return -8080;
		}

		//Comprobar si el usuario está buscando una entrada:
		if(isset($_GET["letra"])){
			$letra = $_GET["letra"];

			$consulta = "select count(id_entrada) as num_entradas from final_entradas where titulo like '$letra%'"; //Contar el número de entradas en la BBDD que empiecen por una letra y devolver el número como una variable llamada num_entradas
		}
		else//Si no es así obtener todas las entradas:
			$consulta = "select count(id_entrada) as num_entradas from final_entradas"; //Contar el número de entradas en la BBDD y devolver el número como una variable llamada num_entradas

		return $con->query($consulta);
	}
	function mselecentrada(){
		/*
			Devuelve: 
			- Informacion relacionada con la entrada seleccionada
			- Criticas sobre la entrada correspondiente
			- Escrito: comprobacion de si el usuario que intenta escribir ya ha escrito una critica, en cuyo caso no podremos escribir una nueva critica
		*/
		$bd = conexion();

		if($bd == null){ //No hay conexión
			mcierrasesion();
			return -8080;
		}

		if(isset($_POST['entrada'])){
			$identrada = $_POST['entrada'];
			$correosesion = $_SESSION['user'];
			$consulta = "select final_entradas.*,final_usuarios.nick as nick_creador, (SELECT nick from final_usuarios WHERE final_usuarios.correo_electronico = final_entradas.correo_modificacion) as nick_modificacion from final_entradas,final_usuarios where final_entradas.id_entrada='$identrada' and final_usuarios.correo_electronico = final_entradas.correo_creador";
			$consulta_actores = "select final_actores.* from final_actores NATURAL JOIN final_entradas_actores where final_entradas_actores.id_entrada=$identrada";
			$consulta_directores = "select final_directores.* from final_directores NATURAL JOIN final_entradas_directores where final_entradas_directores.id_entrada = $identrada";
			$criticas = "select * from final_criticas where id_entrada=$identrada";
			$punt_media = "select CAST(AVG(valoracion) AS DECIMAL(10,1)) as media from final_criticas where id_entrada = $identrada";
			$escrito = "select count(*) as numcriticas from final_criticas where id_entrada = $identrada AND final_criticas.correo_creador='$correosesion'";
			$imagenes = "select * from final_imagenes where id_entrada = $identrada";
			return array($bd->query($consulta),$bd->query($consulta_actores),$bd->query($consulta_directores),$bd->query($criticas),$bd->query($punt_media),$bd->query($escrito),$bd->query($imagenes));
		}
		return null;
	}

	function meliminarentrada(){
		$con = conexion();

		if($con == null){ //No hay conexión
			mcierrasesion();
			return -8080;
		}

		if (isset($_POST['entrada_elim'])){
			$id_entrada = $_POST['entrada_elim'];
			$consulta = "delete from final_entradas where id_entrada = $id_entrada";
			if ($con->query($consulta)){
				return 1;
			}else{
				return -1;
			}
		}
	}

	function mrecuperacriticas(){
		$bd = conexion();

		if($bd == null){ //No hay conexión
			mcierrasesion();
			return -8080;
		}

		if(isset($_GET['entrada'])){
			if(isset($_GET["pagina"])){ //Comprobar que el parámetro página no sea nulo.
				$identrada = $_GET['entrada'];
				$pagina_actual = $_GET["pagina"]; //Obtener el número de página actual en la que se encuentra el usuario.
				$pagina_actual--; //Como el número de página comienza por 1 en vez de por 0 reducir en 1 el número de la página.
				$desplazamiento = $pagina_actual * ELEMENTOS_PAGINA; //Calular el desplazamiento en función de la página actual * el número de criticas a mostrar por página

				if(isset($_GET["letras"]) && strcmp($_GET["letras"]," ") != 0){
					$letras = $_GET["letras"];
					$criticas = "select final_criticas.*,final_usuarios.nick from final_criticas inner join final_usuarios on (final_usuarios.correo_electronico = final_criticas.correo_creador) where final_criticas.id_entrada='$identrada' and final_usuarios.nick like '$letras%' order by final_criticas.fecha limit $desplazamiento,".ELEMENTOS_PAGINA;
				}
				else
					$criticas = "select final_criticas.*,final_usuarios.nick from final_criticas inner join final_usuarios on (final_usuarios.correo_electronico = final_criticas.correo_creador) where final_criticas.id_entrada='$identrada' order by final_criticas.fecha limit $desplazamiento,".ELEMENTOS_PAGINA;
				return $bd->query($criticas);
			}
		}
		return null;
	}

	function mnumerocriticas(){
		$bd = conexion();

		if($bd == null){ //No hay conexión
			mcierrasesion();
			return -8080;
		}

		if(isset($_GET['entrada'])){
			if(isset($_GET['letras']) && strcmp($_GET['letras'], " ") != 0){
				$letras = $_GET['letras'];
				$identrada = $_GET['entrada'];
				$consulta = "select count(id_entrada) as num_criticas from final_criticas inner join final_usuarios on (final_usuarios.correo_electronico = final_criticas.correo_creador) where final_usuarios.nick like '$letras%' and id_entrada = '$identrada'";
			}
			else
				$consulta = "select count(id_entrada) as num_criticas from final_criticas where id_entrada = '$identrada'";
			
			if($result = $bd->query($consulta))
				return $result;
		}
		return null;
	}

	function meditarcritica(){
		/*
			Devuelve: consulta con una critica seleccionada para poder ser editada
		*/
		$bd = conexion();

		if($bd == null){ //No hay conexión
			mcierrasesion();
			return -8080;
		}
		$correo = $_POST['correo'];
		if(isset($_POST["entrada"])){
			$identrada = $_POST["entrada"];
			$critica = "select final_entradas.titulo,final_criticas.critica,final_entradas.id_entrada,final_criticas.valoracion,final_criticas.correo_creador,final_criticas.fecha,final_usuarios.nick from final_criticas,final_entradas,final_usuarios where final_criticas.id_entrada=$identrada AND final_criticas.correo_creador='$correo' AND final_entradas.id_entrada = final_criticas.id_entrada AND final_usuarios.correo_electronico = final_criticas.correo_creador";
			return $bd->query($critica);
		}
		return null;
	}

	function mmodificarcritica(){
		/*
			Devuelve: Instruccion sql para actualizar una critica presente
		*/
		$bd = conexion();

		if($bd == null){ //No hay conexión
			mcierrasesion();
			return -8080;
		}

		if(isset($_POST['critica'])){
			if(isset($_POST['identrada'])){
				if(isset($_POST['valoracion'])){
					if(isset($_POST['correo'])){
						$critica = $_POST["critica"];
						$correo_creador = $_POST['correo'];
						$identrada = $_POST["identrada"];
						$valoracion = $_POST["valoracion"];
						if(!empty($critica) && strlen($critica) >= MIN_CRITICA && strlen($critica) <= MAX_CRITICA){

							$consulta = "update final_criticas set critica='$critica',valoracion = '$valoracion' where id_entrada = id_entrada AND correo_creador = '$correo_creador'";
							if($bd -> query($consulta)){
								if($correo_creador != $_SESSION['user']){
									if(isset($_POST['comentarios']))
										$explicacion = $_POST['comentarios'];
									if(isset($_POST['titulo'])){
										$titulo = $_POST['titulo'];
									}
									$consulta2 = "insert into final_notificaciones(correo_electronico,titulo,texto) values('$correo_creador', 'Su crítica ha sido modificada','Su crítica de la entrada: $titulo ha sido revisada y modificada. Explicación adicional: $explicacion')";
									$bd->query($consulta2);
								}
								return 0;
							}
							else
								return -4;
						}
						else
							return -6;
					}
					else
						return -5;
				}
				else
					return -3;
			}
			else
				return -2;
		}
		else
			return -1;	
	}

	function meliminarcritica(){
		/*
			Devuelve: resultado del intento de eliminacion de una critica
			1 - Se ha eliminado correctamente
			0 - No se ha eliminado correctamente
		*/
		$bd = conexion();

		if($bd == null){ //No hay conexión
			mcierrasesion();
			return -8080;
		}

		if(isset($_POST['correo'])){
			if(isset($_POST['identrada'])){
				$correo_creador = $_POST['correo'];
				$identrada = $_POST["identrada"];

				$consulta ="delete from final_criticas where id_entrada=$identrada AND correo_creador='$correo_creador'";
				if($bd -> query($consulta)){
					if($correo_creador != $_SESSION['user']){
						if(isset($_POST['comentarios']))
							$explicacion = $_POST['comentarios'];
						if(isset($_POST['titulo'])){
							$titulo = $_POST['titulo'];
						}
						$consulta2 = "insert into final_notificaciones(correo_electronico,titulo,texto) values('$correo_creador', 'Su crítica ha sido eliminada','Su crítica de la entrada: $titulo ha sido revisada y eliminada. Explicación adicional: $explicacion')";
						$bd->query($consulta2);
					}
					return 0;
				}
				else
					return -4;
			}
			else
				return -5;
		}
		else
			return -3;
	}

	function mrecuperacritica(){
		$con = conexion();

		if($con == null){ //No hay conexión
			mcierrasesion();
			return -8080;
		}

		if(isset($_POST['entrada'])){
			if(isset($_POST['correo'])){
				$id_entrada = $_POST['entrada'];
				$correo = $_POST['correo'];

				$consulta = "select critica, valoracion, fecha, nick, correo_electronico from final_usuarios inner join final_criticas on (final_usuarios.correo_electronico = final_criticas.correo_creador) where id_entrada = '$id_entrada' and correo_creador = '$correo'";

				if($result = $con->query($consulta))
					return $result;
			}
		}
		return null;

	}

	/** Función que permite comprobar el cambio de datos de perfil del usuario **/
	function mcambiodatosperfil(){
		$con = conexion();

		if($con == null){ //No hay conexión
			mcierrasesion();
			return -8080;
		}

		$datos_modificados = 0; //Variable que indica si los datos han sido modificados o no uçpor el usuario.
		//Inicializar las variables por si algún campo no es modificado:
		$correo = $_SESSION['user'];
		$nombre = $_SESSION['nombre'];
		$ap1 = $_SESSION['ap1'];
		$ap2 = $_SESSION['ap2'];
		$nick = $_SESSION['nick'];
		$direccion = $_SESSION['direccion'];
		$provincia = $_SESSION['provincia'];
		$municipio = $_SESSION['municipio'];

		if(isset($_POST['nombre'])){
			if(!empty($_POST['nombre']) && strlen($_POST['nombre']) > 0){
				$nombre = $_POST['nombre'];
				$datos_modificados = 1;
				$aux = strlen(preg_replace('/\s/', '', $nombre)); //Eliminar todos los espacios en blanco
				if($aux >= MIN_NOMBRE && $aux <= MAX_NOMBRE){ //Comprobar longitud
					if(preg_match('/[^A-Za-z]/', $nombre)) //Comprobar los caracteres
						return -1;
				}
				else
					return -2;
			}
		}
		if(isset($_POST['ap1'])){
			if(!empty($_POST['ap1']) && strlen($_POST['ap1']) > 0){
				$ap1 = $_POST['ap1'];
				$datos_modificados = 1;
				$aux = strlen(preg_replace('/\s/', '', $ap1)); //Eliminar todos los espacios en blanco
				if($aux >= MIN_APELLIDO && $aux <= MAX_APELLIDO){ //Comprobar longitud
					if(preg_match('/[^A-Za-z]/', $ap1)) //Comprobar los caracteres
						return -3;
				}
				else
					return -4;
			}
		}
		if(isset($_POST['ap2'])){
			if(!empty($_POST['ap2']) && strlen($_POST['ap2']) > 0){
				$ap2 = $_POST['ap2'];
				$datos_modificados = 1;
				$aux = strlen(preg_replace('/\s/', '', $ap2)); //Eliminar todos los espacios en blanco
				if($aux >= MIN_APELLIDO && $aux <= MAX_APELLIDO){ //Comprobar longitud
					if(preg_match('/[^A-Za-z]/', $ap2)) //Comprobar los caracteres
						return -5;
				}
				else
					return -6;
			}
		}
		if(isset($_POST['nick'])){
			if(!empty($_POST['nick']) && strlen($_POST['nick']) > 0){
				$nick = $_POST['nick'];
				$datos_modificados = 1;
				$aux = strlen(preg_replace('/\s/', '', $nick)); //Eliminar todos los espacios en blanco
				if($aux >= MIN_NICK && $aux <= MAX_NICK){ //Comprobar longitud
					if(!preg_match('/[^A-Za-z0-9]/', $nick)){ //Comprobar los caracteres
						$consulta = "select count(nick) as num_nick from final_usuarios where nick = '$nick'";
						if($result = $con->query($consulta)){
							$num_nick = $result -> fetch_assoc();
							if($num_nick['num_nick'] != 0) 
								return -7; //Nick en la BBDD
						}
						else
							return -8;
					}
					else	
						return -9;
				}
				else
					return -10;
			}
		}

		if(isset($_POST['direccion'])){
			if(!empty($_POST['direccion']) && strlen($_POST['direccion']) > 0){
				$datos_modificados = 1;
				$direccion = $_POST['direccion'];
				$aux = strlen(preg_replace('/\s/', '', $direccion)); //Eliminar todos los espacios en blanco
				if(!($aux >= MIN_DIRECCION && $aux <= MAX_DIRECCION)) //Comprobar longitud
					return -11;
			}
		}

		if(isset($_POST['provincia'])){
			if(!empty($_POST['provincia'])){
				$datos_modificados = 1;
				$provincia = $_POST['provincia'];
				$check_provincia = mcheckprovincia();
				if($check_provincia != 0)
					return $check_provincia;
				else{
					if(!empty($_POST['municipio'])){
						$municipio = $_POST['municipio'];
						$datos_modificados = 1;
						$check_municipio = mcheckmunicipio();
						if($check_municipio != 0)
							return $check_municipio;
					}
					else
						return -13;
				}
			}
		}

		if($datos_modificados == 1){
			$consulta = "update final_usuarios set nombre = '$nombre', apellido1 = '$ap1', apellido2= '$ap2', nick = '$nick', direccion = '$direccion', provincia = '$provincia', municipio = '$municipio' where correo_electronico = '$correo'";
			if($con -> query($consulta))
				return 0;
			else
				return -15;
		}
		else
			return 1;
	}

	/** Función que permite obtener el conjunto de provincias almacenadas en la base de datos **/
	function mobtenerprovincias(){
		$con = conexion();

		if($con == null){ //No hay conexión
			mcierrasesion();
			return -8080;
		}

		$consulta = 'select * from final_provincias';

		if($resultado = $con->query($consulta)) //Si recupera correctamente las provincias:
			return $resultado;
		else //Si no se han podido recuperar las provincias:
			return null;
	}

	/** Función que permite obtener el conjunto de municipios almacenados en la base de datos de acuerdo a el id de provincia recibido **/
	function mobtenermunicipios(){
		$con = conexion();

		if($con == null){ //No hay conexión
			mcierrasesion();
			return -8080;
		}

		if($_GET['idprovincia']){ //Comprobar que el id de la provincia no sea nulo:
			$provincia_id = $_GET['idprovincia'];
			$consulta = "select * from final_municipios where provincia_id = '$provincia_id'";

			if($result = $con->query($consulta)){ //Datos obtenidos de manera correcta:
				return $result;
			}
		}
		return null;
	}

	/** Función que permite comprobar la provincia seleccionada por el usuario **/
	function mcheckprovincia(){
		$con = conexion();

		if($con == null){ //No hay conexión
			mcierrasesion();
			return -8080;
		}

		if(isset($_POST['provincia'])){ //Comprobar que la provincia no sea vacia:
			$provincia = $_POST['provincia']; //Obtener la provincia seleccionada por el usuario:

			$consulta = "SELECT COUNT(id_provincia) as provincia from final_provincias where id_provincia = '$provincia'";

			if($resultados = $con->query($consulta)){ //Obtener el resultado de la query
				$resultado = $resultados->fetch_assoc();
				if($resultado['provincia'] == 1){ //Se ha encontrado la provincia en la base de datos
					return 0;
				}
				else //No se ha encontrado la provincia en la base de datos:
					return -33;

			}
			else //Error en la query:
				return -34;
		}
		else //Provincia vacia:
			return -35;
	}

	/** Función que permite comprobar el municipio seleccionado por el usuario **/
	function mcheckmunicipio(){
		$con = conexion();

		if($con == null){ //No hay conexión
			mcierrasesion();
			return -8080;
		}

		if(isset($_POST['municipio'])){ //Comprobar que el municipio no sea vacio:
			$municipio = $_POST['municipio']; //Obtener el municipio seleccionada por el usuario:
			$provincia = $_POST['provincia']; 

			$consulta = "SELECT COUNT(id_municipio) as final_municipio from final_municipios where id_municipio = '$municipio' and provincia_id = '$provincia'";

			if($resultados = $con->query($consulta)){ //Obtener el resultado de la query
				$resultado = $resultados->fetch_assoc();
				if($resultado['final_municipio']  == 1){ //Se ha encontrado el municipio en la base de datos
					return 0;
				}
				else //No se ha encontrado el municipio en la base de datos:
					return -43;
			}
			else //Error en la query:
				return -44;
		}
		else //Municipio vacio:
			return -45;
	}

	/** Función que permite obtener el perfil de un usuario ajeno **/
	function mobtenerperfilajeno(){

		if(isset($_GET['nick'])){
			$con = conexion();

			if($con == null){ //No hay conexión
				mcierrasesion();
				return -8080;
			}

			$nick = $_GET['nick'];
			$consulta = "select final_usuarios.*,final_provincias.provincia,final_municipios.municipio from final_usuarios,final_provincias,final_municipios where nick = '$nick' and final_provincias.id_provincia = final_usuarios.provincia and final_municipios.id_municipio = final_usuarios.municipio";

			if($result = $con -> query($consulta)){
				return $result;
			}
		}
		return null;
	}

	/** Función que permite obtener el almacenamiento ocupado por la BBDD **/
	function malmacenamiento(){
		$con = conexion();

		if($con == null){ //No hay conexión
			mcierrasesion();
			return -8080;
		}

		$consulta = "SELECT table_schema,
		ROUND(sum(data_length + index_length ) / 1024 / 1024,1) as tamano
		FROM information_schema.TABLES where TABLE_SCHEMA like 'db_%' group by table_schema";

		if($result = $con -> query($consulta)){
				return $result;
		}
		return null;
	}

	/** Función que permite obtener información estadística a cerca de la página web **/
	function mestadisticas(){
		$con = conexion();

		if($con == null){ //No hay conexión
			mcierrasesion();
			return -8080;
		}

		$usuarios = "select count(correo_electronico) as num_usuarios from final_usuarios";
		$bloqueados = "select count(correo_electronico) as num_bloqueados from final_usuarios where bloqueado = '1'";
		$verificados = "select count(correo_electronico) as num_verificados from final_usuarios where tipo = '2'";
		$posts = "select count(id_post) as num_posts from final_posts";
		$entradas = "select count(id_entrada) as num_entradas from final_entradas";
		$descuentos = "select count(id_descuento) as num_descuentos from final_descuentos";

		return array($con->query($usuarios),$con->query($bloqueados),$con->query($verificados),$con->query($posts),$con->query($entradas),$con->query($descuentos));
	}

	/** Elimina la entrada almacenada en el proceso de creación de la misma si el administrador pulsa atras **/
	function meliminarentradaalmacenada(){
		$con = conexion();

		if($con == null){ //No hay conexión
			mcierrasesion();
			return -8080;
		}
		if($_GET['id_entrada']){
			$id_entrada = $_GET['id_entrada'];
			$consulta = "delete from final_entradas where id_entrada = '$id_entrada'";

			if($con ->query($consulta))
				return 0;
			else
				return -2;
		}
		else
			return -1;
	}

	function mrecuperadescuentos(){
		/*
			Funcion que devuelve los descuentos disponibles en la página web
		*/
		$bd = conexion();

		if($bd == null){ //No hay conexión
			mcierrasesion();
			return -8080;
		}
		if(isset($_GET['pagina'])){
			$pagina_actual = $_GET["pagina"]; //Obtener el número de página actual en la que se encuentra el usuario.
			$pagina_actual--; //Como el número de página comienza por 1 en vez de por 0 reducir en 1 el número de la página.
			$desplazamiento = $pagina_actual * ELEMENTOS_PAGINA; //Calular el desplazamiento en función de la página actual * el número de entradas a mostrar por página
			$letra_cine = "";
			if($_GET['letra_cine'])
				$letra_cine = $_GET['letra_cine'];
			$letra_pelicula = "";
			if($_GET['letra_pelicula'])
				$letra_pelicula = $_GET['letra_pelicula'];

			$consulta = "select final_descuentos.* from final_descuentos where cine like '$letra_cine%' and pelicula like '$letra_pelicula%' order by dia desc, mes desc, ano desc limit $desplazamiento,".ELEMENTOS_PAGINA;
			if($result = $bd->query($consulta))
				return $result;
		}
		return null;
	}

	/** Función que permite recuperar el numero de descuentos disponibles **/
	function mnumerodescuentos(){
		$bd = conexion();

		if($bd == null){ //No hay conexión
			mcierrasesion();
			return -8080;
		}
		$letra_cine = "";
		if($_GET['letra_cine'])
			$letra_cine = $_GET['letra_cine'];
		$letra_pelicula = "";
		if($_GET['letra_pelicula'])
			$letra_pelicula = $_GET['letra_pelicula'];

		$consulta= "select count(id_descuento) as num_descuentos from final_descuentos where cine like '$letra_cine%' and pelicula like '$letra_pelicula%'";
		if($result = $bd -> query($consulta))
			return $result;
		else
			return null;
	}

	/** Función que permite eliminar el descuento seleccionado **/
	function meliminardescuento(){
		$bd = conexion();

		if($bd == null){ //No hay conexión
			mcierrasesion();
			return -8080;
		}

		if(isset($_POST['iddescuento'])){
			$id_descuento = $_POST['iddescuento'];
			$consulta = "Delete from final_descuentos where id_descuento = '$id_descuento'";

			if($bd -> query($consulta))
				return 0;
			else
				return -1;
		}
		else
			return -2;
	}
 ?>