<?php
	//Constantes:
	
	define ("MAX_TIEMPO_SESION", "600"); //Segundos

	//Constantes para el login:
	define("MIN_NOMBRE","3");
	define("MIN_APELLIDO","4");
	define("MAX_APELLIDO","30");
	define("MAX_NOMBRE","30");
	define("MAX_PASSWD","100");
	define("MIN_PASSWD","6");
	define("MAX_CORREO","60");
	define("MAX_DIRECCION","50");
	define("MIN_DIRECCION","5");
	define("MAX_NICK", "30");
	define("MIN_NICK", "5");

	//Constantes para la paginación:
	define("ELEMENTOS_PAGINA","5");
	define("LIMITE_PAGINA","10");

	//Constantes para modificar una entrada:
	define("MAX_TITULO","100");
	define("MIN_TITULO","10");
	define("MIN_CONTENIDO","10");
	define("TAMANO_DESCRIPCION","25");
	define("TAMANO_TITULO","25");
	define("TAMANO_CRITICA", "30");
	define("PUNTUACION_MAXIMA", "10");
	define("LONGITUD_ENTRADA","100");
	define("LONGITUD_TITULO","40");
	define("LONGITUD_REPATO", "45");
	define("LONGITUD_DIRECCION", "45");

	//constantes para críticas:
	define('MIN_CRITICA', '100');
	define('MAX_CRITICA', '5000');
	define("LONGITUD_CRITICA", "100");
	

	//Constantes para el perfil:
	define("MAX_IMAGEN","2000000"); //2MB

	//Constantes para el foro:
	define("MIN_COMENTARIO", "20");
	define("MAX_COMENTARIO", "500");
	define("LONGITUD_COMENTARIO","100");
	define("LONGITUD_TITULO_POST","40");
	define("LONGITUD_COMENTARIO_POST","80");

	//Constantes para el foro:
	define("COMENTARIOS_PAGINA","4");

	//Constantes para solicitudes
	define("TAMANO_SOLICITUD","100");



?>
